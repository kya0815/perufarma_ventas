package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview;

import android.content.Context;
import android.databinding.ObservableDouble;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerViewHolder;
import com.kyaemp.perufarma.demo.model.mapper.CatalogoModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.model.observables.PedidoModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDemoViewModel;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.ObtenerCatalogoByIDUseCase;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;

import javax.inject.Inject;


@PerViewHolder
public class PedidosItemViewModel extends BaseDemoViewModel<MvvmView> implements PedidosItemMvvm.ViewModel {

    private PedidoModel pedido;
    private ClientesModel cliente;
    private ClienteModelDataMapper mapper;
    private ObtenerCatalogoByIDUseCase obtenerCatalogoByIDUseCase;
    private CatalogoModelDataMapper mapperCatalogo;
    private CatalogoModel catalogo;
    private ObservableDouble precioTotal;

    @Inject
    public PedidosItemViewModel(@AppContext Context context, Navigator navigator, ClienteModelDataMapper mapper,
                                ObtenerCatalogoByIDUseCase obtenerCatalogoByIDUseCase,CatalogoModelDataMapper mapperCatalogo) {
        super(context, navigator);
        this.mapper=mapper;
        this.mapperCatalogo=mapperCatalogo;
        this.obtenerCatalogoByIDUseCase=obtenerCatalogoByIDUseCase;
        this.catalogo=new CatalogoModel(0,"","","",0,0,0,0);
    }

    @Override
    public void update(Pedido pedido, Cliente cliente) {
        this.pedido=mapper.transformToPresenter(pedido);
        this.cliente=mapper.transform(cliente);
        obtenerCatalogoByIDUseCase.execute(new ObtenerDetalleItemPedidoObserver(),
                ObtenerCatalogoByIDUseCase.Params.id(pedido.getCodigo_item()));
        precioTotal= new ObservableDouble(pedido.getCantidad()*catalogo.getPrecioUnitario());
        notifyChange();
    }

    @Override
    public PedidoModel getPedido() {
        return pedido;
    }

    @Override
    public ClientesModel getCliente() {
        return cliente;
    }

    @Override
    public CatalogoModel getCatalogo() {
        return catalogo;
    }

    @Override
    public ObservableDouble getPrecioTotal() {
        return precioTotal;
    }

    public void setCatalogo(CatalogoModel catalogo) {
        this.catalogo = catalogo;
        precioTotal= new ObservableDouble(pedido.getCantidad()*catalogo.getPrecioUnitario());
        notifyChange();
    }

    private final class ObtenerDetalleItemPedidoObserver extends DefaultObserver<Catalogo> {

        @Override
        public void onComplete() {
//            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
//            hideLoading();
            toast(ctx,e.getMessage());
        }

        @Override
        public void onNext(Catalogo result) {
            CatalogoModel catalogo = mapperCatalogo.transform(result);
            if (result != null && cliente != null) {
                setCatalogo(catalogo);
            } else {
                toast(ctx, "No existe el cliente");
            }
        }
    }
}
