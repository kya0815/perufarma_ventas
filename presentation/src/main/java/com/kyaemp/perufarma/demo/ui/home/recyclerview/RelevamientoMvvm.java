package com.kyaemp.perufarma.demo.ui.home.recyclerview;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.view.View;

import com.kyaemp.perufarma.demo.model.observables.RelevamientoModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;


/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
public interface RelevamientoMvvm {

    interface ViewModel extends IViewModel<MvvmView> {

        void onEnviar(View v);

        ObservableBoolean isLoaded();

        ObservableBoolean isEnviado();

        void update(RelevamientoModel relevamiento);

        RelevamientoModel getRelevamiento();

        void setPosition(int position);

        ObservableInt getPosition();
        ObservableBoolean getPrimero();
    }
}
