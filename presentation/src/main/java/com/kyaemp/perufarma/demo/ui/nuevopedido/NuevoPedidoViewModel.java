package com.kyaemp.perufarma.demo.ui.nuevopedido;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.kyaemp.perufarma.data.entity.mapper.ClienteEntityDataMapper;
import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.mapper.CatalogoModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.model.observables.NuevoPedidoModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.DetalleClienteViewPagerAdapter;
import com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview.PedidoItemAdapter;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.GuardarPedidosClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerCatalogoLocalUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerDetalleClienteUseCase;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.model.Pedido;

import java.util.List;

import javax.inject.Inject;

@PerActivity
public class NuevoPedidoViewModel extends BaseActivityViewModel<NuevoPedidoMvvm.View> implements NuevoPedidoMvvm.ViewModel {

    private static final int MY_REQUEST_PERMISSION = 14;
    private final ObservableBoolean loaded;
    private ClientesModel cliente=null;
    private final static int CODE_SIGNUP=13;
    private CatalogoModelDataMapper mapper;
    private int idCliente;
    private ObtenerDetalleClienteUseCase obtenerDetalleClienteUseCase;
    private NuevoPedidoModel model;
    private CatalogoModel producto;
    private ObtenerCatalogoLocalUseCase obtenerCatalogoLocalUseCase;
    private List<CatalogoModel> items;
    private ObservableBoolean productoSelect;
    private GuardarPedidosClienteUseCase guardarPedidosClienteUseCase;

    private PedidoItemAdapter adapter;
    @Inject
    NuevoPedidoViewModel(@AppContext Context appContext, Navigator navigator,PedidoItemAdapter adapter,
                         CatalogoModelDataMapper mapper, ObtenerCatalogoLocalUseCase obtenerCatalogoLocalUseCase,
                         GuardarPedidosClienteUseCase guardarPedidosClienteUseCase) {
        super(appContext, navigator);
        this.loaded = new ObservableBoolean(false);
        this.productoSelect = new ObservableBoolean(false);
        this.obtenerCatalogoLocalUseCase=obtenerCatalogoLocalUseCase;
        this.guardarPedidosClienteUseCase=guardarPedidosClienteUseCase;
        this.mapper=mapper;
        this.adapter=adapter;
        this.model=new NuevoPedidoModel(0,0,"");
        initView();
    }

    private void initView() {
        showLoading();
        obtenerCatalogoLocalUseCase.execute(new ObtenerProductosObserver(),null);
    }


    @Override
    public ObservableBoolean getLoaded() {
        return loaded;
    }

    @Override
    public void saveData() {
        if(model.getCantidad()!=0 && productoSelect.get()){
            Pedido pedido=new Pedido();
            pedido.setCodigo_item(model.getIdItem());
            pedido.setCantidad(model.getCantidad());
            pedido.setCliente_codigo(idCliente);
            showLoading();
            guardarPedidosClienteUseCase.execute(new GuardarPedidoObserver(),
                    GuardarPedidosClienteUseCase.Params.pedido(pedido));
        }else{
            toast("seleccione un producto y la cantidad que desea solicitar");
        }
    }

    private void showLoading(){
        loaded.set(true);
    }

    private void hideLoading(){
        loaded.set(false);
    }

    @Override
    public void setModel(NuevoPedidoModel model) {
        this.model = model;
        notifyChange();
    }

    @Override
    public NuevoPedidoModel getModel() {
        return model;
    }

    public void setItems(List<CatalogoModel> items) {
        this.items = items;
        adapter.setItems(this.items);
    }

    private final class ObtenerProductosObserver extends DefaultObserver<List<Catalogo>> {

        @Override
        public void onComplete() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            toast(ctx,e.getMessage());
        }

        @Override
        public void onNext(List<Catalogo> result) {
            List<CatalogoModel> items = mapper.transform(result);
            if (result != null && items != null) {
                setItems(items);
            } else {
                toast(ctx, "No existen catalogos");
            }
        }
    }
    private final class GuardarPedidoObserver extends DefaultObserver<Boolean> {

        @Override
        public void onComplete() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            toast(ctx,e.getMessage());
        }

        @Override
        public void onNext(Boolean result) {
            if(result){
                navigator.setResult(Activity.RESULT_OK,null);
                navigator.finishActivity();
            }
        }
    }
    @Override
    public void setProducto(CatalogoModel producto) {
        getView().ocultarTeclado();
        getView().ocultarSearchView(true);
        this.producto=producto;
        productoSelect.set(true);
        this.model=new NuevoPedidoModel(producto.getId(),0,producto.getProducto());
        notifyChange();
    }

    @Override
    public CatalogoModel getProducto() {
        return producto;
    }

    @Override
    public ObservableBoolean getProductoSelect() {
        return productoSelect;
    }

    @Override
    public void OnClickMenosCantidad(View view) {
        if(model.getCantidad()!=0){
            model.setCantidad(model.getCantidad()-1);
        }
    }

    @Override
    public void OnClickMasCantidad(View view) {
        model.setCantidad(model.getCantidad()+1);
    }

    @Override
    public void OnClickDeleteProducto(View view) {
        productoSelect.set(false);
        getView().ocultarSearchView(false);
    }

    @Override
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
}


