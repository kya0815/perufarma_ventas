package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview;

import android.databinding.ObservableDouble;

import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.model.observables.PedidoModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;


/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
public interface PedidosItemMvvm {

    interface ViewModel extends IViewModel<MvvmView> {


        void update(Pedido pedido, Cliente cliente);

        PedidoModel getPedido();
        ClientesModel getCliente();
        CatalogoModel getCatalogo();
        ObservableDouble getPrecioTotal();
    }
}
