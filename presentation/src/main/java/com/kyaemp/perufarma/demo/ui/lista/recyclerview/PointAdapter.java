package com.kyaemp.perufarma.demo.ui.lista.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.observables.PointModel;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class PointAdapter extends RecyclerView.Adapter<PointViewHolder> {

    private List<PointModel> points = Collections.emptyList();

    @Inject
    public PointAdapter() {
    }

    public void setCountryList(List<PointModel> points) {
        this.points = points;
    }

    @Override
    public PointViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_point, viewGroup, false);

        return new PointViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PointViewHolder countryViewHolder, int position) {
        countryViewHolder.viewModel().update(points.get(position));
        countryViewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return points.size();
    }

}
