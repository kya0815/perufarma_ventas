package com.kyaemp.perufarma.demo.model.observables;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */
public class ClientesModel extends BaseObservable {

    private int id;
    private String cliente_nombre;
    private Long cliente_ruc;
    private int cliente_codigo;
    private String ultimo_pedido;
    private int monto_vencido;
    private String telefono;
    private String direccion;
    private String departamen;
    private String provincia;
    private String distrito;
    private String correo;
    private double latitud;
    private double longitud;

    public ClientesModel(int id, String cliente_nombre, Long cliente_ruc, int cliente_codigo, String ultimo_pedido, int monto_vencido, String telefono, String direccion,
                         String departamen,String provincia,String distrito,String correo,double latitud, double longitud) {
        this.id = id;
        this.cliente_nombre = cliente_nombre;
        this.cliente_ruc = cliente_ruc;
        this.cliente_codigo = cliente_codigo;
        this.ultimo_pedido = ultimo_pedido;
        this.monto_vencido = monto_vencido;
        this.telefono = telefono;
        this.direccion = direccion;
        this.departamen = departamen;
        this.provincia = provincia;
        this.distrito = distrito;
        this.correo = correo;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getCliente_nombre() {
        return cliente_nombre;
    }

    public void setCliente_nombre(String cliente_nombre) {
        this.cliente_nombre = cliente_nombre;
        notifyPropertyChanged(BR.cliente_nombre);
    }

    @Bindable
    public String getCliente_ruc() {
        return String.valueOf(cliente_ruc);
    }

    public void setCliente_ruc(Long cliente_ruc) {
        this.cliente_ruc = cliente_ruc;
        notifyPropertyChanged(BR.cliente_ruc);
    }
    public void setCliente_ruc(String cliente_ruc) {
        this.cliente_ruc = Long.valueOf(cliente_ruc);
        notifyPropertyChanged(BR.cliente_ruc);
    }

    @Bindable
    public String getCliente_codigo() {
        return String.valueOf(cliente_codigo);
    }

    public void setCliente_codigo(int cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
        notifyPropertyChanged(BR.cliente_codigo);
    }
    public void setCliente_codigo(String cliente_codigo) {
        this.cliente_codigo = Integer.valueOf(cliente_codigo);
        notifyPropertyChanged(BR.cliente_codigo);
    }

    @Bindable
    public String getUltimo_pedido() {
        return ultimo_pedido;
    }

    public void setUltimo_pedido(String ultimo_pedido) {
        this.ultimo_pedido = ultimo_pedido;
        notifyPropertyChanged(BR.ultimo_pedido);
    }

    @Bindable
    public int getMonto_vencido() {
        return monto_vencido;
    }

    public void setMonto_vencido(int monto_vencido) {
        this.monto_vencido = monto_vencido;
        notifyPropertyChanged(BR.monto_vencido);
    }

    @Bindable
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
        notifyPropertyChanged(BR.telefono);
    }

    @Bindable
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
        notifyPropertyChanged(BR.direccion);
    }

    @Bindable
    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
        notifyPropertyChanged(BR.latitud);
    }

    @Bindable
    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
        notifyPropertyChanged(BR.longitud);
    }

    @Bindable
    public String getDepartamen() {
        return departamen;
    }

    public void setDepartamen(String departamen) {
        this.departamen = departamen;
        notifyPropertyChanged(BR.departamen);
    }

    @Bindable
    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
        notifyPropertyChanged(BR.provincia);
    }

    @Bindable
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
        notifyPropertyChanged(BR.distrito);
    }

    @Bindable
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
        notifyPropertyChanged(BR.correo);
    }
}
