package com.kyaemp.perufarma.demo.ui.base.viewmodel;


import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

import io.reactivex.disposables.CompositeDisposable;

public abstract class RxBaseViewModel<T extends MvvmView> extends BaseViewModel<T> {

    protected final CompositeDisposable disposable = new CompositeDisposable();

    @Override
    public void detachView() {
        super.detachView();
        disposable.clear();
    }
}
