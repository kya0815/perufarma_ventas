package com.kyaemp.perufarma.demo.injection.components;

import android.content.Context;

import com.kyaemp.perufarma.demo.injection.modules.ServiceModule;
import com.kyaemp.perufarma.demo.injection.qualifier.ServiceContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerService;
import com.kyaemp.perufarma.demo.services.SocketServices;
import com.kyaemp.perufarma.demo.services.TrackingController;

import dagger.Component;

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
@PerService
@Component(dependencies = AppComponent.class, modules = {ServiceModule.class})
public interface ServiceComponent extends AppComponent {

    @ServiceContext
    Context serviceContext();

    TrackingController trackingController();
    // create inject methods for your Activities here

    void inject(SocketServices services);

}
