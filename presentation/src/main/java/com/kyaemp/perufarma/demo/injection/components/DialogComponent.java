package com.kyaemp.perufarma.demo.injection.components;

import com.kyaemp.perufarma.demo.injection.modules.DialogModule;
import com.kyaemp.perufarma.demo.injection.modules.ViewModelModule;
import com.kyaemp.perufarma.demo.injection.scopes.PerDialogFragment;
import com.kyaemp.perufarma.demo.ui.map.guardarpoint.GuardarPointDialog;

import dagger.Component;

@PerDialogFragment
@Component(dependencies = ActivityComponent.class, modules = {DialogModule.class, ViewModelModule.class})
public interface DialogComponent {
    // create inject methods for your DialogFragments here
    void inject(GuardarPointDialog fragment);
}
