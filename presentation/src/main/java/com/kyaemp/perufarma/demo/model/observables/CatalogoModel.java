package com.kyaemp.perufarma.demo.model.observables;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */

public class CatalogoModel extends BaseObservable {

    private long id;
    private String categoria;
    private String marca;
    private String producto;
    private int stockAlmacen;
    private int cantidadVendida;
    private double precioUnitario;
    private double subtotal;

    public CatalogoModel(long id, String categoria, String marca, String producto, int stockAlmacen, int cantidadVendida, double precioUnitario, double subtotal) {
        this.id = id;
        this.categoria = categoria;
        this.marca = marca;
        this.producto = producto;
        this.stockAlmacen = stockAlmacen;
        this.cantidadVendida = cantidadVendida;
        this.precioUnitario = precioUnitario;
        this.subtotal = subtotal;
    }

    @Bindable
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
        notifyPropertyChanged(BR.categoria);
    }

    @Bindable
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
        notifyPropertyChanged(BR.marca);
    }

    @Bindable
    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
        notifyPropertyChanged(BR.producto);
    }

    @Bindable
    public int getStockAlmacen() {
        return stockAlmacen;
    }

    public void setStockAlmacen(int stockAlmacen) {
        this.stockAlmacen = stockAlmacen;
        notifyPropertyChanged(BR.stockAlmacen);
    }

    @Bindable
    public int getCantidadVendida() {
        return cantidadVendida;
    }

    public void setCantidadVendida(int cantidadVendida) {
        this.cantidadVendida = cantidadVendida;
        notifyPropertyChanged(BR.cantidadVendida);
    }

    @Bindable
    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
        notifyPropertyChanged(BR.precioUnitario);
    }

    @Bindable
    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
        notifyPropertyChanged(BR.subtotal);
    }
}
