package com.kyaemp.perufarma.demo.injection.modules;

import android.support.v4.app.DialogFragment;

import com.kyaemp.perufarma.demo.injection.scopes.PerDialogFragment;
import com.kyaemp.perufarma.demo.ui.base.navigator.DialogFragmentNavigator;
import com.kyaemp.perufarma.demo.ui.base.navigator.DialogNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class DialogModule {

    private final DialogFragment mDialogFragment;

    public DialogModule(DialogFragment dialogFragment) {
        mDialogFragment = dialogFragment;
    }

    @Provides
    @PerDialogFragment
    DialogNavigator provideDialogFragmentNavigator() {
        return new DialogFragmentNavigator(mDialogFragment);
    }

}
