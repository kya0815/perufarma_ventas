package com.kyaemp.perufarma.demo.ui.signup;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.mapper.RespuestaServerModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.UserSignUpModelDataMapper;
import com.kyaemp.perufarma.demo.model.normal.RespuestaServerModel;
import com.kyaemp.perufarma.demo.model.observables.UserSignUpModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.SignUpUseCase;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import javax.inject.Inject;

import static android.app.Activity.RESULT_OK;

@PerActivity
public class SignUpViewModel extends BaseActivityViewModel<SignUpMvvm.View> implements SignUpMvvm.ViewModel {

    private final ObservableBoolean loaded;
    private SignUpUseCase signUpUseCase;
    private RespuestaServerModelDataMapper respuestaServerModelDataMapper;
    private UserSignUpModel userSignUp;
    private UserSignUpModelDataMapper signUpModelDataMapper;

    // TODO falta crear obserbale string
    @Inject
    SignUpViewModel(@AppContext Context appContext, Navigator navigator, SignUpUseCase signUpUseCase,
                    UserSignUpModelDataMapper signUpModelDataMapper,
                    RespuestaServerModelDataMapper respuestaServerModelDataMapper) {
        super(appContext, navigator);
        this.signUpUseCase=signUpUseCase;
        this.respuestaServerModelDataMapper = respuestaServerModelDataMapper;
        this.signUpModelDataMapper = signUpModelDataMapper;
        this.userSignUp = new UserSignUpModel("", "","","");
        this.loaded = new ObservableBoolean(false);
    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    @Override
    public UserSignUpModel getUserSignUp() {
        return userSignUp;
    }

    @Override
    public void onClickResgistrase(View v) {
        registrar();
    }

    @Override
    public void onClickLogin(View v) {
        navigator.finishActivity();
    }


    void registroOk(String messages) {
        toast(messages);
        navigator.setResult(RESULT_OK, null);
        navigator.finishActivity();
    }

    void registroFail(String messages) {
        toast(messages);
    }

    void showLoading() {
        loaded.set(true);
    }

    void hideLoading() {
        loaded.set(false);
    }

    void showError(String messages) {
        toast(messages);
    }

    //
    private void registrar() {
        showLoading();
        this.signUpUseCase.execute(
                new SignUpUserObserver(),
                SignUpUseCase.Params.userSignUp(signUpModelDataMapper.transform(userSignUp))
        );
    }

    private final class SignUpUserObserver extends DefaultObserver<RespuestaServer> {

        @Override
        public void onComplete() {
            SignUpViewModel.this.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            SignUpViewModel.this.hideLoading();
            SignUpViewModel.this.showError(e.getMessage());
        }

        @Override
        public void onNext(RespuestaServer result) {
            RespuestaServerModel respuestaServerModel = respuestaServerModelDataMapper.transform(result);
            if (respuestaServerModel.getStatus() == 1) {
                SignUpViewModel.this.registroOk(respuestaServerModel.getMensaje());
            } else {
                SignUpViewModel.this.registroFail(respuestaServerModel.getMensaje());
            }
        }
    }
}
