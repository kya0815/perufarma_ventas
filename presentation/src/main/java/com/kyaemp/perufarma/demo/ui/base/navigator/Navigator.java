package com.kyaemp.perufarma.demo.ui.base.navigator;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.utils.PlainConsumer;


public interface Navigator {
    String EXTRA_ARG = "_args";

    View inflate(int resource, ViewGroup root);

    void requestPermissions(String[] permissions, int resultCode);

    int checkSelfPermission(@NonNull String permission);

    void setResult(int resultCode, Intent data);

    void startService(Intent service);
    void stopService(Intent service);

    void finishActivity();

    void startDialog(DialogFragment dialog);
    void startDialogFullScreen(DialogFragment dialog);

    void startActivity(@NonNull Intent intent);

    void startActivity(@NonNull String action);

    void startActivity(@NonNull String action, @NonNull Uri uri);

    void startActivity(@NonNull Class<? extends Activity> activityClass);

    void startActivity(@NonNull Class<? extends Activity> activityClass, @NonNull PlainConsumer<Intent> setArgsAction);

    void startActivity(@NonNull Class<? extends Activity> activityClass, Bundle args);

    void startActivity(@NonNull Class<? extends Activity> activityClass, Parcelable args);

    void startActivity(@NonNull Class<? extends Activity> activityClass, @NonNull String arg);

    void startActivity(@NonNull Class<? extends Activity> activityClass, int arg);

    void startActivityForResult(@NonNull Class<? extends Activity> activityClass, int requestCode);

    void startActivityForResult(@NonNull Class<? extends Activity> activityClass, @NonNull PlainConsumer<Intent> setArgsAction, int requestCode);

    void startActivityForResult(@NonNull Class<? extends Activity> activityClass, @NonNull Parcelable arg, int requestCode);

    void startActivityForResult(@NonNull Class<? extends Activity> activityClass, @NonNull String arg, int requestCode);

    void startActivityForResult(@NonNull Class<? extends Activity> activityClass, int arg, int requestCode);

    void replaceFragment(@IdRes int containerId, @NonNull Fragment fragment, Bundle args);

    void replaceFragment(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args);

    void replaceFragmentAndAddToBackStack(@IdRes int containerId, @NonNull Fragment fragment, Bundle args, String backstackTag);

    void replaceFragmentAndAddToBackStack(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args, String backstackTag);

    boolean isFinishing();

    boolean isLoggin();

    void logout();

    <T> Boolean savePreferences(T objectSave, String nameObject);

    void cleanPreferences();

    void setProductoSelect(CatalogoModel producto);
}
