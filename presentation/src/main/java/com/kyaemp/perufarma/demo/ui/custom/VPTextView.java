package com.kyaemp.perufarma.demo.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.utils.FontCache;

/**
 * Created by HP on 21/08/2016.
 */
public class VPTextView extends android.support.v7.widget.AppCompatTextView {

    public VPTextView(Context context) {
        super(context);
        applyCustomFont(context, 0);
    }

    public VPTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    public VPTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initAttrs(context, attrs);
    }

    private void applyCustomFont(Context context, Integer fontID) {
        if (!isInEditMode()) {
            String ruta = "fonts/";
            switch (fontID) {
                case 1:
                    ruta = ruta + "font_vp.otf";
                    break;
                case 2:
                    ruta = ruta + "arialbd.ttf";
                    break;
                default:
                    ruta = ruta + "arial.ttf";
                    break;

            }
            Typeface customFont = FontCache.getTypeface(ruta, context);
            setTypeface(customFont);
        }
    }

    void initAttrs(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray attributeArray = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.VPTextView);

            Drawable drawableLeft;
            Drawable drawableRight;
            Drawable drawableBottom;
            Drawable drawableTop;
            Drawable drawablebackground;
            Integer fontID;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableLeft = attributeArray.getDrawable(R.styleable.VPTextView_drawableLeftCompat);
                drawableRight = attributeArray.getDrawable(R.styleable.VPTextView_drawableRightCompat);
                drawableBottom = attributeArray.getDrawable(R.styleable.VPTextView_drawableBottomCompat);
                drawableTop = attributeArray.getDrawable(R.styleable.VPTextView_drawableTopCompat);
                drawablebackground = attributeArray.getDrawable(R.styleable.VPTextView_backgroundSVG);

            } else {
                final int drawableLeftId = attributeArray.getResourceId(R.styleable.VPTextView_drawableLeftCompat, -1);
                final int drawableRightId = attributeArray.getResourceId(R.styleable.VPTextView_drawableRightCompat, -1);
                final int drawableBottomId = attributeArray.getResourceId(R.styleable.VPTextView_drawableBottomCompat, -1);
                final int drawableTopId = attributeArray.getResourceId(R.styleable.VPTextView_drawableTopCompat, -1);
                final int drawablebackgroundId = attributeArray.getResourceId(R.styleable.VPTextView_backgroundSVG, -1);

                drawableLeft = drawableLeftId != -1 ? AppCompatResources.getDrawable(context, drawableLeftId) : null;
                drawableRight = drawableRightId != -1 ? AppCompatResources.getDrawable(context, drawableRightId) : null;
                drawableBottom = drawableBottomId != -1 ? AppCompatResources.getDrawable(context, drawableBottomId) : null;
                drawableTop = drawableTopId != -1 ? AppCompatResources.getDrawable(context, drawableTopId) : null;
                drawablebackground = drawablebackgroundId != -1 ? AppCompatResources.getDrawable(context, drawablebackgroundId) : null;
            }
            fontID = attributeArray.getInteger(R.styleable.VPTextView_font, 0);
            applyCustomFont(context, fontID);
            setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRight, drawableBottom);
            if (drawablebackground != null) {
                setBackground(drawablebackground);
            }
            attributeArray.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}