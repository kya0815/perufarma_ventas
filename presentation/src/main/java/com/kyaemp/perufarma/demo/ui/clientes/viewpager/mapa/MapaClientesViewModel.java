package com.kyaemp.perufarma.demo.ui.clientes.viewpager.mapa;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.ui.base.navigator.FragmentNavigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseFragmentViewModel;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.ObtenerClientesUseCase;
import com.kyaemp.perufarma.domain.model.Cliente;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

@PerFragment
public class MapaClientesViewModel extends BaseFragmentViewModel<IMapaClientesViewModel.View> implements IMapaClientesViewModel,IMapaClientesViewModel.ViewModel, GoogleMap.OnInfoWindowClickListener {

    private ObtenerClientesUseCase obtenerClientesUseCase;
    private ClienteModelDataMapper mapper;
    private GoogleMap mMap;
    Resources res;
    List<Cliente> clientes= Collections.emptyList();

    @Inject
    public MapaClientesViewModel(Context context, FragmentNavigator navigator, ObtenerClientesUseCase obtenerClientesUseCase,
                                 ClienteModelDataMapper mapper, Resources res) {
        super(context,navigator);
        this.mapper = mapper;
        this.obtenerClientesUseCase = obtenerClientesUseCase;
        this.res = res;
    }

    @Override
    public void attachView(View mvvmView, @Nullable Bundle savedInstanceState) {
        super.attachView(mvvmView, savedInstanceState);
    }

    @Override
    public void detachView() {
        super.detachView();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        iniciarViews();
    }

    private void iniciarViews() {
        if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().isMyLocationButtonEnabled();
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.setOnInfoWindowClickListener(this);
            obtenerPuntos();
        }
    }

    private void modificarZoom() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for(Cliente cliente: clientes){
            builder.include(new LatLng(cliente.getLatitud(),cliente.getLongitud()));
        }
        LatLngBounds bounds=null;
        try {
            bounds = builder.build();
        }catch (Exception e){
            e.printStackTrace();
        }
        CameraUpdate cu;
        if(bounds!=null) {
            try {
                cu = CameraUpdateFactory.newLatLngBounds(bounds, 100);
                mMap.animateCamera(cu);
            } catch (Exception e) {
                e.printStackTrace();
                cu = CameraUpdateFactory.newLatLngBounds(bounds, 400, 400, 0);
                mMap.animateCamera(cu);
            }
        }

    }

    private void obtenerPuntos() {
        this.obtenerClientesUseCase.execute(new ObtenerClientesVisitarLocalObserver(),null);
    }

    private void crearPunto(Cliente cliente, BitmapDescriptor markercolor) {
        markercolor = markercolor == null ? BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED) : markercolor;
        LatLng pos = new LatLng(cliente.getLatitud(),cliente.getLongitud());
        mMap.addMarker(new MarkerOptions()
                .position(pos)
                .title(String.valueOf(cliente.getCliente_codigo()))
                .icon(markercolor));
        mMap.setInfoWindowAdapter(getInfoAdapterPoint());
    }

    private GoogleMap.InfoWindowAdapter getInfoAdapterPoint() {
        return new GoogleMap.InfoWindowAdapter() {

            @Override
            public android.view.View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public android.view.View getInfoContents(Marker marker) {

                android.view.View v = navigator.inflate(R.layout.infowindows, null);

                TextView title = (TextView) v.findViewById(R.id.title);
                TextView info = (TextView) v.findViewById(R.id.info);
                TextView deuda = (TextView) v.findViewById(R.id.deuda);
                ImageButton waze = (ImageButton) v.findViewById(R.id.waze);

                Cliente cliente=obtenerClienteFromListByCodigo(marker.getTitle());
                if(cliente!=null){
                    title.setText(cliente.getCliente_nombre());
                    info.setText(cliente.getDireccion());
                    if(cliente.getMonto_vencido()==0){
                        deuda.setVisibility(android.view.View.GONE);
                    }
                    deuda.setText(String.format(res.getString(R.string.soles),cliente.getMonto_vencido()));
                }else{
                    title.setVisibility(android.view.View.GONE);
                    info.setVisibility(android.view.View.GONE);
                    deuda.setText("Error");
                }


                return v;
            }
        };
    }

    private Cliente obtenerClienteFromListByCodigo(String code){
        int codigo= Integer.valueOf(code);
        for(Cliente cliente: clientes){
            if(cliente.getCliente_codigo() == codigo){
                return cliente;
            }
        }
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        try
        {
            Cliente cliente=obtenerClienteFromListByCodigo(marker.getTitle());
            // Launch Waze to look for Hawaii:
            //https://waze.com/ul?pd_title=DestinationName&pd_ll=32.0835,34.7659&pd_time=1496928900&deep_link=YourApp
            if(cliente!=null) {
//                String url = String.format(res.getString(R.string.url_base_waze),cliente.getCliente_nombre(),cliente.getLatitud(),cliente.getLongitud());
                String url = "https://waze.com/ul?title="+cliente.getCliente_nombre()+
                        "&ll="+cliente.getLatitud()+","+cliente.getLongitud()+"&navigate=yes";
                Log.e("URL_WAZE",url);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                navigator.startActivity(intent);
            }else{
                toast("error de datos para calcular ruta");
            }
        }
        catch ( ActivityNotFoundException ex  )
        {
            // If Waze is not installed, open it in Google Play:
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
            navigator.startActivity(intent);
        }
    }

    private final class ObtenerClientesVisitarLocalObserver extends DefaultObserver<List<Cliente>> {

        @Override
        public void onComplete() {
//            unlock();
        }

        @Override
        public void onError(Throwable e) {
          e.printStackTrace();
            toast(ctx,e.getMessage());
        }

        @Override
        public void onNext(List<Cliente> result) {
            try {
                if (result!=null && !result.isEmpty()) {
                    clientes=result;
                    BitmapDescriptor markercolorDeuda = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                    BitmapDescriptor markercolorNormal = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE);
                    for (Cliente cliente : result) {
                            crearPunto(cliente,cliente.getMonto_vencido()==0 ? markercolorNormal:markercolorDeuda);
                    }
                    modificarZoom();
                }
            } catch (Exception e) {
                e.printStackTrace();
                toast(ctx,"no hay puntos de clientes para mostrar");
            }
        }
    }
}
