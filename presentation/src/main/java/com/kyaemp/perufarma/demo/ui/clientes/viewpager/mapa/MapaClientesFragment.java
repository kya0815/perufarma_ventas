package com.kyaemp.perufarma.demo.ui.clientes.viewpager.mapa;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.MapClientesBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseFragment;

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
public class MapaClientesFragment extends BaseFragment<MapClientesBinding,IMapaClientesViewModel.ViewModel> implements OnMapReadyCallback, IMapaClientesViewModel.View  {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.map_clientes);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        llamarMapa(savedInstanceState);
    }
    private void llamarMapa(Bundle savedInstanceState) {
        try {
            if (binding.map != null) {

                binding.map.onCreate(savedInstanceState);
                binding.map.onResume();
                binding.map.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        viewModel.onMapReady(googleMap);
    }

}
