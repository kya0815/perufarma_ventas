package com.kyaemp.perufarma.demo.ui.home.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.observables.MenuItemModel;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class MenuItemAdapter extends RecyclerView.Adapter<MenuItemViewHolder> {

    private List<MenuItemModel> items = Collections.emptyList();

    @Inject
    public MenuItemAdapter() {
    }

    public void setItem(List<MenuItemModel> items) {
        this.items = items;
    }

    @Override
    public MenuItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_menu, viewGroup, false);

        return new MenuItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MenuItemViewHolder viewHolder, int position) {
        viewHolder.viewModel().update(items.get(position));
        viewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
