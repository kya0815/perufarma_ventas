package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.detalle;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.TabDetalleClienteBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseFragment;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.DetalleClienteViewPagerAdapter;

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
public class TabDetalleClienteFragment extends BaseFragment<TabDetalleClienteBinding,ITabDetalleClienteViewModel.ViewModel>
        implements ITabDetalleClienteViewModel.View{
    DetalleClienteViewPagerAdapter adapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.tab_detalle_cliente);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        llamarId();
    }

    private void llamarId() {
        if(adapter!=null){
            setId(adapter.idActual);
        }
    }

    public void setId(int id) {
        viewModel.setId(id);
    }


    public void setAdapter(DetalleClienteViewPagerAdapter adapter) {
        this.adapter=adapter;
    }
}
