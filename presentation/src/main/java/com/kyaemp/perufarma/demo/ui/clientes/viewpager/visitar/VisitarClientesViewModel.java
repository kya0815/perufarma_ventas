package com.kyaemp.perufarma.demo.ui.clientes.viewpager.visitar;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDemoViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.recyclerview.ClienteItemAdapter;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.ClientesView;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.ObtenerClientesUseCase;
import com.kyaemp.perufarma.domain.model.Cliente;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

@PerFragment
public class VisitarClientesViewModel extends BaseDemoViewModel<ClientesView> implements IVisitarClientesViewModel {

    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ObtenerClientesUseCase obtenerClientesUseCase;
    private final ClienteItemAdapter adapter;
    private ClienteModelDataMapper mapper;
    private ObservableBoolean empty;

    @Inject
    public VisitarClientesViewModel(Context context, Navigator navigator,
                                    ClienteItemAdapter adapter, ObtenerClientesUseCase obtenerClientesUseCase,
                                    ClienteModelDataMapper mapper) {
        super(context,navigator);
        this.adapter = adapter;
        this.mapper = mapper;
        this.obtenerClientesUseCase = obtenerClientesUseCase;
        this.empty=new ObservableBoolean(false);
    }

    @Override
    public void attachView(@NonNull ClientesView view, @Nullable Bundle savedInstanceState) {
        super.attachView(view, savedInstanceState);

        this.obtenerClientesUseCase.execute(new ObtenerClientesVisitarLocalObserver(),null);
    }


    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public ClienteItemAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onClickNewPedido(View v) {

    }

    @Override
    public ObservableBoolean getEmpty() {
        return empty;
    }

    private final class ObtenerClientesVisitarLocalObserver extends DefaultObserver<List<Cliente>> {

        @Override
        public void onComplete() {
//            unlock();
        }

        @Override
        public void onError(Throwable e) {
//            unlock();
        }

        @Override
        public void onNext(List<Cliente> result) {
            try {
                if (result!=null && !result.isEmpty()) {
                    adapter.setItems(mapper.transform(result));
                    adapter.notifyDataSetChanged();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
