package com.kyaemp.perufarma.demo.ui.lista;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.ListaActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.ui.lista.recyclerview.PointAdapter;

import javax.inject.Inject;

//import com.squareup.leakcanary.RefWatcher;


public class ListaActivity extends BaseActivity<ListaActivityBinding, ListaMvvm.ViewModel> implements ListaMvvm.View {

    @Inject
    PointAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.lista_activity);
        configRecycler();
    }

    private void configRecycler() {
        binding.rvLista.setHasFixedSize(true);
        binding.rvLista.setLayoutManager(new LinearLayoutManager(this));
        binding.rvLista.setAdapter(adapter);
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        RefWatcher refWatcher = DemoApplication.getRefWatcher(this);
//        refWatcher.watch(this);
//    }
}