package com.kyaemp.perufarma.demo.ui.clientes.viewpager;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.qualifier.ActivityFragmentManager;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.hoy.HoyClientesFragment;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.mapa.MapaClientesFragment;

import javax.inject.Inject;


@PerActivity
public class ClientesViewPagerAdapter extends FragmentPagerAdapter {
    
    private final Resources res;

    @Inject
    public ClientesViewPagerAdapter(@ActivityFragmentManager FragmentManager fm, Resources res) {
        super(fm);
        this.res = res;
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) {
            return new HoyClientesFragment();
//        } else if(position == 1){
//            return new VisitarClientesFragment();
        }else{
            return new MapaClientesFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0) {
            return res.getString(R.string.tab_title_hoy);
//        } else if(position == 1) {
//            return res.getString(R.string.tab_title_visitar);
        }else{
            return res.getString(R.string.tab_title_mapa);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}
