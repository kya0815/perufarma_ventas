package com.kyaemp.perufarma.demo;

import android.app.Application;
import android.content.res.Resources;
import android.provider.Settings;

import com.kyaemp.perufarma.data.utiles.Constantes;
import com.kyaemp.perufarma.demo.injection.components.AppComponent;
import com.kyaemp.perufarma.demo.injection.components.DaggerAppComponent;
import com.kyaemp.perufarma.demo.injection.modules.AppModule;
import com.kyaemp.perufarma.demo.services.SocketManager;

import static com.kyaemp.perufarma.demo.utils.Constantes.PREF_DEVICEID;
//import com.squareup.leakcanary.LeakCanary;
//import com.squareup.leakcanary.RefWatcher;


public class DemoApplication extends Application {

    private static DemoApplication sInstance = null;

    private static AppComponent sAppComponent = null;
//    private RefWatcher refWatcher;

//    public static RefWatcher getRefWatcher(Context context) {
//        DemoApplication application = (DemoApplication) context.getApplicationContext();
//        return application.refWatcher;
//    }

    public static DemoApplication getInstance() {
        return sInstance;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static Resources getRes() {
        return sInstance.getResources();
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        refWatcher = LeakCanary.install(this);
//        if(LeakCanary.isInAnalyzerProcess(this)) return;
        String id =Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        getSharedPreferences(Constantes.PREFERENCES,MODE_PRIVATE).edit()
                .putString(PREF_DEVICEID,id )
                .apply();
        SocketManager.getInstance().setDeviceID(id);
        sInstance = this;
        sAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

}