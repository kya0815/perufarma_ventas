package com.kyaemp.perufarma.demo.ui.login;


import android.content.Intent;
import android.databinding.ObservableBoolean;

import com.kyaemp.perufarma.demo.model.observables.UserLoginModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface LoginMvvm {

    interface View extends MvvmView {

    }

    interface ViewModel extends IViewModel<View> {
        // Properties
        ObservableBoolean isLoaded();

        UserLoginModel getUserLogin();

        void onClickIniciarSesion(android.view.View v);

        void onClickSignup(android.view.View v);

        void onActivityResult(int requestCode, int resultCode, Intent data);
        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);
    }
}
