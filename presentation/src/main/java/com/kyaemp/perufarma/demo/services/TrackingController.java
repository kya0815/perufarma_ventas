/*
 * Copyright 2015 Anton Tananaev (anton.tananaev@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.demo.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.kyaemp.perufarma.demo.model.mapper.PositionModelDataMapper;
import com.kyaemp.perufarma.demo.model.normal.PositionModel;
import com.kyaemp.perufarma.demo.model.normal.RespuestaServerSocket;
import com.kyaemp.perufarma.demo.utils.Constantes;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.DeletePositionDBUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionBDUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionServerUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerLastPositionServerUseCase;
import com.kyaemp.perufarma.domain.model.Position;

import javax.inject.Singleton;

import io.socket.client.Ack;

import static com.kyaemp.perufarma.data.utiles.Constantes.PREF_KEY_GUARDAR_GPS;
import static com.kyaemp.perufarma.data.utiles.Constantes.SOCKET_GET_TIEMPO_GUARDADO;

@Singleton
public class TrackingController implements PositionProvider.PositionListener, NetworkManager.NetworkHandler {

    private static final String TAG = TrackingController.class.getSimpleName();
    private static final int RETRY_DELAY = 30 * 1000;
    private static final int WAKE_LOCK_TIMEOUT = 60 * 1000;

    private boolean isOnline;
    DeletePositionDBUseCase deletePositionDBUseCase;
    GuardarPositionBDUseCase guardarPositionBDUseCase;
    GuardarPositionServerUseCase guardarPositionServerUseCase;
    ObtenerLastPositionServerUseCase obtenerLastPositionServerUseCase;
    PositionModelDataMapper mapper;

    private Context context;
    private Handler handler;
    private SharedPreferences preferences;


    private PositionProvider positionProvider;
    private NetworkManager networkManager;

    private PowerManager.WakeLock wakeLock;


    private void lock() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            wakeLock.acquire();
        } else {
            wakeLock.acquire(WAKE_LOCK_TIMEOUT);
        }
    }

    private void unlock() {
        if (wakeLock.isHeld()) {
            wakeLock.release();
        }
    }
    public TrackingController(Context context,DeletePositionDBUseCase deletePositionDBUseCase,
                              GuardarPositionBDUseCase guardarPositionBDUseCase,GuardarPositionServerUseCase guardarPositionServerUseCase,
            ObtenerLastPositionServerUseCase obtenerLastPositionServerUseCase,PositionModelDataMapper mapper) {
        this.context = context;
        handler = new Handler();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        positionProvider = new SimplePositionProvider(context, this);
        networkManager = new NetworkManager(context, this);
        isOnline = networkManager.isOnline();
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
        this.deletePositionDBUseCase=deletePositionDBUseCase;
        this.guardarPositionBDUseCase=guardarPositionBDUseCase;
        this.guardarPositionServerUseCase=guardarPositionServerUseCase;
        this.obtenerLastPositionServerUseCase=obtenerLastPositionServerUseCase;
        this.mapper=mapper;
    }

    public void start() {
//        iniciarDependencias();
        if (isOnline) {

            try {
                SocketManager.getInstance().connectSocket(Constantes.URL_NODEJS);
                SocketManager.getInstance().getSocket().emit(SOCKET_GET_TIEMPO_GUARDADO, (Ack) args -> {
                    Gson gson = new Gson();
                    RespuestaServerSocket result = gson.fromJson(String.valueOf(args[0]),RespuestaServerSocket.class);
                    if (result.getError()==0) {
                        context.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit()
                                .putInt(PREF_KEY_GUARDAR_GPS,Integer.parseInt(result.getData())).apply();
                        Log.e(TAG,"tiempodeGuardado: "+Integer.parseInt(result.getData()));
                    } else {
                        Log.e(TAG,"Error al procesar socket getTiempoGuardado");
                    }
                });
                read();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            positionProvider.startUpdates();
        } catch (SecurityException e) {
            Log.w(TAG, e);
        }
        networkManager.start();
    }



    public void stop() {
        networkManager.stop();
        try {
            positionProvider.stopUpdates();
        } catch (SecurityException e) {
            Log.w(TAG, e);
        }
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onPositionUpdate(PositionModel position) {
        if (position != null) {
            Log.e("traking","posicion actualizada");
            send(position);
        }
    }

    @Override
    public void onNetworkUpdate(boolean isOnline) {
        if (!this.isOnline && isOnline) {
            read();
        }
        this.isOnline = isOnline;
    }

    private void log(String action, PositionModel position) {
        if (position != null) {
            action += " (" +
                    "id:" + position.getId() +
                    " time:" + position.getTime().getTime() / 1000 +
                    " lat:" + position.getLatitude() +
                    " lon:" + position.getLongitude() +
                    " guardar:" + position.getGuardar() + ")";
        }
        Log.d(TAG, action);
    }

    private void write(PositionModel position) {
        log("write", position);
        try {
            this.guardarPositionBDUseCase.execute(new GuardarPositionServerObserver(),
                    GuardarPositionBDUseCase.Params.position(mapper.transform(position)));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void read() {
        log("read", null);
        lock();
        PositionModel item =null;
        try {
            obtenerLastPositionServerUseCase.execute(new ObtenerLastPositionObserver(),null);

        } catch (Exception e) {
            e.printStackTrace();
            unlock();
        }
    }

    private void delete(final PositionModel position) {
        log("delete", position);
        lock();
        try {
            deletePositionDBUseCase.execute(new DeletePositionObserver(),DeletePositionDBUseCase.Params.id(position.getId()));
        } catch (Exception e) {
            e.printStackTrace();
            unlock();
        }
    }

    private void send(final PositionModel position) {
        log("send", position);
        lock();

        if(isOnline){
            this.guardarPositionServerUseCase.execute(
                    new GuardarPositionServerObserver(),
                    GuardarPositionServerUseCase.Params.position(mapper.transform(position)));
        }else if(position.getGuardar()==1){
            write(position);
            unlock();
        }
    }

    private void retry(final PositionModel position) {
        log("retry", null);
        handler.postDelayed(() -> {
            if (isOnline && position !=null) {
                send(position);
            }
        }, RETRY_DELAY);
    }

    private final class GuardarPositionServerObserver extends DefaultObserver<Boolean> {

        @Override
        public void onComplete() {
            unlock();
        }

        @Override
        public void onError(Throwable e) {
            unlock();
        }

        @Override
        public void onNext(Boolean result) {
            try {
                if (result!=null && result && isOnline) {
                    read();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            unlock();
        }
    }
    private final class ObtenerLastPositionObserver extends DefaultObserver<Position> {

        @Override
        public void onComplete() {
            unlock();
        }

        @Override
        public void onError(Throwable e) {
            unlock();
        }

        @Override
        public void onNext(Position result) {
            try {
                if(result!=null){
                    send(mapper.transform(result));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            unlock();
        }
    }
    private final class DeletePositionObserver extends DefaultObserver<Boolean> {

        @Override
        public void onComplete() {
            read();
            unlock();
        }

        @Override
        public void onError(Throwable e) {
            read();
            unlock();
        }

        @Override
        public void onNext(Boolean result) {
            try {
                if(result!=null && result && isOnline){
                    read();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            unlock();
        }
    }
}
