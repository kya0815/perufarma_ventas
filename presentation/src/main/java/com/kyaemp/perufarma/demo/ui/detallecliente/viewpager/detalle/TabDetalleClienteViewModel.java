package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.detalle;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.FragmentNavigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseFragmentViewModel;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.ObtenerDetalleClienteUseCase;
import com.kyaemp.perufarma.domain.model.Cliente;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;


@PerFragment
public class TabDetalleClienteViewModel extends BaseFragmentViewModel<ITabDetalleClienteViewModel.View> implements ITabDetalleClienteViewModel,ITabDetalleClienteViewModel.ViewModel {

    private ObtenerDetalleClienteUseCase obtenerDetalleCliente;
    private ClienteModelDataMapper mapper;
    Resources res;
    List<Cliente> clientes= Collections.emptyList();
    ClientesModel cliente;
    private final ObservableBoolean loaded;

    @Inject
    public TabDetalleClienteViewModel(Context context, FragmentNavigator navigator, ObtenerDetalleClienteUseCase obtenerDetalleCliente,
                                      ClienteModelDataMapper mapper, Resources res) {
        super(context,navigator);
        this.mapper = mapper;
        this.obtenerDetalleCliente = obtenerDetalleCliente;
        this.loaded = new ObservableBoolean(false);
        this.res = res;
    }

    @Override
    public void attachView(View mvvmView, @Nullable Bundle savedInstanceState) {
        super.attachView(mvvmView, savedInstanceState);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    @Override
    public void setDataCliente(Cliente cliente) {
        this.cliente=mapper.transform(cliente);
    }

    @Override
    public ClientesModel getCliente() {
        return cliente;
    }

    public void setCliente(ClientesModel cliente) {
        this.cliente=cliente;
        notifyChange();
    }

    @Override
    public void setId(int id) {
        showLoading();
        obtenerDetalleCliente.execute(new ObtenerDetalleClienteObserver(),
                ObtenerDetalleClienteUseCase.Params.id(id));
    }

    private final class ObtenerDetalleClienteObserver extends DefaultObserver<Cliente> {

        @Override
        public void onComplete() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            toast(e.getMessage());
        }

        @Override
        public void onNext(Cliente result) {
            ClientesModel cliente= mapper.transform(result);
            if (result != null &&  cliente!=null) {
                setCliente(cliente);
            } else {
                toast("No existe el cliente");
            }
        }
    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    private void showLoading(){
        loaded.set(true);
    }

    private void hideLoading(){
        loaded.set(false);
    }

    @Override
    public void onActivityResultlistener(int requestCode, int resultCode, Intent data) {

    }
}
