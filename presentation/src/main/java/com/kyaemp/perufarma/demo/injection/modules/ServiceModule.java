package com.kyaemp.perufarma.demo.injection.modules;

import android.app.Service;
import android.content.Context;

import com.kyaemp.perufarma.demo.injection.qualifier.ServiceContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerService;
import com.kyaemp.perufarma.demo.model.mapper.PositionModelDataMapper;
import com.kyaemp.perufarma.demo.services.TrackingController;
import com.kyaemp.perufarma.domain.interactor.DeletePositionDBUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionBDUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionServerUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerLastPositionServerUseCase;

import dagger.Module;
import dagger.Provides;


@Module
public class ServiceModule {

    private final Service mServices;

    public ServiceModule(Service mServices) {
        this.mServices = mServices;
    }

    @Provides
    @PerService
    @ServiceContext
    Context provideServiceContext() {
        return mServices;
    }

    @Provides
    TrackingController provideTrackingController(DeletePositionDBUseCase deletePositionDBUseCase , GuardarPositionBDUseCase guardarPositionBDUseCase,
                                                 GuardarPositionServerUseCase guardarPositionServerUseCase, ObtenerLastPositionServerUseCase obtenerLastPositionServerUseCase,
                                                 PositionModelDataMapper mapper) {
        return new TrackingController(mServices,deletePositionDBUseCase,guardarPositionBDUseCase,guardarPositionServerUseCase,obtenerLastPositionServerUseCase,mapper);
    }
}
