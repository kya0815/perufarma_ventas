package com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview;

import android.view.View;

import com.kyaemp.perufarma.demo.databinding.HomeActivityBinding;
import com.kyaemp.perufarma.demo.databinding.ItemProductosBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseViewHolder;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

public class PedidoItemViewHolder extends BaseViewHolder<ItemProductosBinding, PedidoItemMvvm.ViewModel> implements MvvmView {

    public PedidoItemViewHolder(View v) {
        super(v);

        viewHolderComponent().inject(this);
        bindContentView(v);
    }
}
