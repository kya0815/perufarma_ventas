package com.kyaemp.perufarma.demo.ui.base.navigator;


public interface DialogNavigator extends Navigator {

    void dismiss();
}
