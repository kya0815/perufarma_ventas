package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.PedidosDetalleClienteBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseFragment;
import com.kyaemp.perufarma.demo.ui.clientes.recyclerview.ClienteItemAdapter;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.DetalleClienteViewPagerAdapter;

import javax.inject.Inject;

public class PedidosClienteFragment extends BaseFragment<PedidosDetalleClienteBinding,IPedidosClienteViewModel.ViewModel>
        implements IPedidosClienteViewModel.View,DetalleClienteViewPagerAdapter.listener {
//    @Inject
    private DetalleClienteViewPagerAdapter adapter2;
    @Inject
    public ClienteItemAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.pedidos_detalle_cliente);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
        llamarId();
    }
    private void llamarId() {
        if(adapter2!=null){
            setId(adapter2.idActual);
        }
    }
    public void setId(int id) {
        viewModel.setId(id);
    }
    public void setAdapter(DetalleClienteViewPagerAdapter adapter) {
        this.adapter2=adapter;
    }

    @Override
    public void onActivityResultlistener(int requestCode, int resultCode, Intent data) {

        viewModel.onActivityResultlistener(requestCode, resultCode, data);
    }
}
