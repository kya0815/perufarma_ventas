package com.kyaemp.perufarma.demo.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.kyaemp.perufarma.data.utiles.Constantes;
import com.kyaemp.perufarma.demo.DemoApplication;
import com.kyaemp.perufarma.demo.injection.components.DaggerServiceComponent;
import com.kyaemp.perufarma.demo.injection.modules.ServiceModule;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SocketServices extends Service implements OnSocketConnectionListener {
    private static final String TAG = "SocketServices";

    @Inject
    public TrackingController trackingController;
    @Inject
    Context context;
    public SocketServices() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onCreate() {
        iniciarDependencias();
        Log.d(TAG, "Servicio creado...");
//        trackingController= new TrackingController(this,);
        trackingController.start();
    }
    private void iniciarDependencias() {
        DaggerServiceComponent.builder()
                .serviceModule(new ServiceModule(this))
                .appComponent(DemoApplication.getAppComponent())
                .build().inject(this);
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Servicio iniciado...");
        SocketManager.getInstance().setSocketConnectionListener(this);
        SocketManager.getInstance()
                .connectSocket(Constantes.URL_NODEJS);
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        //Restart the service once it has been killed android

        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 100, restartServicePI);

    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Servicio destruido...");
        SocketManager.getInstance().destroy();
        stopForeground(true);
        if (trackingController != null) {
            trackingController.stop();
        }
    }

    @Override
    public void onSocketEventFailed() {

    }

    @Override
    public void onSocketConnectionStateChange(int socketState) {
        switch (socketState) {
            case SocketManager.STATE_DISCONNECTED:
                break;
            case SocketManager.STATE_CONNECTED:

                break;
        }
    }

    @Override
    public void onSocketSendCorreoPrimerChat() {

    }

    @Override
    public void onInternetConnectionStateChange(int socketState) {

    }
}
