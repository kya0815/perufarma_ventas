package com.kyaemp.perufarma.demo.ui.splash;


import android.view.MotionEvent;

import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface SplashMvvm {

    interface View extends MvvmView {

    }

    interface ViewModel extends IViewModel<View> {
        // Properties

        void onTouchScreeen(MotionEvent v);

    }
}
