package com.kyaemp.perufarma.demo.ui.home.recyclerview;

import android.content.Context;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerViewHolder;
import com.kyaemp.perufarma.demo.model.observables.MenuItemModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDemoViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.ClientesActivity;

import javax.inject.Inject;



@PerViewHolder
public class MenuItemViewModel extends BaseDemoViewModel<MvvmView> implements MenuItemMvvm.ViewModel {

    private MenuItemModel menu;

    @Inject
    public MenuItemViewModel(@AppContext Context context, Navigator navigator) {
        super(context, navigator);
    }


    @Override
    public void update(MenuItemModel menu) {
        this.menu=menu;
    }

    @Override
    public MenuItemModel getMenu() {
        return menu;
    }

    @Override
    public void onClickMenu(View v) {
        switch (menu.getId()){
            case 0:
                navigator.startActivity(ClientesActivity.class);
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
        }
    }
}
