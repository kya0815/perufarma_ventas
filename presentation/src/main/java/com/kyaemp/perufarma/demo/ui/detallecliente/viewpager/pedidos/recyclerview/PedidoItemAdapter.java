package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerFragment
public class PedidoItemAdapter extends RecyclerView.Adapter<PedidosItemViewHolder> {

    private List<Pedido> items = Collections.emptyList();
    private Cliente cliente;

    @Inject
    public PedidoItemAdapter() {
    }

    public void setItems(List<Pedido> items) {
        this.items = items;
    }

    @Override
    public PedidosItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_pedido, viewGroup, false);

        return new PedidosItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PedidosItemViewHolder viewHolder, int position) {
        viewHolder.viewModel().update(items.get(position),cliente);
        viewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
