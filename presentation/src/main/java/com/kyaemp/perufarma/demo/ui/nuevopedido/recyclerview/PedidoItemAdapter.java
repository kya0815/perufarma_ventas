package com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

@PerActivity
public class PedidoItemAdapter extends RecyclerView.Adapter<PedidoItemViewHolder> implements Filterable {

    private List<CatalogoModel> items = Collections.emptyList();
    private List<CatalogoModel> itemsFilterable= new ArrayList<>();

    @Inject
    public PedidoItemAdapter() {
    }

    public void setItems(List<CatalogoModel> items) {
        this.items = items;
        this.itemsFilterable = items;
    }

    @Override
    public PedidoItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_productos, viewGroup, false);

        return new PedidoItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PedidoItemViewHolder viewHolder, int position) {
        viewHolder.viewModel().update(itemsFilterable.get(position));
        viewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return itemsFilterable.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                Log.e("filter", "performFiltering: "+charSequence );
                if (charString.isEmpty()) {
                    itemsFilterable = items;
                } else {
                    List<CatalogoModel> filteredList = new ArrayList<>();
                    for (CatalogoModel row : items) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getProducto().toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getId()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    itemsFilterable = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = itemsFilterable;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                itemsFilterable = (List<CatalogoModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
