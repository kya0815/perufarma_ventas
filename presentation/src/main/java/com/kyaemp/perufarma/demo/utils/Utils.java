package com.kyaemp.perufarma.demo.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import javax.inject.Singleton;


/**
 * Created by pedro.zevallos on 10/07/2017.
 */
@Singleton
public class Utils {
    // Usage: createViewHolder(viewGroup, R.layout.my_layout, MyViewHolder::new);
    public static <T extends RecyclerView.ViewHolder> T createViewHolder(@NonNull ViewGroup viewGroup, @LayoutRes int layoutResId, @NonNull PlainFunction<View, T> newViewHolderAction) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(layoutResId, viewGroup, false);

        return newViewHolderAction.apply(view);
    }

    // Tries to cast an Activity Context to another type
    @SuppressWarnings("unchecked")
    @Nullable
    public static <T> T castActivityFromContext(Context context, Class<T> castClass) {
        if (castClass.isInstance(context)) {
            return (T) context;
        }

        while (context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();

            if (castClass.isInstance(context)) {
                return (T) context;
            }
        }

        return null;
    }

    public static void setFondoCompleto(ImageView view, String url) {
        Glide.with(view.getContext())
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(view);
    }

    public static String idDevice(){
        return "";
    }
}
