package com.kyaemp.perufarma.demo.ui.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.IntegerRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;

import com.kyaemp.perufarma.demo.BR;
import com.kyaemp.perufarma.demo.injection.components.DaggerDialogComponent;
import com.kyaemp.perufarma.demo.injection.components.DialogComponent;
import com.kyaemp.perufarma.demo.injection.modules.DialogModule;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.MvvmViewModel;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.NoOpViewModel;

import javax.inject.Inject;

//import com.squareup.leakcanary.RefWatcher;


/* Base class for Fragments when using a view model with data binding.
 * This class provides the binding and the view model to the subclass. The
 * view model is injected and the binding is created when the content view is set.
 * Each subclass therefore has to call the following code in onCreateView():
 *    if(viewModel == null) { fragmentComponent().inject(this); }
 *    return setAndBindContentView(inflater, container, R.layout.my_fragment_layout, savedInstanceState);
 *
 * After calling these methods, the binding and the view model is initialized.
 * saveInstanceState() and restoreInstanceState() methods of the view model
 * are automatically called in the appropriate lifecycle events when above calls
 * are made.
 *
 * Your subclass must implement the MvvmView implementation that you use in your
 * view model. */
public abstract class BaseDialog<B extends ViewDataBinding, V extends MvvmViewModel> extends DialogFragment {

    protected B binding;
    @Inject
    protected V viewModel;

//    @Inject RefWatcher refWatcher;

    private DialogComponent mDialogComponent;

    @Override
    @CallSuper
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (viewModel != null) {
            viewModel.saveInstanceState(outState);
        }
    }

    @Override
    @CallSuper
    public void onDestroyView() {
        super.onDestroyView();
        if (viewModel != null) {
            viewModel.detachView();
//            if(refWatcher != null) { refWatcher.watch(viewModel);}
        }
        binding = null;
        viewModel = null;
    }

    @Override
    @CallSuper
    public void onDestroy() {
        super.onDestroy();
//        if(refWatcher != null) {
//            refWatcher.watch(this);
//            refWatcher.watch(mDialogComponent);
//        }
        mDialogComponent = null;
    }


    protected final DialogComponent dialogComponent() {
        if (mDialogComponent == null) {
            mDialogComponent = DaggerDialogComponent.builder()
                    .dialogModule(new DialogModule(this))
                    .activityComponent(((BaseActivity) getActivity()).activityComponent())
                    .build();
        }

        return mDialogComponent;
    }

    /* Sets the content view, creates the binding and attaches the view to the view model */
    protected final View setAndBindContentView(@Nullable Bundle savedInstanceState, @LayoutRes int layoutResID) {
        if (viewModel == null) {
            throw new IllegalStateException("viewModel must already be set via injection");
        }
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), layoutResID, null, false);
        binding.setVariable(BR.vm, viewModel);

        try {
            //noinspection unchecked
            viewModel.attachView((MvvmView) this, savedInstanceState);
        } catch (ClassCastException e) {
            if (!(viewModel instanceof NoOpViewModel)) {
                throw new RuntimeException(getClass().getSimpleName() + " must implement MvvmView subclass as declared in " + viewModel.getClass().getSimpleName());
            }
        }

        return binding.getRoot();
    }

    public int dimen(@DimenRes int resId) {
        return (int) getResources().getDimension(resId);
    }

    public int color(@ColorRes int resId) {
        return getResources().getColor(resId);
    }

    public int integer(@IntegerRes int resId) {
        return getResources().getInteger(resId);
    }

    public String string(@StringRes int resId) {
        return getResources().getString(resId);
    }
}
