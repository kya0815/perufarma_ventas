package com.kyaemp.perufarma.demo.ui.map.guardarpoint;


import android.databinding.ObservableBoolean;

import com.kyaemp.perufarma.demo.model.observables.GuardarPointModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;


/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface GuardarPointMvvm {

    interface View extends MvvmView {

    }

    interface ViewModel extends IViewModel<View> {
        // Properties
        ObservableBoolean isLoaded();

        GuardarPointModel getEntidad();

        void setPoint(Double lat, Double lon);

        void onClickAceptar(android.view.View view);

        void onClickCancelar(android.view.View view);
    }
}
