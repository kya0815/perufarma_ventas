package com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview;

import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;


/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
public interface PedidoItemMvvm {

    interface ViewModel extends IViewModel<MvvmView> {


        void update(CatalogoModel producto);

        CatalogoModel getProducto();
        void OnClickNuevoPedido(android.view.View v);

        void OnClickProducto(android.view.View v);
    }
}
