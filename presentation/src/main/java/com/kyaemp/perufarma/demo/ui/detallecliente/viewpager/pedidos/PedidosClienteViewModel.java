package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.FragmentNavigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseFragmentViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview.PedidoItemAdapter;
import com.kyaemp.perufarma.demo.ui.nuevopedido.NuevoPedidoActivity;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.ObtenerDetalleClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerPedidosClienteUseCase;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

//import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.nuevopedido.NuevoPedidoDialog;

@PerFragment
public class PedidosClienteViewModel extends BaseFragmentViewModel<IPedidosClienteViewModel.View> implements IPedidosClienteViewModel.ViewModel {


    public static final int COD_PEDIDO = 45;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ObtenerPedidosClienteUseCase obtenerPedidosUseCase;
    private ObtenerDetalleClienteUseCase obtenerDetalleCliente;
    @Inject
    public PedidoItemAdapter adapter;
    private ClienteModelDataMapper mapper;
    private ObservableBoolean empty;
    private int idPedido;
    private final ObservableBoolean loaded;
    ClientesModel cliente;

    @Inject
    public PedidosClienteViewModel(Context context, FragmentNavigator navigator,
                                   PedidoItemAdapter adapter, ObtenerPedidosClienteUseCase obtenerPedidosUseCase,
                                   ClienteModelDataMapper mapper,ObtenerDetalleClienteUseCase obtenerDetalleCliente) {
        super(context,navigator);
        this.adapter = adapter;
        this.mapper = mapper;
        this.obtenerDetalleCliente = obtenerDetalleCliente;
        this.obtenerPedidosUseCase = obtenerPedidosUseCase;
        this.empty=new ObservableBoolean(false);
        this.loaded=new ObservableBoolean(false);
    }

    @Override
    public void attachView(IPedidosClienteViewModel.View mvvmView, @Nullable Bundle savedInstanceState) {
        super.attachView(mvvmView, savedInstanceState);
    }

    @Override
    public void detachView() {
        super.detachView();
        compositeDisposable.clear();
    }


    @Override
    public PedidoItemAdapter getAdapter() {
        return adapter;
    }


    @Override
    public ObservableBoolean getEmpty() {
        return empty;
    }

    @Override
    public void onClickNewPedido(View v) {
        navigator.startActivityForResult(NuevoPedidoActivity.class,cliente.getId(),COD_PEDIDO);
    }

    private void setEmty(boolean value){
        empty.set(value);
    }

    @Override
    public void setId(int id) {
        idPedido=id;
       reload();
    }

    private final class ObtenerDetalleClienteObserver extends DefaultObserver<Cliente> {

        @Override
        public void onComplete() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            toast(ctx,e.getMessage());
        }

        @Override
        public void onNext(Cliente result) {
            ClientesModel cliente= mapper.transform(result);
            if (result != null &&  cliente!=null) {
                setCliente(cliente);
                adapter.setCliente(result);
                adapter.notifyDataSetChanged();
            } else {
                toast(ctx,"No existe el cliente");
            }
        }
    }

    @Override
    public ClientesModel getCliente() {
        return cliente;
    }

    public void setCliente(ClientesModel cliente) {
        this.cliente=cliente;
        notifyChange();
    }

    @Override
    public ObservableBoolean getLoaded() {
        return loaded;
    }

    private void showLoading(){
        loaded.set(true);
    }

    private void hideLoading(){
        loaded.set(false);
    }

    private final class ObtenerPedidosObserver extends DefaultObserver<List<Pedido>> {

        @Override
        public void onComplete() {
//            unlock();
        }

        @Override
        public void onError(Throwable e) {
//            unlock();
        }

        @Override
        public void onNext(List<Pedido> result) {
            try {
                if (result!=null && !result.isEmpty()) {
                    setEmty(false);
                    adapter.setItems(result);
                    adapter.notifyDataSetChanged();
                }else{
                    setEmty(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResultlistener(int requestCode, int resultCode, Intent data) {
        if(requestCode==COD_PEDIDO){
            reload();
        }
    }

    private void reload() {
        obtenerDetalleCliente.execute(new ObtenerDetalleClienteObserver(),
                ObtenerDetalleClienteUseCase.Params.id(idPedido));
        obtenerPedidosUseCase.execute(new ObtenerPedidosObserver(),
                ObtenerPedidosClienteUseCase.Params.id(idPedido));
    }


}
