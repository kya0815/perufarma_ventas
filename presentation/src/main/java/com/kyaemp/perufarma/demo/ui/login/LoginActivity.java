package com.kyaemp.perufarma.demo.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.LoginActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;


public class LoginActivity extends BaseActivity<LoginActivityBinding, LoginMvvm.ViewModel> implements LoginMvvm.View {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.login_activity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}