package com.kyaemp.perufarma.demo.ui.clientes.viewpager.hoy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.kyaemp.perufarma.demo.ui.clientes.viewpager.ClientesFragment;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.ClientesView;

public class HoyClientesFragment extends ClientesFragment<IHoyClientesViewModel> implements ClientesView {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
