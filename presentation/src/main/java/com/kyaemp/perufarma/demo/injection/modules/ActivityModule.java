package com.kyaemp.perufarma.demo.injection.modules;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.kyaemp.perufarma.demo.injection.qualifier.ActivityContext;
import com.kyaemp.perufarma.demo.injection.qualifier.ActivityFragmentManager;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.ui.base.navigator.ActivityNavigator;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;

import dagger.Module;
import dagger.Provides;


@Module
public class ActivityModule {

    private final BaseActivity mActivity;

    public ActivityModule(BaseActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    @ActivityContext
    Context provideActivityContext() {
        return mActivity;
    }

    @Provides
    @PerActivity
    @ActivityFragmentManager
    FragmentManager provideFragmentManager() {
        return mActivity.getSupportFragmentManager();
    }

    @Provides
    @PerActivity
    Navigator provideNavigator() {
        return new ActivityNavigator(mActivity);
    }

}
