package com.kyaemp.perufarma.demo.ui.splash;

import android.content.Context;
import android.view.MotionEvent;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.demo.ui.home.HomeActivity;
import com.kyaemp.perufarma.demo.ui.login.LoginActivity;

import javax.inject.Inject;

@PerActivity
public class SplashViewModel extends BaseActivityViewModel<SplashMvvm.View> implements SplashMvvm.ViewModel {

    @Inject
    SplashViewModel(@AppContext Context appContext, Navigator navigator) {
        super(appContext, navigator);
        iniciarView();
    }

    private void iniciarView() {
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    flujo();
                }
            }
        };
        timerThread.start();
    }

    private void flujo() {
        if (navigator.isLoggin()) {
            navigator.startActivity(HomeActivity.class);

        } else {
            navigator.startActivity(LoginActivity.class);
        }
        navigator.finishActivity();
    }

    @Override
    public void onTouchScreeen(MotionEvent v) {
            flujo();
    }
}
