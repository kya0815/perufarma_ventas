package com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerViewHolder;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDemoViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.DetalleClienteActivity;

import javax.inject.Inject;


@PerViewHolder
public class PedidoItemViewModel extends BaseDemoViewModel<MvvmView> implements PedidoItemMvvm.ViewModel {

    private CatalogoModel producto;
    private ClienteModelDataMapper mapper;

    @Inject
    public PedidoItemViewModel(@AppContext Context context, Navigator navigator, ClienteModelDataMapper mapper) {
        super(context, navigator);
        this.mapper=mapper;
    }

    @Override
    public void update(CatalogoModel producto) {
        this.producto=producto;
        notifyChange();
    }

    @Override
    public CatalogoModel getProducto() {
        return producto;
    }

    @Override
    public void OnClickNuevoPedido(View v) {
    }

    @Override
    public void OnClickProducto(View v) {
        navigator.setProductoSelect(producto);
    }
}
