package com.kyaemp.perufarma.demo.model.observables;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.drawable.Drawable;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by pedro.zevallos on 26/08/2017.
 */

public class MenuItemModel extends BaseObservable {
    private int id;
    private String nombre;
    private Drawable drawable;

    public MenuItemModel(int id, String nombre, Drawable drawableId) {
        this.id = id;
        this.nombre = nombre;
        this.drawable = drawableId;
    }

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
        notifyPropertyChanged(BR.nombre);
    }

    @Bindable
    public Drawable getDrawable() {
        return drawable;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
        notifyPropertyChanged(BR.drawable);
    }
}
