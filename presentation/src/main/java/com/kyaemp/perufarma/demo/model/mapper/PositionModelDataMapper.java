/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.demo.model.mapper;

import com.kyaemp.perufarma.demo.model.normal.PositionModel;
import com.kyaemp.perufarma.domain.model.Position;

import java.util.ArrayList;
import java.util.List;


public class PositionModelDataMapper {

    public PositionModel transform(Position item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new PositionModel(item.getId(),item.getDeviceId(),item.getTime(),item.getLatitude(),item.getLongitude(),item.getAltitude(),item.getSpeed(),item.getCourse(),item.getBattery(),item.getNombre(),item.getGuardar());
    }

    public Position transform(PositionModel item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new Position(item.getId(),item.getDeviceId(),item.getTime(),item.getLatitude(),item.getLongitude(),item.getAltitude(),item.getSpeed(),item.getCourse(),item.getBattery(),item.getNombre(),item.getGuardar());
    }

    public <R, T> List<R> transform(List<T> c) {
        List<R> items = new ArrayList<>();
        if (c != null && !c.isEmpty()) {
            for (T t : c) {
                items.add(transform(t));
            }
        }
        return items;
    }

    private <R, T> R transform(T t) {
        if (t instanceof Position) {
            return (R) transform((Position) t);
        } else if (t instanceof PositionModel) {
            return (R) transform((PositionModel) t);
        }
        return null;
    }
}
