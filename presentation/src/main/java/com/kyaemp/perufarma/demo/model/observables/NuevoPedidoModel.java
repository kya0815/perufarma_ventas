package com.kyaemp.perufarma.demo.model.observables;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */

public class NuevoPedidoModel extends BaseObservable {
    private long idItem;
    private int cantidad;
    private String nombre;

    public NuevoPedidoModel(long idItem, int cantidad, String nombre) {
        this.idItem = idItem;
        this.cantidad = cantidad;
        this.nombre = nombre;
    }

    @Bindable
    public long getIdItem() {
        return idItem;
    }

    public void setIdItem(long idItem) {
        this.idItem = idItem;
        notifyPropertyChanged(BR.idItem);
    }

    @Bindable
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
        notifyPropertyChanged(BR.cantidad);
    }

    @Bindable
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
        notifyPropertyChanged(BR.nombre);
    }
}
