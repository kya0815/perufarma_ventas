package com.kyaemp.perufarma.demo.ui.home.recyclerview;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerViewHolder;
import com.kyaemp.perufarma.demo.model.observables.RelevamientoModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDemoViewModel;

import javax.inject.Inject;


@PerViewHolder
public class RelevamientoViewModel extends BaseDemoViewModel<MvvmView> implements RelevamientoMvvm.ViewModel {

    private final ObservableBoolean loaded;
    private ObservableBoolean enviado;
    private RelevamientoModel relevamiento;
    private ObservableInt position;

    @Inject
    public RelevamientoViewModel(@AppContext Context context, Navigator navigator) {
        super(context, navigator);
        this.loaded = new ObservableBoolean(false);
        this.enviado = new ObservableBoolean(false);
        this.position = new ObservableInt(0);
    }

    @Override
    public void onEnviar(View v) {
//        guardarPoint();
    }


    @Override
    public void update(RelevamientoModel relevamiento) {
        this.relevamiento=relevamiento;
    }

    @Override
    public RelevamientoModel getRelevamiento() {
        return relevamiento;
    }

    @Override
    public void setPosition(int position) {
        this.position.set(position);
    }

    @Override
    public ObservableInt getPosition() {
        return position;
    }

    @Override
    public ObservableBoolean getPrimero() {
        return new ObservableBoolean(position.get() == 0);
    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    @Override
    public ObservableBoolean isEnviado() {
        return enviado;
    }

    void showLoading() {
        loaded.set(true);
    }

    void hideLoading() {
        loaded.set(false);
    }

    void showError(String messages) {
        toast(null, messages);
    }


}
