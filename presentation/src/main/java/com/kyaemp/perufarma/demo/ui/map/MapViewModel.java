package com.kyaemp.perufarma.demo.ui.map;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.mapper.PointModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.PointModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.demo.ui.map.guardarpoint.GuardarPointDialog;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.ObtenerPuntosUseCase;
import com.kyaemp.perufarma.domain.model.Point;

import java.util.List;

import javax.inject.Inject;

@PerActivity
public class MapViewModel extends BaseActivityViewModel<MapMvvm.View> implements MapMvvm.ViewModel {

    private GoogleMap mMap;
    private PointModelDataMapper pointModelDataMapper;
    private ObtenerPuntosUseCase obtenerPuntosUseCase;
    private boolean isfirst = true;

    @Inject
    MapViewModel(@AppContext Context appContext, Navigator navigator, ObtenerPuntosUseCase obtenerPuntosUseCase, PointModelDataMapper pointModelDataMapper) {
        super(appContext, navigator);
        this.pointModelDataMapper = pointModelDataMapper;
        this.obtenerPuntosUseCase = obtenerPuntosUseCase;
    }


    private void iniciarViews() {
        if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().isMyLocationButtonEnabled();
            isfirst = false;
            obtenerPuntos();
            listenerLongClickMap();
        }

    }

    private void listenerLongClickMap() {
        mMap.setOnMapLongClickListener(latLng -> {
            navigator.startDialog(GuardarPointDialog.nuevaInstancia(latLng.latitude, latLng.longitude));
        });
    }

    private void obtenerPuntos() {
        this.obtenerPuntosUseCase.execute(new ObtenerPointsObserver(), null);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        iniciarViews();
    }

    private void crearPunto(Double lat, Double lon, String name, BitmapDescriptor markercolor) {
        markercolor = markercolor == null ? BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED) : markercolor;
        LatLng pos = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions()
                .position(pos)
                .title(name)
                .snippet("")
                .icon(markercolor));
        mMap.setInfoWindowAdapter(getInfoAdapterPoint());
    }

    private GoogleMap.InfoWindowAdapter getInfoAdapterPoint() {
        return new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                View v = navigator.inflate(R.layout.infowindows, null);

                TextView info = (TextView) v.findViewById(R.id.info);
                TextView title = (TextView) v.findViewById(R.id.title);

                info.setText(marker.getSnippet());
                title.setText(marker.getTitle());

                return v;
            }
        };
    }

    @Override
    public void reloadMap() {
        if (mMap != null) {
            mMap.clear();
            obtenerPuntos();
        }
    }

    private final class ObtenerPointsObserver extends DefaultObserver<List<Point>> {

        @Override
        public void onComplete() {

        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            MapViewModel.this.toast(e.getMessage());
        }

        @Override
        public void onNext(List<Point> result) {
            List<PointModel> points = pointModelDataMapper.transform(result);
            if (result != null && !result.isEmpty()) {
                BitmapDescriptor markercolor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE);
                for (PointModel pointModel : points) {
                    crearPunto(pointModel.getLat(), pointModel.getLon(), pointModel.getName(), markercolor);
                }
            } else {
                MapViewModel.this.toast("No tiene puntos guardados");
            }

        }
    }
}
