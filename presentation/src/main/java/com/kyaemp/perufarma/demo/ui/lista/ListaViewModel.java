package com.kyaemp.perufarma.demo.ui.lista;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.mapper.PointModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.PointModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.demo.ui.lista.recyclerview.PointAdapter;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.GuardarPuntosUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerPuntosUseCase;
import com.kyaemp.perufarma.domain.model.Point;

import java.util.List;

import javax.inject.Inject;

@PerActivity
public class ListaViewModel extends BaseActivityViewModel<ListaMvvm.View> implements ListaMvvm.ViewModel {

    private final ObservableBoolean loaded;
    private final PointAdapter pointAdapter;
    private final GuardarPuntosUseCase guardarPuntosUseCase;
    private final PointModelDataMapper pointModelDataMapper;
    private ObtenerPuntosUseCase obtenerPuntosUseCase;

    @Inject
    ListaViewModel(@AppContext Context appContext, Navigator navigator,
                   ObtenerPuntosUseCase obtenerPuntosUseCase, GuardarPuntosUseCase guardarPuntosUseCase,
                   PointModelDataMapper pointModelDataMapper, PointAdapter pointAdapter) {
        super(appContext, navigator);
        this.obtenerPuntosUseCase = obtenerPuntosUseCase;
        this.guardarPuntosUseCase = guardarPuntosUseCase;
        this.pointModelDataMapper = pointModelDataMapper;
        this.pointAdapter = pointAdapter;
        this.loaded = new ObservableBoolean(false);
        iniciarView();
    }

    private void iniciarView() {
        obtenerLista();
    }

    private void obtenerLista() {
        this.obtenerPuntosUseCase.execute(new ObtenerPointsObserver(), null);
    }

    void showLoading() {
        loaded.set(true);
    }

    void hideLoading() {
        loaded.set(false);
    }

    @Override
    public void showError() {

    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    @Override
    public void onClickLista(View v) {

    }

    private final class ObtenerPointsObserver extends DefaultObserver<List<Point>> {

        @Override
        public void onComplete() {

        }

        @Override
        public void onError(Throwable e) {
            ListaViewModel.this.toast(e.getMessage());
        }

        @Override
        public void onNext(List<Point> result) {
            List<PointModel> points = pointModelDataMapper.transform(result);
            if (result != null && !result.isEmpty()) {
                pointAdapter.setCountryList(points);
            } else {
                ListaViewModel.this.toast("No tiene puntos guardados");
            }

        }
    }
}
