package com.kyaemp.perufarma.demo.ui.clientes.recyclerview;

import android.view.View;

import com.kyaemp.perufarma.demo.databinding.HomeActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseViewHolder;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

public class ClienteItemViewHolder extends BaseViewHolder<HomeActivityBinding, ClienteItemMvvm.ViewModel> implements MvvmView {

    public ClienteItemViewHolder(View v) {
        super(v);

        viewHolderComponent().inject(this);
        bindContentView(v);
    }
}
