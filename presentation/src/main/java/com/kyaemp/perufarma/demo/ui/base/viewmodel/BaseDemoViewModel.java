package com.kyaemp.perufarma.demo.ui.base.viewmodel;

import android.content.Context;
import android.content.pm.PackageManager;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.model.observables.PointModel;
import com.kyaemp.perufarma.demo.model.observables.RelevamientoModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;


public abstract class BaseDemoViewModel<V extends MvvmView> extends BaseViewModel<V> implements IViewModel<V> {


    protected final Context ctx;
    protected final Navigator navigator;
    protected final boolean mapsAvailable;

    protected PointModel point;
    protected RelevamientoModel relevamiento;
    private boolean isLast = false;

    public BaseDemoViewModel(@AppContext Context context, Navigator navigator) {
        this.ctx = context.getApplicationContext();
        this.navigator = navigator;

        boolean mapsAvailable = false;

        try {
            context.getPackageManager().getPackageInfo("com.google.android.apps.maps", 0);
            mapsAvailable = true;
        } catch (PackageManager.NameNotFoundException ignore) {
        }

        this.mapsAvailable = mapsAvailable;
    }

    @Override
    public void update(PointModel point) {
        this.point = point;
    }

    public PointModel getPoint() {
        return point;
    }
//
//    @Override
//    public void update(RelevamientoModel relevamiento) {
//        this.relevamiento=relevamiento;
//    }

//    public RelevamientoModel getRelevamiento() {
//        return relevamiento;
//    }

    @Override
    protected void toast(Context con, String mensaje) {
        super.toast(ctx, mensaje);
    }
}
