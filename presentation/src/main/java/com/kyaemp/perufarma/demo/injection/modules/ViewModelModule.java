package com.kyaemp.perufarma.demo.injection.modules;

import com.kyaemp.perufarma.demo.ui.clientes.ClientesMvvm;
import com.kyaemp.perufarma.demo.ui.clientes.ClientesViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.recyclerview.ClienteItemMvvm;
import com.kyaemp.perufarma.demo.ui.clientes.recyclerview.ClienteItemViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.hoy.HoyClientesViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.hoy.IHoyClientesViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.mapa.IMapaClientesViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.mapa.MapaClientesViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.visitar.IVisitarClientesViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.visitar.VisitarClientesViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.DetalleClienteMvvm;
import com.kyaemp.perufarma.demo.ui.detallecliente.DetalleClienteViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.detalle.ITabDetalleClienteViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.detalle.TabDetalleClienteViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.IPedidosClienteViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.PedidosClienteViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview.PedidosItemMvvm;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview.PedidosItemViewModel;
import com.kyaemp.perufarma.demo.ui.home.HomeMvvm;
import com.kyaemp.perufarma.demo.ui.home.HomeViewModel;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.MenuItemMvvm;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.MenuItemViewModel;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.RelevamientoMvvm;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.RelevamientoViewModel;
import com.kyaemp.perufarma.demo.ui.lista.ListaMvvm;
import com.kyaemp.perufarma.demo.ui.lista.ListaViewModel;
import com.kyaemp.perufarma.demo.ui.lista.recyclerview.PointMvvm;
import com.kyaemp.perufarma.demo.ui.lista.recyclerview.PointViewModel;
import com.kyaemp.perufarma.demo.ui.login.LoginMvvm;
import com.kyaemp.perufarma.demo.ui.login.LoginViewModel;
import com.kyaemp.perufarma.demo.ui.map.MapMvvm;
import com.kyaemp.perufarma.demo.ui.map.MapViewModel;
import com.kyaemp.perufarma.demo.ui.map.guardarpoint.GuardarPointMvvm;
import com.kyaemp.perufarma.demo.ui.map.guardarpoint.GuardarPointViewModel;
import com.kyaemp.perufarma.demo.ui.nuevopedido.NuevoPedidoMvvm;
import com.kyaemp.perufarma.demo.ui.nuevopedido.NuevoPedidoViewModel;
import com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview.PedidoItemMvvm;
import com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview.PedidoItemViewModel;
import com.kyaemp.perufarma.demo.ui.signup.SignUpMvvm;
import com.kyaemp.perufarma.demo.ui.signup.SignUpViewModel;
import com.kyaemp.perufarma.demo.ui.splash.SplashMvvm;
import com.kyaemp.perufarma.demo.ui.splash.SplashViewModel;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelModule {
    // Bind your ViewModel implementations to interfaces here


    // Activities

    @Binds
    abstract SplashMvvm.ViewModel bindSplashViewModel(SplashViewModel viewModel);

    @Binds
    abstract LoginMvvm.ViewModel bindLoginViewModel(LoginViewModel viewModel);

    @Binds
    abstract SignUpMvvm.ViewModel bindSignUpViewModel(SignUpViewModel viewModel);

    @Binds
    abstract HomeMvvm.ViewModel bindHomeViewModel(HomeViewModel viewModel);

    @Binds
    abstract MapMvvm.ViewModel bindMapViewModel(MapViewModel viewModel);

    @Binds
    abstract ListaMvvm.ViewModel bindListaViewModel(ListaViewModel viewModel);

    @Binds
    abstract DetalleClienteMvvm.ViewModel bindDetalleClienteViewModel(DetalleClienteViewModel viewModel);

    @Binds
    abstract GuardarPointMvvm.ViewModel bindGuardarPointViewModel(GuardarPointViewModel viewModel);
    @Binds
    abstract NuevoPedidoMvvm.ViewModel bindNuevoPedidoViewModel(NuevoPedidoViewModel viewModel);
    // Fragments
    @Binds
    abstract IVisitarClientesViewModel bindVisitarClientesViewModel(VisitarClientesViewModel viewModel);
    @Binds
    abstract IHoyClientesViewModel bindHoyClientesViewModel(HoyClientesViewModel viewModel);
    @Binds
    abstract IMapaClientesViewModel.ViewModel bindMapaClientesViewModel(MapaClientesViewModel viewModel);
    @Binds
    abstract ITabDetalleClienteViewModel.ViewModel bindTabDetalleClienteViewModel(TabDetalleClienteViewModel viewModel);
    @Binds
    abstract IPedidosClienteViewModel.ViewModel bindPedidosClienteViewModel(PedidosClienteViewModel viewModel);
    @Binds
    abstract PedidosItemMvvm.ViewModel bindPedidosItemViewModel(PedidosItemViewModel viewModel);
    // View Holders

    @Binds
    abstract PointMvvm.ViewModel bindPointViewModel(PointViewModel ViewModel);
    @Binds
    abstract RelevamientoMvvm.ViewModel bindRelevamientoViewModel(RelevamientoViewModel ViewModel);
    @Binds
    abstract MenuItemMvvm.ViewModel bindMenuItemViewModel(MenuItemViewModel ViewModel);
    @Binds
    abstract ClienteItemMvvm.ViewModel bindClienteItemViewModel(ClienteItemViewModel ViewModel);
    @Binds
    abstract ClientesMvvm.ViewModel bindClientesViewModel(ClientesViewModel ViewModel);
    @Binds
    abstract PedidoItemMvvm.ViewModel bindPedidoItemViewModel(PedidoItemViewModel ViewModel);
}
