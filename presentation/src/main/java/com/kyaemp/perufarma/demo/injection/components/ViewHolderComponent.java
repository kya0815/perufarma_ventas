package com.kyaemp.perufarma.demo.injection.components;

import com.kyaemp.perufarma.demo.injection.modules.ViewHolderModule;
import com.kyaemp.perufarma.demo.injection.modules.ViewModelModule;
import com.kyaemp.perufarma.demo.injection.scopes.PerViewHolder;
import com.kyaemp.perufarma.demo.ui.clientes.recyclerview.ClienteItemViewHolder;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview.PedidosItemViewHolder;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.MenuItemViewHolder;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.RelevamientoViewHolder;
import com.kyaemp.perufarma.demo.ui.lista.recyclerview.PointViewHolder;
import com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview.PedidoItemViewHolder;

import dagger.Component;


@PerViewHolder
@Component(dependencies = ActivityComponent.class, modules = {ViewHolderModule.class, ViewModelModule.class})
public interface ViewHolderComponent {
    // create inject methods for your ViewHolders here
    void inject(PointViewHolder viewHolder);
    void inject(RelevamientoViewHolder viewHolder);
    void inject(MenuItemViewHolder viewHolder);
    void inject(ClienteItemViewHolder clienteItemViewHolder);
    void inject(PedidosItemViewHolder clienteItemViewHolder);

    void inject(PedidoItemViewHolder pedidoItemViewHolder);
}
