package com.kyaemp.perufarma.demo.ui.map.guardarpoint;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.HomeActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseDialog;
import com.kyaemp.perufarma.demo.ui.map.MapActivity;


/**
 * Fragmento con un diálogo personalizado
 */
public class GuardarPointDialog extends BaseDialog<HomeActivityBinding, GuardarPointMvvm.ViewModel> implements GuardarPointMvvm.View {

    public static final String TAG_EXTRA_POINT_LAT = "EXTRA_POINT_LAT";
    public static final String TAG_EXTRA_POINT_LON = "EXTRA_POINT_LON";

    public static GuardarPointDialog nuevaInstancia(Double lat, Double lon) {
        GuardarPointDialog dialog = new GuardarPointDialog();
        Bundle args = new Bundle();
        args.putDouble(TAG_EXTRA_POINT_LAT, lat);
        args.putDouble(TAG_EXTRA_POINT_LON, lon);
        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialogComponent().inject(this);
        Bundle mArgs = getArguments();
        viewModel.setPoint(mArgs.getDouble(TAG_EXTRA_POINT_LAT), mArgs.getDouble(TAG_EXTRA_POINT_LON));
        return new AlertDialog.Builder(getActivity())
                .setView(setAndBindContentView(savedInstanceState, R.layout.guardar_point_dialog))
                .create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (getActivity() instanceof MapActivity) {
            ((MapActivity) getActivity()).setReloadMap();
        }
        super.onDismiss(dialog);
    }
}


