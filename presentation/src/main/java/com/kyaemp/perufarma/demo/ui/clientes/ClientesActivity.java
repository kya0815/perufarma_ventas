package com.kyaemp.perufarma.demo.ui.clientes;

import android.os.Bundle;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.ActivityMainBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.ClientesViewPagerAdapter;

import javax.inject.Inject;


public class ClientesActivity extends BaseActivity<ActivityMainBinding, ClientesMvvm.ViewModel> implements ClientesMvvm.View {

    @Inject
    ClientesViewPagerAdapter adapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);
        setTitle("Clientes");
        setDisplayHomeAsUpEnabled();

        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }
}