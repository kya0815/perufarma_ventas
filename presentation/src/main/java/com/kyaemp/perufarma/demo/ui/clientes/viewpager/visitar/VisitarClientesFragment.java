package com.kyaemp.perufarma.demo.ui.clientes.viewpager.visitar;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.kyaemp.perufarma.demo.ui.clientes.viewpager.ClientesFragment;

public class VisitarClientesFragment extends ClientesFragment<IVisitarClientesViewModel> {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
