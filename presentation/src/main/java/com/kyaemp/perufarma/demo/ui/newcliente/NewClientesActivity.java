package com.kyaemp.perufarma.demo.ui.newcliente;

import android.os.Bundle;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.ActivityMainBinding;
import com.kyaemp.perufarma.demo.databinding.NewClienteActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.ClientesViewPagerAdapter;

import javax.inject.Inject;


public class NewClientesActivity extends BaseActivity<NewClienteActivityBinding, NewClientesMvvm.ViewModel> implements NewClientesMvvm.View {

    @Inject
    ClientesViewPagerAdapter adapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.new_cliente_activity);
        setSupportActionBar(binding.toolbar);
        setTitle("Clientes");
        setDisplayHomeAsUpEnabled();

        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setAdapter(adapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }
}