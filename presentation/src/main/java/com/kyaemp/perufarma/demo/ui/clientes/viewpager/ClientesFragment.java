package com.kyaemp.perufarma.demo.ui.clientes.viewpager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.FragmentRecyclerviewBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseFragment;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.MvvmViewModel;
import com.kyaemp.perufarma.demo.ui.clientes.recyclerview.ClienteItemAdapter;

import javax.inject.Inject;

public abstract class ClientesFragment<V extends MvvmViewModel> extends BaseFragment<FragmentRecyclerviewBinding, V> implements ClientesView {

    @Inject
    ClienteItemAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return setAndBindContentView(inflater, container, savedInstanceState, R.layout.fragment_recyclerview);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.setAdapter(adapter);
    }

    // View

}
