package com.kyaemp.perufarma.demo.ui;

import com.kyaemp.perufarma.demo.model.observables.PointModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.MvvmViewModel;

public interface IViewModel<V extends MvvmView> extends MvvmViewModel<V> {
    //propiedades generales
    //se podria poder listeners generales como de botones globales favbutton vp

    void update(PointModel point);

    PointModel getPoint();

}
