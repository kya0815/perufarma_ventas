package com.kyaemp.perufarma.demo.ui.detallecliente;

import android.content.Context;
import android.databinding.ObservableBoolean;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.DetalleClienteViewPagerAdapter;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.ObtenerDetalleClienteUseCase;
import com.kyaemp.perufarma.domain.model.Cliente;

import javax.inject.Inject;

@PerActivity
public class DetalleClienteViewModel extends BaseActivityViewModel<DetalleClienteMvvm.View> implements DetalleClienteMvvm.ViewModel {

    private static final int MY_REQUEST_PERMISSION = 14;
    private final ObservableBoolean loaded;
    private ClientesModel cliente=null;
    private final static int CODE_SIGNUP=13;
    private ClienteModelDataMapper mapper;
    private int idCliente;
    private ObtenerDetalleClienteUseCase obtenerDetalleClienteUseCase;
    private GoogleMap mMap;
    private DetalleClienteViewPagerAdapter adapter;

    @Inject
    DetalleClienteViewModel(@AppContext Context appContext, Navigator navigator, ClienteModelDataMapper mapper, ObtenerDetalleClienteUseCase obtenerDetalleClienteUseCase,DetalleClienteViewPagerAdapter adapter) {
        super(appContext, navigator);
        this.loaded = new ObservableBoolean(false);
        this.obtenerDetalleClienteUseCase=obtenerDetalleClienteUseCase;
        this.mapper=mapper;
        this.adapter=adapter;
    }


    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    private void showLoading(){
        loaded.set(true);
    }

    private void hideLoading(){
        loaded.set(false);
    }

    @Override
    public ClientesModel getCliente() {
        return cliente;
    }

    public void setCliente(ClientesModel cliente) {
        this.cliente=cliente;
        notifyChange();
        getView().setTitleToolbar(this.cliente.getCliente_nombre());

        iniciarView();
    }

    @Override
    public void setIdCliente(int idCliente) {
        this.idCliente=idCliente;
        llamarDetalleCliente();
        adapter.setId(idCliente);
    }

    private void llamarDetalleCliente() {
        showLoading();
        obtenerDetalleClienteUseCase.execute(new ObtenerDetalleClienteObserver(),
                ObtenerDetalleClienteUseCase.Params.id(idCliente));
    }

    private final class ObtenerDetalleClienteObserver extends DefaultObserver<Cliente> {

        @Override
        public void onComplete() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            toast(e.getMessage());
        }

        @Override
        public void onNext(Cliente result) {
            ClientesModel cliente= mapper.transform(result);
            if (result != null &&  cliente!=null) {
                setCliente(cliente);
            } else {
                toast("No existe el cliente");
            }
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        iniciarView();
    }

    private void iniciarView() {
        if (mMap != null && cliente!=null) {
            mMap.setMyLocationEnabled(true);
            crearPunto();
        }
    }

    private void crearPunto() {
        LatLng pos = new LatLng(cliente.getLatitud(), cliente.getLongitud());
        mMap.addMarker(
            new MarkerOptions()
                    .position(pos)
                    .title(cliente.getCliente_nombre())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        ).showInfoWindow();

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));
    }

}


