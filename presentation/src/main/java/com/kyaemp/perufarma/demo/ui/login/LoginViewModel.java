package com.kyaemp.perufarma.demo.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.ObservableBoolean;
import android.os.Build;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.mapper.RespuestaServerModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.UserLoginModelDataMapper;
import com.kyaemp.perufarma.demo.model.normal.RespuestaServerModel;
import com.kyaemp.perufarma.demo.model.observables.UserLoginModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.demo.ui.home.HomeActivity;
import com.kyaemp.perufarma.demo.ui.signup.SignUpActivity;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.LoginUseCase;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import javax.inject.Inject;

@PerActivity
public class LoginViewModel extends BaseActivityViewModel<LoginMvvm.View> implements LoginMvvm.ViewModel {

    private static final int MY_REQUEST_PERMISSION = 14;
    private final ObservableBoolean loaded;
        private LoginUseCase loginUseCase;
    private RespuestaServerModelDataMapper respuestaServerModelDataMapper;
    private UserLoginModel userLogin;
    private final static int CODE_SIGNUP=13;

    // TODO falta crear obserbale string
    @Inject
    LoginViewModel(@AppContext Context appContext, Navigator navigator, LoginUseCase loginUseCase,RespuestaServerModelDataMapper respuestaServerModelDataMapper) {
        super(appContext, navigator);
        this.loginUseCase=loginUseCase;
        this.respuestaServerModelDataMapper = respuestaServerModelDataMapper;
        this.userLogin = new UserLoginModel("", "");
        this.loaded = new ObservableBoolean(false);
        initView();
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {
                //tiene permisos
            } else {
                requestPermission();//no tiene permisos
            }
        } else {
            //version no requiere solicitud de permisos
        }
    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    @Override
    public UserLoginModel getUserLogin() {
        return userLogin;
    }

    @Override
    public void onClickIniciarSesion(View v) {
        login();
    }

    @Override
    public void onClickSignup(View v) {
        navigator.startActivityForResult(SignUpActivity.class,CODE_SIGNUP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CODE_SIGNUP){
            if(resultCode == Activity.RESULT_OK){
                cierreLogeoOk();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_REQUEST_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    toast("Gracias!, ahora podras usar Todas nuestras funcionalidades");
                } else {
//                    Toast.makeText(this,"Recuerda que no podras hacer uso de todas nuestras funcionalidades",Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }

    void cierreLogeoOk(){
        navigator.startActivity(HomeActivity.class);
        navigator.finishActivity();
    }

    void loginOK(String messages) {
        toast(messages);
        cierreLogeoOk();
    }

    void loginFail(String messages) {
        toast(messages);
    }

    void showLoading() {
        loaded.set(true);
    }

    void hideLoading() {
        loaded.set(false);
    }

    void showError(String messages) {
        toast(messages);
    }

    //
    private void login() {
        showLoading();
        if(checkPermission()){
            this.loginUseCase.execute(
                    new LoginUserObserver(),
                    LoginUseCase.Params.userLogin(
                            new UserLoginModelDataMapper().transform(userLogin)
                    )
            );
        }else{
            hideLoading();
            toast("Acepte todos los permisos para poder acceder al App");
            requestPermission();
        }

    }

    private final class LoginUserObserver extends DefaultObserver<RespuestaServer> {

        @Override
        public void onComplete() {
            LoginViewModel.this.hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            LoginViewModel.this.hideLoading();
            LoginViewModel.this.showError(e.getMessage());
        }

        @Override
        public void onNext(RespuestaServer result) {
            RespuestaServerModel respuestaServerModel = respuestaServerModelDataMapper.transform(result);
            if (respuestaServerModel.getStatus() == 1) {
                LoginViewModel.this.loginOK(respuestaServerModel.getMensaje());
            } else {
                LoginViewModel.this.loginFail(respuestaServerModel.getMensaje());
            }
        }
    }


    private void requestPermission() {
        String[] permisos = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        navigator.requestPermissions(permisos, MY_REQUEST_PERMISSION);
    }

    private boolean checkPermission() {
        int location1 = navigator.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        int location2 = navigator.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        int location3 = navigator.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int location4 = navigator.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        return location1 == PackageManager.PERMISSION_GRANTED
                && location2 == PackageManager.PERMISSION_GRANTED
                && location3 == PackageManager.PERMISSION_GRANTED
                && location4 == PackageManager.PERMISSION_GRANTED;
    }
}
