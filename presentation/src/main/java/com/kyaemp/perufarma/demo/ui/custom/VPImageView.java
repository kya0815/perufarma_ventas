package com.kyaemp.perufarma.demo.ui.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.utils.Utils;

public class VPImageView extends android.support.v7.widget.AppCompatImageView {


    public VPImageView(Context context) {
        super(context);
        initAttrs(context, null);
    }

    public VPImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttrs(context, attrs);
    }

    public VPImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttrs(context, attrs);
    }

    void initAttrs(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray attributeArray = context.obtainStyledAttributes(
                    attrs,
                    R.styleable.VPImageView);

            Drawable drawableImage;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableImage = attributeArray.getDrawable(R.styleable.VPImageView_srcFull);
            } else {
                final int drawableImageID = attributeArray.getResourceId(R.styleable.VPImageView_srcFull, -1);
                drawableImage = drawableImageID != -1 ? AppCompatResources.getDrawable(context, drawableImageID) : null;
            }
            setImageDrawable(drawableImage);
            String url = attributeArray.getString(R.styleable.VPImageView_url);
            if (url != null && !url.isEmpty()) {
                Utils.setFondoCompleto(this, url);
            }
            attributeArray.recycle();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}