package com.kyaemp.perufarma.demo.ui.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.GridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.HomeActivityBinding;
import com.kyaemp.perufarma.demo.model.observables.MenuItemModel;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.MenuItemAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class HomeActivity extends BaseActivity<HomeActivityBinding, HomeMvvm.ViewModel> implements HomeMvvm.View {

    @Inject
    MenuItemAdapter adapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.home_activity);
        configRecycler();
        creatMenu();
        setSupportActionBar(binding.toolbar);
        setTitle("Home");
    }

    private void configRecycler() {
        binding.rvItemsMenu.setHasFixedSize(true);
        binding.rvItemsMenu.setLayoutManager(new GridLayoutManager(this,2));
        binding.rvItemsMenu.setAdapter(adapter);
//        binding.rvRelevamientos.setHasFixedSize(true);
//        binding.rvRelevamientos.setLayoutManager(new LinearLayoutManager(this));
//        binding.rvRelevamientos.setAdapter(adapter);
    }
    
    private void creatMenu(){
        List<MenuItemModel> itemlist = new ArrayList<>();
        try {
            itemlist.add(new MenuItemModel(0, "Clientes", ResourcesCompat.getDrawable(getResources(), R.drawable.icon_home_clientes, null)));
            itemlist.add(new MenuItemModel(1, "Pedidos Pendientes", ResourcesCompat.getDrawable(getResources(), R.drawable.icon_home_pedidos_pendientes, null)));
            itemlist.add(new MenuItemModel(2, "Reclamos Pendientes", ResourcesCompat.getDrawable(getResources(), R.drawable.icon_home_reclamos_pendientes, null)));
            itemlist.add(new MenuItemModel(3, "Stock", ResourcesCompat.getDrawable(getResources(), R.drawable.icon_home_stock, null)));
            itemlist.add(new MenuItemModel(4, "Indicador Resultados", ResourcesCompat.getDrawable(getResources(), R.drawable.icon_home_indicador_resultados, null)));
            itemlist.add(new MenuItemModel(5, "Mensajes", ResourcesCompat.getDrawable(getResources(), R.drawable.icon_home_mensajes, null)));
//            itemlist.add(new MenuItemModel(6, "Logout", ResourcesCompat.getDrawable(getResources(), R.drawable.icon_home_logout, null)));
        }catch (Exception e){
            e.printStackTrace();
        }
        adapter.setItem(itemlist);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item_logout:
                viewModel.logout();
                break;
            default: break;
        }

        return super.onOptionsItemSelected(item);
    }
}