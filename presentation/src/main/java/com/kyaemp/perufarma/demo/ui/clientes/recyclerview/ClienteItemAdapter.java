package com.kyaemp.perufarma.demo.ui.clientes.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerFragment
public class ClienteItemAdapter extends RecyclerView.Adapter<ClienteItemViewHolder> {

    private List<ClientesModel> items = Collections.emptyList();

    @Inject
    public ClienteItemAdapter() {
    }

    public void setItems(List<ClientesModel> items) {
        this.items = items;
    }

    @Override
    public ClienteItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_cliente, viewGroup, false);

        return new ClienteItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClienteItemViewHolder viewHolder, int position) {
        viewHolder.viewModel().update(items.get(position));
        viewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
