package com.kyaemp.perufarma.demo.utils;

import io.reactivex.functions.Consumer;

public interface PlainConsumer<T> extends Consumer<T> {
    /**
     * Consume the given value.
     *
     * @param t the value
     */
    @Override
    void accept(T t);
}
