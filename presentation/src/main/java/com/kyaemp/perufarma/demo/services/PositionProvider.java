/*
 * Copyright 2013 - 2015 Anton Tananaev (anton.tananaev@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.demo.services;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.kyaemp.perufarma.data.utiles.Constantes;
import com.kyaemp.perufarma.demo.model.normal.PositionModel;

import static com.kyaemp.perufarma.data.utiles.Constantes.PREF_KEY_GUARDAR_GPS;

public abstract class PositionProvider {

    protected static final String TAG = PositionProvider.class.getSimpleName();

    public interface PositionListener {
        void onPositionUpdate(PositionModel position);
    }

    private final PositionListener listener;

    private final Context context;
    private final SharedPreferences preferences;
    protected final LocationManager locationManager;

    private String deviceId;
    protected final long period;
    protected final long guardar;

    private long lastUpdateTime;

    public PositionProvider(Context context, PositionListener listener) {
        this.context = context;
        this.listener = listener;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        deviceId = Constantes.idDevice(context);
        guardar = context.getSharedPreferences(Constantes.PREFERENCES,Context.MODE_PRIVATE)
                .getInt(PREF_KEY_GUARDAR_GPS,0) * 1000;
        period = 2000;
    }

    public abstract void startUpdates();

    public abstract void stopUpdates();

    protected void updateLocation(Location location) {
        if (location != null ) {
            Log.i(TAG, "location new");
            int flag=0;
            if(location.getTime() - lastUpdateTime >= guardar) {
                lastUpdateTime=location.getTime();
                flag=1;
            }
            Log.i(TAG, "period-----"+guardar);
            Log.i(TAG, "flag-----"+flag);
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.herprogramacion.restaurantericoparico.prefs", context.MODE_PRIVATE);
            String name =sharedPreferences.getString("name","");
            listener.onPositionUpdate(new PositionModel(deviceId, location, getBatteryLevel(),name,flag));
        } else {
            Log.i(TAG, location != null ? "location old" : "location nil");
        }
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private double getBatteryLevel() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ECLAIR) {
            Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, 1);
            return (level * 100.0) / scale;
        } else {
            return 0;
        }
    }
}
