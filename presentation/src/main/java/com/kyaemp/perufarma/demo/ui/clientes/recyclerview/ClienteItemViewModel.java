package com.kyaemp.perufarma.demo.ui.clientes.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerViewHolder;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDemoViewModel;
import com.kyaemp.perufarma.demo.ui.detallecliente.DetalleClienteActivity;

import javax.inject.Inject;


@PerViewHolder
public class ClienteItemViewModel extends BaseDemoViewModel<MvvmView> implements ClienteItemMvvm.ViewModel {

    private ClientesModel cliente;
    private ClienteModelDataMapper mapper;

    @Inject
    public ClienteItemViewModel(@AppContext Context context, Navigator navigator,ClienteModelDataMapper mapper) {
        super(context, navigator);
        this.mapper=mapper;
    }

    @Override
    public void update(ClientesModel cliente) {
        this.cliente=cliente;
        notifyChange();
    }

    @Override
    public ClientesModel getCliente() {
        return cliente;
    }

    @Override
    public void OnClickNuevoPedido(View v) {
        String url = "https://waze.com/ul?title="+cliente.getCliente_nombre()+
                "&ll="+cliente.getLatitud()+","+cliente.getLongitud()+"&navigate=yes";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        navigator.startActivity(intent);
    }

    @Override
    public void OnClickCliente(View v) {
        navigator.startActivity(DetalleClienteActivity.class, cliente.getId());
    }
}
