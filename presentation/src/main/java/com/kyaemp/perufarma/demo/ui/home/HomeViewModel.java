package com.kyaemp.perufarma.demo.ui.home;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.ObservableBoolean;
import android.os.Build;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.services.SocketServices;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseActivityViewModel;
import com.kyaemp.perufarma.demo.ui.home.recyclerview.MenuItemAdapter;
import com.kyaemp.perufarma.demo.ui.login.LoginActivity;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.GuardarCatalogoToOnlineUseCase;

import javax.inject.Inject;

@PerActivity
public class HomeViewModel extends BaseActivityViewModel<HomeMvvm.View> implements HomeMvvm.ViewModel {

    private static final int MY_REQUEST_PERMISSION = 15;
    private MenuItemAdapter adapter;
    private GuardarCatalogoToOnlineUseCase guardarCatalogoToOnlineUseCase;
    private ObservableBoolean active;

    @Inject
    HomeViewModel(@AppContext Context appContext, Navigator navigator, MenuItemAdapter adapter,
                  GuardarCatalogoToOnlineUseCase guardarCatalogoToOnlineUseCase) {
        super(appContext, navigator);
        this.adapter = adapter;
        this.guardarCatalogoToOnlineUseCase = guardarCatalogoToOnlineUseCase;
        this.active= new ObservableBoolean(false);
        iniciarView();
    }

    private void iniciarView() {
        solicitarCatalogo();
        validarPermisos();
    }

    private void validarPermisos() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermission()) {
                habilitar();
                iniciarServicioTracking();
            } else {
                desabilitar();
                requestPermission();//no tiene permisos
            }
        } else {
           habilitar();
        }
    }

    @Override
    public ObservableBoolean getActive() {
        return active;
    }

    @Override
    public void onClickValidarPermisos(View v) {
        validarPermisos();
    }

    private void iniciarServicioTracking() {
        navigator.startService(new Intent(con, SocketServices.class));
    }


    private void solicitarCatalogo() {
        this.guardarCatalogoToOnlineUseCase.execute(new GuardarCatalogoToOnlineObserver(), null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_REQUEST_PERMISSION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    toast("Gracias!, ahora podras usar Todas nuestras funcionalidades");
                    habilitar();
                } else {
                    desabilitar();
                }
                break;
            default:
                break;
        }
    }

    private void desabilitar(){
        active.set(false);
    }
    private void habilitar(){
        active.set(true);
    }

    private final class GuardarCatalogoToOnlineObserver extends DefaultObserver<Boolean> {

        @Override
        public void onComplete() {

        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            HomeViewModel.this.toast("Error al Sincronizar Fallida");
        }

        @Override
        public void onNext(Boolean result) {
            if (result) {
                HomeViewModel.this.toast("Sincronizacion Exitosa");
            } else {
                onError(new Exception("Sincronizacion Fallida"));
            }
        }
    }



    private void requestPermission() {
        String[] permisos = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        navigator.requestPermissions(permisos, MY_REQUEST_PERMISSION);
    }

    private boolean checkPermission() {
        int location1 = navigator.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        int location2 = navigator.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        int location3 = navigator.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int location4 = navigator.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
        return location1 == PackageManager.PERMISSION_GRANTED
                && location2 == PackageManager.PERMISSION_GRANTED
                && location3 == PackageManager.PERMISSION_GRANTED
                && location4 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void logout() {
        navigator.stopService(new Intent(ctx, SocketServices.class));
        navigator.cleanPreferences();
        navigator.startActivity(LoginActivity.class);
        navigator.finishActivity();
    }
}
