package com.kyaemp.perufarma.demo.model.mapper;

import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.domain.model.Catalogo;

import java.util.ArrayList;
import java.util.List;


public class CatalogoModelDataMapper {

    public CatalogoModel transform(Catalogo item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new CatalogoModel(item.getId(),item.getCategoria(),item.getMarca(),item.getProducto(),item.getStockAlmacen(),item.getCantidadVendida(),item.getPrecioUnitario(),item.getSubtotal());
    }

    public Catalogo transform(CatalogoModel item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new Catalogo(item.getId(),item.getCategoria(),item.getMarca(),item.getProducto(),item.getStockAlmacen(),item.getCantidadVendida(),item.getPrecioUnitario(),item.getSubtotal());
    }

    public <R, T> List<R> transform(List<T> c) {
        List<R> items = new ArrayList<>();
        if (c != null && !c.isEmpty()) {
            for (T t : c) {
                items.add(transform(t));
            }
        }
        return items;
    }

    private <R, T> R transform(T t) {
        if (t instanceof Catalogo) {
            return (R) transform((Catalogo) t);
        } else if (t instanceof CatalogoModel) {
            return (R) transform((CatalogoModel) t);
        }
        return null;
    }
}
