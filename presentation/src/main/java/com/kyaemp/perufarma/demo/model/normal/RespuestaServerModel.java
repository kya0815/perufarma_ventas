package com.kyaemp.perufarma.demo.model.normal;

import java.io.Serializable;

/**
 * Created by pedro.zevallos on 26/08/2017.
 */

public class RespuestaServerModel implements Serializable {
    private int status;
    private String mensaje;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
