package com.kyaemp.perufarma.demo.ui.map;


import com.google.android.gms.maps.GoogleMap;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface MapMvvm {

    interface View extends MvvmView {

    }

    interface ViewModel extends IViewModel<View> {
        // Properties
        void onMapReady(GoogleMap googleMap);

        void reloadMap();
    }
}
