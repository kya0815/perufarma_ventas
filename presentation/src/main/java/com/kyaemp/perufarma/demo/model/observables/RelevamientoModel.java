package com.kyaemp.perufarma.demo.model.observables;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */

public class RelevamientoModel extends BaseObservable {

    private long _id;
    private String empresa;
    private String telefono;
    private String direccion;
    private double latitud;
    private double longitud;
    private boolean terminado;

    public RelevamientoModel(long _id, String empresa, String telefono, String direccion, double latitud, double longitud, boolean terminado) {
        this._id = _id;
        this.empresa = empresa;
        this.telefono = telefono;
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
        this.terminado = terminado;
    }

    @Bindable
    public boolean isTerminado() {
        return terminado;
    }

    public void setTerminado(boolean terminado) {
        this.terminado = terminado;
        notifyPropertyChanged(BR.terminado);
    }

    @Bindable
    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
        notifyPropertyChanged(BR._id);
    }

    @Bindable
    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
        notifyPropertyChanged(BR.empresa);
    }

    @Bindable
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
        notifyPropertyChanged(BR.telefono);
    }

    @Bindable
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
        notifyPropertyChanged(BR.direccion);
    }

    @Bindable
    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
        notifyPropertyChanged(BR.latitud);
    }

    @Bindable
    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
        notifyPropertyChanged(BR.longitud);
    }
}
