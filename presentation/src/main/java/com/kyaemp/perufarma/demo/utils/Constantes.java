package com.kyaemp.perufarma.demo.utils;


import com.kyaemp.perufarma.data.BuildConfig;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public class Constantes {
    public static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID;
    public static final String PREFERENCES = PACKAGE_NAME + ".pref";
    public static final String PREF_LOGGIN = "loggin";
    public static final String PREF_DEVICEID = "deviceID";
    public static final String URL_NODEJS="http://52.22.169.65:90/";
}
