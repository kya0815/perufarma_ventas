package com.kyaemp.perufarma.demo.ui.lista.recyclerview;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerViewHolder;
import com.kyaemp.perufarma.demo.model.observables.PointModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDemoViewModel;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.GuardarPuntosServerUseCase;

import javax.inject.Inject;


@PerViewHolder
public class PointViewModel extends BaseDemoViewModel<MvvmView> implements PointMvvm.ViewModel {

    private final ObservableBoolean loaded;
    private final GuardarPuntosServerUseCase guardarPuntosServerUseCase;
    private ObservableBoolean enviado;

    @Inject
    public PointViewModel(@AppContext Context context, Navigator navigator, GuardarPuntosServerUseCase guardarPuntosServerUseCase) {
        super(context, navigator);
        this.loaded = new ObservableBoolean(false);
        this.enviado = new ObservableBoolean(false);
        this.guardarPuntosServerUseCase = guardarPuntosServerUseCase;
    }

    @Override
    public void onEnviar(View v) {
        guardarPoint();
    }

    @Override
    public void update(PointModel point) {
        super.update(point);
        enviado = new ObservableBoolean(getPoint().isEnviado());
    }

    private void guardarPoint() {
        showLoading();
        this.guardarPuntosServerUseCase.execute(new GuardarPointsServerObserver(),
                GuardarPuntosServerUseCase.Params.id(getPoint().getId()));
    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    @Override
    public ObservableBoolean isEnviado() {
        return enviado;
    }

    void showLoading() {
        loaded.set(true);
    }

    void hideLoading() {
        loaded.set(false);
    }

    void showError(String messages) {
        toast(null, messages);
    }

    private final class GuardarPointsServerObserver extends DefaultObserver<Boolean> {

        @Override
        public void onComplete() {
            hideLoading();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            e.printStackTrace();
            PointViewModel.this.showError(e.getMessage());
        }

        @Override
        public void onNext(Boolean result) {
            if (result) {
                point.setEnviado(true);
                enviado.set(true);
                PointViewModel.this.toast(null, "Datos Gaurdados Corectamente");
            }
        }
    }
}
