package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos;

import android.content.Intent;
import android.databinding.ObservableBoolean;
import android.support.v7.widget.RecyclerView;

import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.MvvmViewModel;

public interface IPedidosClienteViewModel  {
    interface View extends MvvmView {

    }

    interface ViewModel extends MvvmViewModel<IPedidosClienteViewModel.View> {
        RecyclerView.Adapter getAdapter();
        void setId(int id);
        ObservableBoolean getLoaded();
        ClientesModel getCliente();
        ObservableBoolean getEmpty();
        void onClickNewPedido(android.view.View v);


        void onActivityResultlistener(int requestCode, int resultCode, Intent data);
    }

}
