/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.demo.model.mapper;


import com.kyaemp.perufarma.demo.model.observables.UserSignUpModel;
import com.kyaemp.perufarma.domain.model.UserSignUp;

/**
 * Mapper class used to transform {@link UserSignUp} (in the domain layer) to {@link UserSignUpModel} in the
 * presentation layer.
 */
public class UserSignUpModelDataMapper {

  public UserSignUpModel transform(UserSignUp item) {
    if (item == null) {
      throw new IllegalArgumentException("Cannot transform a null value");
    }
    return new UserSignUpModel(item.getUser(),item.getPass(),item.getCode(),item.getName());
  }

  public UserSignUp transform(UserSignUpModel item) {
    if (item == null) {
      throw new IllegalArgumentException("Cannot transform a null value");
    }
    return new UserSignUp(item.getUser(),item.getPass(),item.getCode(),item.getName());
  }
}
