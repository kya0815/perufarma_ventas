package com.kyaemp.perufarma.demo.ui.signup;

import android.os.Bundle;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.SignupActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;


public class SignUpActivity extends BaseActivity<SignupActivityBinding, SignUpMvvm.ViewModel> implements SignUpMvvm.View {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.signup_activity);
    }
}