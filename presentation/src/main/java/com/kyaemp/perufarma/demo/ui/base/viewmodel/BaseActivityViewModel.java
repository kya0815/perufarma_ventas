package com.kyaemp.perufarma.demo.ui.base.viewmodel;

import android.content.Context;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

public abstract class BaseActivityViewModel<V extends MvvmView> extends BaseDemoViewModel<V> implements IViewModel<V> {

    protected final Context con;
    protected final Navigator navigator;

    public BaseActivityViewModel(@AppContext Context context, Navigator navigator) {
        super(context, navigator);
        this.con = context.getApplicationContext();
        this.navigator = navigator;
    }

    protected void toast(String mensaje) {
        toast(con, mensaje);
    }
}
