package com.kyaemp.perufarma.demo.ui.map.guardarpoint;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.injection.scopes.PerDialogFragment;
import com.kyaemp.perufarma.demo.model.mapper.PointModelDataMapper;
import com.kyaemp.perufarma.demo.model.observables.GuardarPointModel;
import com.kyaemp.perufarma.demo.model.observables.PointModel;
import com.kyaemp.perufarma.demo.ui.base.navigator.DialogNavigator;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.BaseDialogViewModel;
import com.kyaemp.perufarma.domain.interactor.DefaultObserver;
import com.kyaemp.perufarma.domain.interactor.GuardarPuntosUseCase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


@PerDialogFragment
public class GuardarPointViewModel extends BaseDialogViewModel<GuardarPointMvvm.View> implements GuardarPointMvvm.ViewModel {

    private final ObservableBoolean loaded;
    private GuardarPuntosUseCase guardarPuntosUseCase;
    private PointModelDataMapper pointModelDataMapper;
    private GuardarPointModel guardarPointModel;
    private PointModel pointModel;

    @Inject
    GuardarPointViewModel(@AppContext Context appContext, DialogNavigator navigator, GuardarPuntosUseCase guardarPuntosUseCase, PointModelDataMapper pointModelDataMapper) {
        super(appContext, navigator);
        this.guardarPuntosUseCase = guardarPuntosUseCase;
        this.pointModelDataMapper = pointModelDataMapper;
        this.guardarPointModel = new GuardarPointModel("");
        this.pointModel = new PointModel(0, null, null, null, false);
        this.loaded = new ObservableBoolean(false);
    }

    @Override
    public ObservableBoolean isLoaded() {
        return loaded;
    }

    @Override
    public GuardarPointModel getEntidad() {
        return guardarPointModel;
    }

    @Override
    public void setPoint(Double lat, Double lon) {
        pointModel.setLat(lat);
        pointModel.setLon(lon);
    }

    @Override
    public void onClickAceptar(View view) {
        guardarPoint();
    }

    private void guardarPoint() {
        showLoading();
        pointModel.setName(guardarPointModel.getName());
        List<PointModel> listaPoint = new ArrayList<>();
        listaPoint.add(pointModel);
        this.guardarPuntosUseCase.execute(new GuardarPointsObserver(), GuardarPuntosUseCase.Params.listaPuntos(pointModelDataMapper.transform(listaPoint)));
    }

    @Override
    public void onClickCancelar(View view) {
        navigator.dismiss();
    }

    void showLoading() {
        loaded.set(true);
    }

    void hideLoading() {
        loaded.set(false);
    }

    void showError(String messages) {
        toast(messages);
    }


    private final class GuardarPointsObserver extends DefaultObserver<Boolean> {

        @Override
        public void onComplete() {
            hideLoading();
            navigator.dismiss();
        }

        @Override
        public void onError(Throwable e) {
            hideLoading();
            e.printStackTrace();
            GuardarPointViewModel.this.showError(e.getMessage());
        }

        @Override
        public void onNext(Boolean result) {
            if (result) {
                GuardarPointViewModel.this.toast("Datos Gaurdados Corectamente");
            }
        }
    }
}
