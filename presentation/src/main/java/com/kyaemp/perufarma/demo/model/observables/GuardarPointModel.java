package com.kyaemp.perufarma.demo.model.observables;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */

public class GuardarPointModel extends BaseObservable {
    private String name;

    public GuardarPointModel(String name) {
        this.name = name;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.error);
        notifyPropertyChanged(BR.valido);
    }

    @Bindable
    public String getError() {
        if (!name.isEmpty() && name.length() <= 3) {
            return "ingrese un nombre Valido";
        } else {
            return null;
        }
    }

    @Bindable
    public Boolean getValido() {
        return !name.isEmpty() && getError() == null;
    }
}
