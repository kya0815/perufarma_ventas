package com.kyaemp.perufarma.demo.model.observables;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */
public class PedidoModel extends BaseObservable {

    private int id;
    private long codigo_item;
    private int cantidad;
    private int cliente_codigo;

    public PedidoModel(int id, long codigo_item, int cantidad, int cliente_codigo) {
        this.id = id;
        this.codigo_item = codigo_item;
        this.cantidad = cantidad;
        this.cliente_codigo = cliente_codigo;
    }

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public long getCodigo_item() {
        return codigo_item;
    }

    public void setCodigo_item(long codigo_item) {
        this.codigo_item = codigo_item;
        notifyPropertyChanged(BR.codigo_item);
    }

    @Bindable
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
        notifyPropertyChanged(BR.cantidad);
    }

    @Bindable
    public int getCliente_codigo() {
        return cliente_codigo;
    }

    public void setCliente_codigo(int cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
        notifyPropertyChanged(BR.cliente_codigo);
    }
}
