package com.kyaemp.perufarma.demo.ui.home.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.model.observables.RelevamientoModel;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

@PerActivity
public class RelevamientoAdapter extends RecyclerView.Adapter<RelevamientoViewHolder> {

    private List<RelevamientoModel> items = Collections.emptyList();

    @Inject
    public RelevamientoAdapter() {
    }

    public void setItem(List<RelevamientoModel> items) {
        this.items = items;
    }

    @Override
    public RelevamientoViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.item_relevamiento, viewGroup, false);

        return new RelevamientoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RelevamientoViewHolder countryViewHolder, int position) {
        countryViewHolder.viewModel().setPosition(position);
        countryViewHolder.viewModel().update(items.get(position));
        countryViewHolder.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
