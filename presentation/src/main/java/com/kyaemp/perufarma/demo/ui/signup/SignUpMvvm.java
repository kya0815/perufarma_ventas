package com.kyaemp.perufarma.demo.ui.signup;


import android.databinding.ObservableBoolean;

import com.kyaemp.perufarma.demo.model.observables.UserSignUpModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface SignUpMvvm {

    interface View extends MvvmView {

    }

    interface ViewModel extends IViewModel<View> {
        // Properties
        ObservableBoolean isLoaded();

        UserSignUpModel getUserSignUp();

        void onClickResgistrase(android.view.View v);
        void onClickLogin(android.view.View v);
    }
}
