package com.kyaemp.perufarma.demo.injection.modules;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.kyaemp.perufarma.demo.injection.qualifier.ChildFragmentManager;
import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.ui.base.navigator.ChildFragmentNavigator;
import com.kyaemp.perufarma.demo.ui.base.navigator.FragmentNavigator;

import dagger.Module;
import dagger.Provides;

@Module
public class FragmentModule {

    private final Fragment mFragment;

    public FragmentModule(Fragment fragment) {
        mFragment = fragment;
    }

    @Provides
    @PerFragment
    @ChildFragmentManager
    FragmentManager provideChildFragmentManager() {
        return mFragment.getChildFragmentManager();
    }

    @Provides
    @PerFragment
    FragmentNavigator provideFragmentNavigator() {
        return new ChildFragmentNavigator(mFragment);
    }

}
