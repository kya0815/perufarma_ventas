package com.kyaemp.perufarma.demo.injection.components;

import com.kyaemp.perufarma.demo.injection.modules.FragmentModule;
import com.kyaemp.perufarma.demo.injection.modules.ViewModelModule;
import com.kyaemp.perufarma.demo.injection.scopes.PerFragment;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.hoy.HoyClientesFragment;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.mapa.MapaClientesFragment;
import com.kyaemp.perufarma.demo.ui.clientes.viewpager.visitar.VisitarClientesFragment;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.detalle.TabDetalleClienteFragment;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.PedidosClienteFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ActivityComponent.class, modules = {FragmentModule.class, ViewModelModule.class})
public interface FragmentComponent {
    // create inject methods for your Fragments here
    void inject(HoyClientesFragment fragment);
    void inject(VisitarClientesFragment fragment);
    void inject(MapaClientesFragment fragment);
    void inject(TabDetalleClienteFragment fragment);
    void inject(PedidosClienteFragment fragment);
}
