package com.kyaemp.perufarma.demo.services;



public interface OnSocketConnectionListener {
    void onSocketEventFailed();

    void onSocketConnectionStateChange(int socketState);
    void onSocketSendCorreoPrimerChat();
    void onInternetConnectionStateChange(int socketState);
}
