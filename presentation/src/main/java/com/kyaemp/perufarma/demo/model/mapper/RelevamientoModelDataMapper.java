/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.demo.model.mapper;

import com.kyaemp.perufarma.demo.model.observables.RelevamientoModel;
import com.kyaemp.perufarma.domain.model.Relevamiento;

import java.util.ArrayList;
import java.util.List;


public class RelevamientoModelDataMapper {

    public RelevamientoModel transform(Relevamiento item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new RelevamientoModel(item.get_id(),item.getEmpresa(),item.getTelefono(),item.getDireccion(),item.getLatitud(),item.getLongitud(),item.isTerminado());
    }

    public Relevamiento transform(RelevamientoModel item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new Relevamiento(item.get_id(),item.getEmpresa(),item.getTelefono(),item.getDireccion(),item.getLatitud(),item.getLongitud(),item.isTerminado());
    }

    public <R, T> List<R> transform(List<T> c) {
        List<R> items = new ArrayList<>();
        if (c != null && !c.isEmpty()) {
            for (T t : c) {
                items.add(transform(t));
            }
        }
        return items;
    }

    private <R, T> R transform(T t) {
        if (t instanceof Relevamiento) {
            return (R) transform((Relevamiento) t);
        } else if (t instanceof RelevamientoModel) {
            return (R) transform((RelevamientoModel) t);
        }
        return null;
    }
}
