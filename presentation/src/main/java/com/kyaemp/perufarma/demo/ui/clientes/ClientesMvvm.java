package com.kyaemp.perufarma.demo.ui.clientes;


import android.databinding.ObservableBoolean;

import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface ClientesMvvm {

    interface View extends MvvmView {

    }

    interface ViewModel extends IViewModel<View> {
        // Properties

        void onClickNewCliente(android.view.View v);
    }
}
