package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kyaemp.perufarma.demo.injection.qualifier.ActivityFragmentManager;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.detalle.TabDetalleClienteFragment;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.PedidosClienteFragment;

import javax.inject.Inject;


@PerActivity
public class DetalleClienteViewPagerAdapter extends FragmentPagerAdapter {
    
    private final Resources res;
    public int idActual;
    public DetalleClienteViewPagerAdapter.listener listener;
//    listener listener;


    @Inject
    public DetalleClienteViewPagerAdapter(@ActivityFragmentManager FragmentManager fm, Resources res) {
        super(fm);
        this.res = res;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0 :
                TabDetalleClienteFragment detallefragment= new TabDetalleClienteFragment();
                detallefragment.setAdapter(this);
                fragment=detallefragment;
                break;
            case 1 :
                PedidosClienteFragment pedidoFragment= new PedidosClienteFragment();
                pedidoFragment.setAdapter(this);
                listener=pedidoFragment;
                fragment=pedidoFragment;
                break;
        }
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String titleTab="";
        switch (position){
            case 0 : titleTab= "Detalle";break;
            case 1 : titleTab= "Pedidos";break;
        }

        return titleTab;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void setId(int idCliente) {
        idActual=idCliente;
    }

    public listener getlistener() {
        return listener;
    }


    public interface listener{
        void onActivityResultlistener(int requestCode, int resultCode, Intent data);
    }
}
