package com.kyaemp.perufarma.demo.ui.base.navigator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.utils.Constantes;
import com.kyaemp.perufarma.demo.utils.PlainConsumer;

public class ActivityNavigator implements Navigator {

    protected final BaseActivity activity;

    public ActivityNavigator(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public View inflate(int resource, ViewGroup root) {
        return activity.getLayoutInflater().inflate(resource, root);
    }

    @Override
    public void requestPermissions(String[] permissions, int resultCode) {
        ActivityCompat.requestPermissions(activity, permissions, resultCode);
    }

    @Override
    public int checkSelfPermission(@NonNull String permission) {
        return ContextCompat.checkSelfPermission(activity, permission);
    }

    @Override
    public void setResult(int resultCode, Intent data) {
        activity.setResult(resultCode, null);
    }

    @Override
    public void startService(Intent service) {
        activity.startService(service);
    }

    @Override
    public void stopService(Intent service) {
        activity.stopService(service);
    }

    @Override
    public void startDialog(DialogFragment dialog) {
        FragmentManager fm = activity.getSupportFragmentManager();
        dialog.setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_alert);
        dialog.show(fm, dialog.getClass().getName());
    }
    @Override
    public void startDialogFullScreen(DialogFragment dialog) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, dialog, "FullScreenFragment")
        .addToBackStack("Nuevo Pedido").commit();

    }

    @Override
    public void finishActivity() {
        activity.finish();
    }

    @Override
    public final void startActivity(@NonNull Intent intent) {
        activity.startActivity(intent);
    }

    @Override
    public final void startActivity(@NonNull String action) {
        activity.startActivity(new Intent(action));
    }

    @Override
    public final void startActivity(@NonNull String action, @NonNull Uri uri) {
        activity.startActivity(new Intent(action, uri));
    }

    @Override
    public final void startActivity(@NonNull Class<? extends Activity> activityClass) {
        startActivityInternal(activityClass, null, null);
    }

    @Override
    public void startActivity(@NonNull Class<? extends Activity> activityClass, @NonNull PlainConsumer<Intent> setArgsAction) {
        startActivityInternal(activityClass, setArgsAction, null);
    }

    @Override
    public void startActivity(@NonNull Class<? extends Activity> activityClass, Bundle args) {
        startActivityInternal(activityClass, intent -> intent.putExtra(EXTRA_ARG, args), null);
    }

    @Override
    public final void startActivity(@NonNull Class<? extends Activity> activityClass, @NonNull Parcelable arg) {
        startActivityInternal(activityClass, intent -> intent.putExtra(EXTRA_ARG, arg), null);
    }

    @Override
    public final void startActivity(@NonNull Class<? extends Activity> activityClass, @NonNull String arg) {
        startActivityInternal(activityClass, intent -> intent.putExtra(EXTRA_ARG, arg), null);
    }

    @Override
    public final void startActivity(@NonNull Class<? extends Activity> activityClass, int arg) {
        startActivityInternal(activityClass, intent -> intent.putExtra(EXTRA_ARG, arg), null);
    }

    @Override
    public final void startActivityForResult(@NonNull Class<? extends Activity> activityClass, int requestCode) {
        startActivityInternal(activityClass, null, requestCode);
    }

    @Override
    public void startActivityForResult(@NonNull Class<? extends Activity> activityClass, @NonNull PlainConsumer<Intent> setArgsAction, int requestCode) {
        startActivityInternal(activityClass, setArgsAction, requestCode);
    }

    @Override
    public final void startActivityForResult(@NonNull Class<? extends Activity> activityClass, @NonNull Parcelable arg, int requestCode) {
        startActivityInternal(activityClass, intent -> intent.putExtra(EXTRA_ARG, arg), requestCode);
    }

    @Override
    public final void startActivityForResult(@NonNull Class<? extends Activity> activityClass, @NonNull String arg, int requestCode) {
        startActivityInternal(activityClass, intent -> intent.putExtra(EXTRA_ARG, arg), requestCode);
    }

    @Override
    public final void startActivityForResult(@NonNull Class<? extends Activity> activityClass, int arg, int requestCode) {
        startActivityInternal(activityClass, intent -> intent.putExtra(EXTRA_ARG, arg), requestCode);
    }

    private void startActivityInternal(Class<? extends Activity> activityClass, PlainConsumer<Intent> setArgsAction, Integer requestCode) {
        Intent intent = new Intent(activity, activityClass);
        if (setArgsAction != null) {
            setArgsAction.accept(intent);
        }

        if (requestCode != null) {
            activity.startActivityForResult(intent, requestCode);
        } else {
            activity.startActivity(intent);
        }
    }

    @Override
    public final void replaceFragment(@IdRes int containerId, Fragment fragment, Bundle args) {
        replaceFragmentInternal(activity.getSupportFragmentManager(), containerId, fragment, null, args, false, null);
    }

    @Override
    public final void replaceFragment(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args) {
        replaceFragmentInternal(activity.getSupportFragmentManager(), containerId, fragment, fragmentTag, args, false, null);
    }

    @Override
    public final void replaceFragmentAndAddToBackStack(@IdRes int containerId, Fragment fragment, Bundle args, String backstackTag) {
        replaceFragmentInternal(activity.getSupportFragmentManager(), containerId, fragment, null, args, true, backstackTag);
    }

    @Override
    public final void replaceFragmentAndAddToBackStack(@IdRes int containerId, @NonNull Fragment fragment, @NonNull String fragmentTag, Bundle args, String backstackTag) {
        replaceFragmentInternal(activity.getSupportFragmentManager(), containerId, fragment, fragmentTag, args, true, backstackTag);
    }

    @Override
    public boolean isFinishing() {
        return activity.isFinishing();
    }

    @Override
    public boolean isLoggin() {
        return activity.isLoggin();
    }

    @Override
    public <T> Boolean savePreferences(T objectSave, String nameObject) {
        Boolean save=true;
        SharedPreferences.Editor editor=activity.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit();
        if (objectSave instanceof String){
            editor.putString(nameObject,(String) objectSave);
        }else if (objectSave instanceof Long){
            editor.putLong(nameObject,(Long) objectSave);
        }else if (objectSave instanceof Boolean){
            editor.putBoolean(nameObject,(Boolean) objectSave);
        }else if (objectSave instanceof Float){
            editor.putFloat(nameObject,(Float) objectSave);
        }else if (objectSave instanceof Integer){
            editor.putInt(nameObject,(Integer) objectSave);
        }else{
            save=false;
        }
        editor.apply();

        return save;
    }

    @Override
    public void cleanPreferences() {
        activity.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit().clear().apply();
    }

    protected final void replaceFragmentInternal(FragmentManager fm, @IdRes int containerId, Fragment fragment, String fragmentTag, Bundle args, boolean addToBackstack, String backstackTag) {
        if (args != null) {
            fragment.setArguments(args);
        }
        FragmentTransaction ft = fm.beginTransaction().replace(containerId, fragment, fragmentTag);
        if (addToBackstack) {
            ft.addToBackStack(backstackTag).commit();
            fm.executePendingTransactions();
        } else {
            ft.commitNow();
        }
    }

    @Override
    public void logout() {
        SharedPreferences.Editor editor = activity.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit();
        editor.putBoolean(Constantes.PREF_LOGGIN, false );
        editor.apply();
    }

    @Override
    public void setProductoSelect(CatalogoModel producto) {
        activity.setProductoSelect(producto);
    }
}
