package com.kyaemp.perufarma.demo.model.normal;

import android.location.Location;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by pedro.zevallos on 26/08/2017.
 */

public class PositionModel implements Serializable {

    private int id;
    private String deviceId;
    private Date time;
    private double latitude;
    private double longitude;
    private double altitude;
    private double speed;
    private double course;
    private double battery;
    private String nombre;
    private int guardar;


    public PositionModel() {
    }

    public PositionModel(int id, String deviceId, Date time, double latitude, double longitude, double altitude, double speed, double course, double battery, String nombre, int guardar) {
        this.id = id;
        this.deviceId = deviceId;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.speed = speed;
        this.course = course;
        this.battery = battery;
        this.nombre = nombre;
        this.guardar = guardar;
    }

    public PositionModel(String deviceId, Location location, double battery, String nombre, int guardar) {
        this.deviceId = deviceId;
        time = new Date(location.getTime());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        speed = location.getSpeed(); //
        course = location.getBearing();
        this.battery = battery;
        this.nombre=nombre;
        this.guardar=guardar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getCourse() {
        return course;
    }

    public void setCourse(double course) {
        this.course = course;
    }

    public double getBattery() {
        return battery;
    }

    public void setBattery(double battery) {
        this.battery = battery;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getGuardar() {
        return guardar;
    }

    public void setGuardar(int guardar) {
        this.guardar = guardar;
    }
}
