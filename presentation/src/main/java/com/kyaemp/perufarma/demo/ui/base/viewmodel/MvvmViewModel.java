package com.kyaemp.perufarma.demo.ui.base.viewmodel;

import android.databinding.Observable;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

public interface MvvmViewModel<V extends MvvmView> extends Observable {
    void attachView(V view, Bundle savedInstanceState);

    void detachView();

    void saveInstanceState(@NonNull Bundle outState);
}
