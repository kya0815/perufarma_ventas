package com.kyaemp.perufarma.demo.ui.nuevopedido;


import android.databinding.ObservableBoolean;
import android.view.MenuItem;
import android.view.View;

import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.model.observables.NuevoPedidoModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;


/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface NuevoPedidoMvvm {

    interface View extends MvvmView {

        void ocultarSearchView(boolean b);

        void ocultarTeclado();
    }

    interface ViewModel extends IViewModel<View> {
        // Properties
        ObservableBoolean getLoaded();
        ObservableBoolean getProductoSelect();

        void saveData();

        void setModel(NuevoPedidoModel model);

        NuevoPedidoModel getModel();

        void setProducto(CatalogoModel producto);
        CatalogoModel getProducto();

        void OnClickMenosCantidad(android.view.View view);
        void OnClickMasCantidad(android.view.View view);
        void OnClickDeleteProducto(android.view.View view);

        void setIdCliente(int intExtra);


//        GuardarPointModel getEntidad();
//
//        void setPoint(Double lat, Double lon);
//
//        void onClickAceptar(android.view.View view);
//
//        void onClickCancelar(android.view.View view);
    }
}
