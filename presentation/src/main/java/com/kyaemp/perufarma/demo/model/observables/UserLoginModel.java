package com.kyaemp.perufarma.demo.model.observables;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */

public class UserLoginModel extends BaseObservable {
    private String user;
    private String pass;

    public UserLoginModel(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    @Bindable
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
        notifyPropertyChanged(BR.user);
        notifyPropertyChanged(BR.error);
        notifyPropertyChanged(BR.valido);
    }

    @Bindable
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
        notifyChange();
    }

    @Bindable
    public String getError() {
        if (!user.isEmpty() && user.length()<=3) {
            return "ingrese un usuario válido";
        } else {
            return null;
        }
    }

    @Bindable
    public Boolean getValido() {
        return !user.isEmpty() && !pass.isEmpty() && getError() == null;
    }
}
