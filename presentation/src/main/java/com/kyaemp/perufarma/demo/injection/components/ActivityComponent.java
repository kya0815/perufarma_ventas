package com.kyaemp.perufarma.demo.injection.components;

import android.content.Context;
import android.support.v4.app.FragmentManager;

import com.kyaemp.perufarma.demo.injection.modules.ActivityModule;
import com.kyaemp.perufarma.demo.injection.modules.ViewModelModule;
import com.kyaemp.perufarma.demo.injection.qualifier.ActivityContext;
import com.kyaemp.perufarma.demo.injection.qualifier.ActivityFragmentManager;
import com.kyaemp.perufarma.demo.injection.scopes.PerActivity;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.clientes.ClientesActivity;
import com.kyaemp.perufarma.demo.ui.detallecliente.DetalleClienteActivity;
import com.kyaemp.perufarma.demo.ui.home.HomeActivity;
import com.kyaemp.perufarma.demo.ui.lista.ListaActivity;
import com.kyaemp.perufarma.demo.ui.login.LoginActivity;
import com.kyaemp.perufarma.demo.ui.map.MapActivity;
import com.kyaemp.perufarma.demo.ui.newcliente.NewClientesActivity;
import com.kyaemp.perufarma.demo.ui.nuevopedido.NuevoPedidoActivity;
import com.kyaemp.perufarma.demo.ui.signup.SignUpActivity;
import com.kyaemp.perufarma.demo.ui.splash.SplashActivity;

import dagger.Component;

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
@PerActivity
@Component(dependencies = AppComponent.class, modules = {ActivityModule.class, ViewModelModule.class})
public interface ActivityComponent extends AppComponent {

    @ActivityContext
    Context activityContext();

    @ActivityFragmentManager
    FragmentManager defaultFragmentManager();

    Navigator navigator();

    // create inject methods for your Activities here

    void inject(LoginActivity activity);

    void inject(SignUpActivity activity);

    void inject(SplashActivity activity);

    void inject(HomeActivity activity);

    void inject(MapActivity activity);

    void inject(ListaActivity activity);

    void inject(ClientesActivity activity);

    void inject(DetalleClienteActivity activity);

    void inject(NuevoPedidoActivity activity);

    void inject(NewClientesActivity activity);

}
