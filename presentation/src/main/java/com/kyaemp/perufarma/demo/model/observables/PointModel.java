package com.kyaemp.perufarma.demo.model.observables;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.kyaemp.perufarma.demo.BR;


/**
 * Created by pedro.zevallos on 9/07/2017.
 */

public class PointModel extends BaseObservable {

    private int id;
    private Double lat;
    private Double lon;
    private String name;
    private boolean enviado;

    public PointModel(int id, Double lat, Double lon, String name, boolean enviado) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.name = name;
        this.enviado = enviado;
    }

    @Bindable
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
        notifyPropertyChanged(BR.lat);
    }

    public String getLatitud() {
        return String.valueOf(lat);
    }

    public void setLatitud(String latitud) {
    }

    @Bindable
    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
        notifyPropertyChanged(BR.lon);
    }

    public String getLongitud() {
        return String.valueOf(lon);
    }

    public void setLongitud(String longitud) {
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
        notifyPropertyChanged(BR.enviado);
    }
}
