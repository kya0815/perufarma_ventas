package com.kyaemp.perufarma.demo.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;


/**
 * Created by penguin on 6/30/2016.
 * <p/>
 * SocketManager manages socket and internet connection.
 * It also provide listeners for connection status
 */
public class SocketManager {

    /**
     * The constant STATE_CONNECTING.
     */
    public static final int STATE_CONNECTING = 1;
    /**
     * The constant STATE_CONNECTED.
     */
    public static final int STATE_CONNECTED = 2;
    /**
     * The constant STATE_DISCONNECTED.
     */
    public static final int STATE_DISCONNECTED = 3;

    /**
     * The constant CONNECTING.
     */
    public static final String CONNECTING = "Connecting";
    /**
     * The constant CONNECTED.
     */
    public static final String CONNECTED = "Connected";
    /**
     * The constant DISCONNECTED.
     */
    public static final String DISCONNECTED = "Disconnected";

    private static SocketManager instance;
    private String deviceId;


    private SocketManager() {
    }



    /**
     * Gets instance.
     *
     * @return the instance
     */
    public synchronized static SocketManager getInstance() {
        if (instance == null) {
            instance = new SocketManager();
        }
        return instance;
    }

    /**
     * The constant TAG.
     */
    public static final String TAG = SocketManager.class.getSimpleName();
    private Socket socket;
    private List<OnSocketConnectionListener> onSocketConnectionListenerList;

    public void connectSocket(String host) {
        try {
            if(socket==null){
                String serverAddress = host;
                IO.Options opts = new IO.Options();
                opts.forceNew = true;
                opts.reconnection = true;
                opts.reconnectionDelay = 500;
                opts.reconnectionDelayMax = 1000;
                opts.reconnectionAttempts=5;
                socket = IO.socket(serverAddress,opts);
//                socket = IO.socket(serverAddress, opts);

                socket.on(Socket.EVENT_CONNECT, args -> {
                    fireSocketStatus(SocketManager.STATE_CONNECTED);
//                    Log.e(TAG, "socket connected");
                }).on(Socket.EVENT_RECONNECTING, args -> {
//                    Log.e(TAG, "Socket reconnecting");
                    fireSocketStatus(SocketManager.STATE_CONNECTING);
                }).on(Socket.EVENT_RECONNECT_FAILED, args -> {
//                    Log.e(TAG, "Socket reconnection failed");
                    fireSocketStatus(SocketManager.STATE_DISCONNECTED);
                }).on(Socket.EVENT_RECONNECT_ERROR, args -> {
//                    Log.e(TAG, "Socket reconnection error");
                    fireSocketStatus(SocketManager.STATE_DISCONNECTED);
                }).on(Socket.EVENT_CONNECT_ERROR, args -> {
//                    Log.e(TAG, "Socket connect error");
                    fireSocketStatus(SocketManager.STATE_DISCONNECTED);
//                        socket.disconnect();
                    socket.connect();
                    socket.emit("new device",deviceId);
                }).on(Socket.EVENT_DISCONNECT, args -> {
//                    Log.e(TAG, "Socket disconnect event");
                    fireSocketStatus(SocketManager.STATE_DISCONNECTED);
                    socket.connect();
                    socket.emit("new device",deviceId);
                }).on(Socket.EVENT_ERROR, args -> {
                    try {
                        final String error = (String) args[0];
//                        Log.e(TAG + " error EVENT_ERROR ", error);
                        if (error.contains("Unauthorized") && !socket.connected()) {
                            if (onSocketConnectionListenerList != null) {
                                for (final OnSocketConnectionListener listener : onSocketConnectionListenerList) {
                                    new Handler(Looper.getMainLooper())
                                            .post(listener::onSocketEventFailed);
                                }
                            }
                        }
                    } catch (Exception e) {
//                        Log.e(TAG, e.getMessage() != null ? e.getMessage() : "");
                    }
                }).on("Error", args -> Log.d(TAG, " Error"))
                        .on("server:respuesta:newchat",args -> {
                            try{
                               if((boolean)(args[0]))
                                   SendCorreoPrimerChat();
                            }catch (Exception e){
                                Log.d("respuestaprimerChat",e.getMessage());
                            }


                        });
                socket.connect();
                socket.emit("new device",deviceId);
            }else if(!socket.connected()){
                socket.connect();
                socket.emit("new device",deviceId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SendCorreoPrimerChat(){
        for(OnSocketConnectionListener listener: onSocketConnectionListenerList){
            listener.onSocketSendCorreoPrimerChat();
        }
    }


    private int lastState = -1;

    /**
     * Fire socket status intent.
     *
     * @param socketState the socket state
     */
    public synchronized void fireSocketStatus(final int socketState) {
        if(onSocketConnectionListenerList !=null && lastState!=socketState){
            lastState = socketState;
            new Handler(Looper.getMainLooper()).post(() -> {
                for(OnSocketConnectionListener listener: onSocketConnectionListenerList){
                    listener.onSocketConnectionStateChange(socketState);
                }
            });
            new Handler(Looper.getMainLooper()).postDelayed(() -> lastState=-1,1000);
        }
    }

    /**
     * Fire internet status intent.
     *
     * @param socketState the socket state
     */
    public synchronized void fireInternetStatusIntent(final int socketState) {
        new Handler(Looper.getMainLooper()).post(() -> {
            if(onSocketConnectionListenerList !=null){
                for(OnSocketConnectionListener listener: onSocketConnectionListenerList){
                    listener.onInternetConnectionStateChange(socketState);
                }
            }
        });
    }

    /**
     * Gets socket.
     *
     * @return the socket
     */
    public Socket getSocket() {
        return socket;
    }
    public void setDeviceID(String id) {
        if(deviceId==null){
            deviceId=id;
        }
    }

    /**
     * Sets socket.
     *
     * @param socket the socket
     */
    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    /**
     * Destroy.
     */
    public void destroy(){
        if (socket != null) {
            socket.off();
            socket.disconnect();
            socket.close();
            socket=null;
        }
    }

    /**
     * Sets socket connection listener.
     *
     * @param onSocketConnectionListenerListener the on socket connection listener listener
     */
    public void setSocketConnectionListener(OnSocketConnectionListener onSocketConnectionListenerListener) {
        if(onSocketConnectionListenerList ==null){
            onSocketConnectionListenerList = new ArrayList<>();
            onSocketConnectionListenerList.add(onSocketConnectionListenerListener);
        }else if(!onSocketConnectionListenerList.contains(onSocketConnectionListenerListener)){
            onSocketConnectionListenerList.add(onSocketConnectionListenerListener);
        }
    }

    /**
     * Remove socket connection listener.
     *
     * @param onSocketConnectionListenerListener the on socket connection listener listener
     */
    public void removeSocketConnectionListener(OnSocketConnectionListener onSocketConnectionListenerListener) {
        if(onSocketConnectionListenerList !=null
                && onSocketConnectionListenerList.contains(onSocketConnectionListenerListener)){
            onSocketConnectionListenerList.remove(onSocketConnectionListenerListener);
        }
    }

    /**
     * Remove all socket connection listener.
     */
    public void removeAllSocketConnectionListener() {
        if(onSocketConnectionListenerList !=null){
            onSocketConnectionListenerList.clear();
        }
    }

    /**
     * The type Net receiver.
     */
    public static class NetReceiver extends BroadcastReceiver {

        /**
         * The Tag.
         */
        public final String TAG = NetReceiver.class.getSimpleName();

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm =
                    (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();

            SocketManager.getInstance().fireInternetStatusIntent(
                    isConnected? SocketManager.STATE_CONNECTED: SocketManager.STATE_DISCONNECTED);
            if (isConnected) {
                if(SocketManager.getInstance().getSocket()!=null
                        && !SocketManager.getInstance().getSocket().connected()){
                    SocketManager.getInstance().fireSocketStatus(SocketManager.STATE_CONNECTING);
                }
                PowerManager powerManager =
                        (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                boolean isScreenOn;
                if (android.os.Build.VERSION.SDK_INT
                        >= android.os.Build.VERSION_CODES.KITKAT_WATCH) {
                    isScreenOn = powerManager.isInteractive();
                }else{
                    //noinspection deprecation
                    isScreenOn = powerManager.isScreenOn();
                }

                if (isScreenOn && SocketManager.getInstance().getSocket() !=null) {
                    Log.d(TAG, "NetReceiver: Connecting Socket");
                    if(!SocketManager.getInstance().getSocket().connected()){
                        SocketManager.getInstance().getSocket().connect();
                        SocketManager.getInstance().getSocket().emit("new device",getInstance().deviceId);
                    }
                }
            }else{
                SocketManager.getInstance().fireSocketStatus(SocketManager.STATE_DISCONNECTED);
                if(SocketManager.getInstance().getSocket() !=null){
                    Log.d(TAG, "NetReceiver: disconnecting socket");
                    SocketManager.getInstance().getSocket().disconnect();
                }
            }
        }
    }
}