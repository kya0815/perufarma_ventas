package com.kyaemp.perufarma.demo.ui.map;

import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.MapActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
//import com.squareup.leakcanary.RefWatcher;


public class MapActivity extends BaseActivity<MapActivityBinding, MapMvvm.ViewModel> implements MapMvvm.View, OnMapReadyCallback {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.map_activity);
        setSupportActionBar(binding.toolbar);
        setDisplayHomeAsUpEnabled();
        setTitle("Mapa");
        llamarMapa(savedInstanceState);
    }

    private void llamarMapa(Bundle savedInstanceState) {
        try {
            if (binding.map != null) {

                binding.map.onCreate(savedInstanceState);
                binding.map.onResume();
                binding.map.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        RefWatcher refWatcher = DemoApplication.getRefWatcher(this);
//        refWatcher.watch(this);
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        viewModel.onMapReady(googleMap);
    }

    public void setReloadMap() {
        viewModel.reloadMap();
    }

}