package com.kyaemp.perufarma.demo.ui.splash;

import android.os.Bundle;
import android.view.MotionEvent;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.SplashActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
//import com.squareup.leakcanary.RefWatcher;


public class SplashActivity extends BaseActivity<SplashActivityBinding, SplashMvvm.ViewModel> implements SplashMvvm.View {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.splash_activity);
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        RefWatcher refWatcher = DemoApplication.getRefWatcher(this);
//        refWatcher.watch(this);
//    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        viewModel.onTouchScreeen(event);
        return super.onTouchEvent(event);
    }
}