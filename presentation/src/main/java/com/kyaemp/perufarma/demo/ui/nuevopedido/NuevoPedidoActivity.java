package com.kyaemp.perufarma.demo.ui.nuevopedido;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.NuevoPedidoActivityBinding;
import com.kyaemp.perufarma.demo.model.observables.CatalogoModel;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.nuevopedido.recyclerview.PedidoItemAdapter;

import javax.inject.Inject;


public class NuevoPedidoActivity extends BaseActivity<NuevoPedidoActivityBinding, NuevoPedidoMvvm.ViewModel> implements NuevoPedidoMvvm.View{
    @Inject
    PedidoItemAdapter adapter;
    private SearchView searchView;
    private Menu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.nuevo_pedido_activity);
        configRecycler();
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("Nuevo Pedido");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_close);
        initView();
    }
    private void configRecycler() {
        binding.rvItems.setHasFixedSize(true);
        binding.rvItems.setLayoutManager(new LinearLayoutManager(this));
        binding.rvItems.setAdapter(adapter);
    }
    private void initView() {
        viewModel.setIdCliente(getIntent().getIntExtra(Navigator.EXTRA_ARG,0));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_nuevo_pedido, menu);
        this.menu = menu;
        MenuItem saveMenu= menu.findItem(R.id.menu_item_save);
        MenuItem searchMenu = menu.findItem(R.id.menu_item_search);
        iniciarMenuBusqueda(searchMenu);
        tintMenuIcon(saveMenu);
        tintMenuIcon(searchMenu);
        return super.onCreateOptionsMenu(menu);
    }

    private void iniciarMenuBusqueda(MenuItem searchMenu) {
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) searchMenu
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_item_save:
                viewModel.saveData();
                break;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void tintMenuIcon(MenuItem menuItem) {
        Drawable favoriteIcon = DrawableCompat.wrap(menuItem.getIcon().mutate());
        DrawableCompat.setTint(favoriteIcon, color(R.color.white));
        menuItem.setIcon(favoriteIcon);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void setProductoSelect(CatalogoModel producto) {
        viewModel.setProducto(producto);
    }

    @Override
    public void ocultarSearchView(boolean b) {

        searchView.setVisibility(b ? View.GONE : View.VISIBLE);
    }

    @Override
    public void ocultarTeclado() {
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }
}