package com.kyaemp.perufarma.demo.ui.home;


import android.databinding.ObservableBoolean;

import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface HomeMvvm {

    interface View extends MvvmView {

    }

    interface ViewModel extends IViewModel<View> {
        // Properties
        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);

        ObservableBoolean getActive();

        void onClickValidarPermisos(android.view.View v);

        void logout();
    }
}
