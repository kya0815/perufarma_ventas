package com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.recyclerview;

import android.view.View;

import com.kyaemp.perufarma.demo.databinding.ItemPedidoBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseViewHolder;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

public class PedidosItemViewHolder extends BaseViewHolder<ItemPedidoBinding, PedidosItemMvvm.ViewModel> implements MvvmView {

    public PedidosItemViewHolder(View v) {
        super(v);

        viewHolderComponent().inject(this);
        bindContentView(v);
    }
}
