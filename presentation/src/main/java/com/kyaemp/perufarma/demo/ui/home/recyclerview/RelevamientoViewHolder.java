package com.kyaemp.perufarma.demo.ui.home.recyclerview;

import android.view.View;

import com.kyaemp.perufarma.demo.databinding.HomeActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseViewHolder;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

public class RelevamientoViewHolder extends BaseViewHolder<HomeActivityBinding, RelevamientoMvvm.ViewModel> implements MvvmView {

    public RelevamientoViewHolder(View v) {
        super(v);

        viewHolderComponent().inject(this);
        bindContentView(v);
    }
}
