package com.kyaemp.perufarma.demo.ui.base.navigator;

import android.support.v4.app.DialogFragment;

import com.kyaemp.perufarma.demo.ui.base.BaseActivity;

public class DialogFragmentNavigator extends ActivityNavigator implements DialogNavigator {

    private final DialogFragment dialogFragment;

    public DialogFragmentNavigator(DialogFragment dialogFragment) {
        super((BaseActivity)dialogFragment.getActivity());
        this.dialogFragment = dialogFragment;
    }

    @Override
    public void dismiss() {
        dialogFragment.dismiss();
    }

}
