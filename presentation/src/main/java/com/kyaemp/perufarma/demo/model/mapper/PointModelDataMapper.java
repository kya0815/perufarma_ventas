/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.demo.model.mapper;

import com.kyaemp.perufarma.demo.model.observables.PointModel;
import com.kyaemp.perufarma.domain.model.Point;

import java.util.ArrayList;
import java.util.List;


public class PointModelDataMapper {

    public PointModel transform(Point point) {
        if (point == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new PointModel(point.getId(), point.getLat(), point.getLon(), point.getName(), point.isEnviado());
    }

    public Point transform(PointModel pointModel) {
        if (pointModel == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new Point(pointModel.getId(), pointModel.getLat(), pointModel.getLon(), pointModel.getName(), pointModel.isEnviado());
    }

    public <R, T> List<R> transform(List<T> c) {
        List<R> points = new ArrayList<>();
        if (c != null && !c.isEmpty()) {
            for (T t : c) {
                points.add(transform(t));
            }
        }
        return points;
    }

    private <R, T> R transform(T t) {
        if (t instanceof Point) {
            return (R) transform((Point) t);
        } else if (t instanceof PointModel) {
            return (R) transform((PointModel) t);
        }
        return null;
    }
}
