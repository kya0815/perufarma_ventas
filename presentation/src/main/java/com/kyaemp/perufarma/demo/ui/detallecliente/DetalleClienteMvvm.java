package com.kyaemp.perufarma.demo.ui.detallecliente;


import android.databinding.ObservableBoolean;

import com.google.android.gms.maps.GoogleMap;
import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.ui.IViewModel;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;

/**
 * Created by pedro.zevallos on 10/07/2017.
 */

public interface DetalleClienteMvvm {

    interface View extends MvvmView {

        void setTitleToolbar(String cliente_nombre);
    }

    interface ViewModel extends IViewModel<View> {
        // Properties
        ObservableBoolean isLoaded();

        ClientesModel getCliente();

        void setIdCliente(int idCliente);

        void onMapReady(GoogleMap googleMap);
    }
}
