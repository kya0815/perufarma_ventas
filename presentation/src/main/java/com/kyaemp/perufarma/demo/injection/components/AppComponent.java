package com.kyaemp.perufarma.demo.injection.components;

import android.content.Context;
import android.content.res.Resources;

import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.ClientesEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PedidoEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PointsEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PositionsEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.RelevamientoEntityRepository;
import com.kyaemp.perufarma.demo.injection.modules.AppModule;
import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.model.mapper.CatalogoModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.PointModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.PositionModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.RelevamientoModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.RespuestaServerModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.UserSignUpModelDataMapper;
import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.interactor.DeletePositionDBUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarCatalogoToOnlineUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPedidosClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionBDUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionServerUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPuntosServerUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPuntosUseCase;
import com.kyaemp.perufarma.domain.interactor.LoginUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerCatalogoByIDUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerCatalogoLocalUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerClientesUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerDetalleClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerLastPositionServerUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerPedidosClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerPuntosUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerRelevamientosUseCase;
import com.kyaemp.perufarma.domain.interactor.SignUpUseCase;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;
import com.kyaemp.perufarma.domain.repository.ClientesRepository;
import com.kyaemp.perufarma.domain.repository.LoginRepository;
import com.kyaemp.perufarma.domain.repository.PedidosRepository;
import com.kyaemp.perufarma.domain.repository.PointsRepository;
import com.kyaemp.perufarma.domain.repository.PositionsRepository;
import com.kyaemp.perufarma.domain.repository.RelevamientoRepository;
import com.kyaemp.perufarma.domain.repository.SignUpRepository;

import javax.inject.Singleton;

import dagger.Component;

//import com.squareup.leakcanary.RefWatcher;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    @AppContext
    Context appContext();
    Context context();
    Resources resources();
//    SocketServices socketServices();
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
    LoginUseCase loginUseCase();
    LoginRepository loginRepository();
    ObtenerPuntosUseCase obtenerPuntosUseCase();
    GuardarPuntosUseCase guardarPuntosUseCase();
    GuardarPuntosServerUseCase guardarPuntosServerUseCase();
    PointsRepository pointsRepository();
    PointModelDataMapper pointModelDataMapper();
    PointsEntityRepository pointsEntityRepository();
    RespuestaServerModelDataMapper respuestaServerModelDataMapper();

    RelevamientoRepository relevamientoRepository();
    ObtenerRelevamientosUseCase obtenerRelevamientosUseCase();
    RelevamientoEntityRepository relevamientoEntityRepository();
    RelevamientoModelDataMapper relevamientoModelDataMapper();

    CatalogoRepository catalogoRepository();
    GuardarCatalogoToOnlineUseCase guardarCatalogoToOnlineUseCase();
    ObtenerCatalogoLocalUseCase obtenerCatalogoLocalUseCase();
    CatalogoEntityRepository catalogoEntityRepository();
    CatalogoModelDataMapper catalogoModelDataMapper();

    SignUpRepository signUpRepository();
    SignUpUseCase signUpUseCase();
    UserSignUpModelDataMapper userSignUpModelDataMapper();

    PositionsEntityRepository positionsEntityRepository();
    PositionsRepository positionsRepository();
    PositionModelDataMapper positionModelDataMapper();

    DeletePositionDBUseCase deletePositionDbUseCase();
    GuardarPositionBDUseCase guardarPositionBdUseCase();
    GuardarPositionServerUseCase guardarPositionServerUseCase();
    ObtenerLastPositionServerUseCase obtenerLastPositionServerUseCase();

    ClientesEntityRepository clientesEntityRepository();
    ClientesRepository clientesRepository();
    ClienteModelDataMapper clienteModelDataMapper();
    ObtenerClientesUseCase obtenerClientesUseCase();
    ObtenerDetalleClienteUseCase obtenerDetalleClienteUseCase();



    PedidosRepository pedidosRepository();
    PedidoEntityRepository pedidoEntityRepository();
    GuardarPedidosClienteUseCase guardarPedidosClienteUseCase();
    ObtenerPedidosClienteUseCase obtenerPedidosClienteUseCase();
    ObtenerCatalogoByIDUseCase obtenerCatalogoByIdUseCase();
}
