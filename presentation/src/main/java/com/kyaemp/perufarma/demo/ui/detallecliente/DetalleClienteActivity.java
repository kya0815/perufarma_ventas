package com.kyaemp.perufarma.demo.ui.detallecliente;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.kyaemp.perufarma.demo.R;
import com.kyaemp.perufarma.demo.databinding.DetalleClienteActivityBinding;
import com.kyaemp.perufarma.demo.ui.base.BaseActivity;
import com.kyaemp.perufarma.demo.ui.base.navigator.Navigator;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.DetalleClienteViewPagerAdapter;
import com.kyaemp.perufarma.demo.ui.detallecliente.viewpager.pedidos.PedidosClienteViewModel;

import javax.inject.Inject;


public class DetalleClienteActivity extends BaseActivity<DetalleClienteActivityBinding, DetalleClienteMvvm.ViewModel> implements DetalleClienteMvvm.View, OnMapReadyCallback {

    @Inject
    DetalleClienteViewPagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setAndBindContentView(savedInstanceState, R.layout.detalle_cliente_activity);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        viewModel.setIdCliente(getIntent().getIntExtra(Navigator.EXTRA_ARG,0));
        binding.viewpager.setOffscreenPageLimit(3);
        binding.viewpager.setAdapter(adapter);
        binding.detailTabs.setupWithViewPager(binding.viewpager);
        llamarMapa(savedInstanceState);
    }
    private void llamarMapa(Bundle savedInstanceState) {
        try {
            if (binding.map != null) {

                binding.map.onCreate(savedInstanceState);
                binding.map.onResume();
                binding.map.getMapAsync(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void setTitleToolbar(String cliente_nombre) {
        setTitle(cliente_nombre);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        viewModel.onMapReady(googleMap);
    }

    @Override
    public void onBackPressed() {
//        if (getFragmentManager().getBackStackEntryCount() > 0) {
//            getFragmentManager().popBackStack();
//        } else {
            super.onBackPressed();
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode== PedidosClienteViewModel.COD_PEDIDO){
            adapter.getlistener().onActivityResultlistener(requestCode, resultCode, data);
        }
    }
}