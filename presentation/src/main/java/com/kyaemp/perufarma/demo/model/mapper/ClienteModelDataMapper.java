package com.kyaemp.perufarma.demo.model.mapper;

import com.kyaemp.perufarma.demo.model.observables.ClientesModel;
import com.kyaemp.perufarma.demo.model.observables.PedidoModel;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;

import java.util.ArrayList;
import java.util.List;


public class ClienteModelDataMapper {

    public ClientesModel transform(Cliente item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }

        return new ClientesModel(item.getId(),item.getCliente_nombre(),item.getCliente_ruc(),item.getCliente_codigo(),
                item.getUltimo_pedido(),item.getMonto_vencido(),item.getTelefono(),item.getDireccion(),
                item.getDepartamen(),item.getProvincia(),item.getDistrito(),item.getCorreo(),item.getLatitud(),item.getLongitud());
    }

    public Cliente transform(ClientesModel item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
//        itemOut.setDepartamen(itemIn.getDepartamen());
//        itemOut.setProvincia(itemIn.getProvincia());
//        itemOut.setDistrito(itemIn.getDistrito());
//        itemOut.setCorreo(itemIn.getCorreo());
        return new Cliente(item.getId(),item.getCliente_nombre(),Long.valueOf(item.getCliente_ruc()),Integer.valueOf(item.getCliente_codigo()),
                item.getUltimo_pedido(),item.getMonto_vencido(),item.getTelefono(),item.getDireccion(),
                item.getDepartamen(),item.getProvincia(),item.getDistrito(),item.getCorreo(),item.getLatitud(),item.getLongitud());
    }

    public <R, T> List<R> transform(List<T> c) {
        List<R> items = new ArrayList<>();
        if (c != null && !c.isEmpty()) {
            for (T t : c) {
                items.add(transform(t));
            }
        }
        return items;
    }

    private <R, T> R transform(T t) {
        if (t instanceof Cliente) {
            return (R) transform((Cliente) t);
        } else if (t instanceof ClientesModel) {
            return (R) transform((ClientesModel) t);
        }
        return null;
    }

    public PedidoModel transformToPresenter(Pedido item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new PedidoModel(item.getId(),item.getCodigo_item(),item.getCantidad(),item.getCliente_codigo());
    }

    public Pedido transformToData(PedidoModel item) {
        if (item == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        return new Pedido(item.getId(),item.getCodigo_item(),item.getCantidad(),item.getCliente_codigo());
    }

}
