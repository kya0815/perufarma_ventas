package com.kyaemp.perufarma.demo.ui.clientes;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

public class MainContainerBehaviour extends CoordinatorLayout.Behavior<View> {

    public MainContainerBehaviour(Context context, AttributeSet attrs) {}

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) child.getLayoutParams();

        if(dependency instanceof AppBarLayout) {
            layoutParams.topMargin = dependency.getMeasuredHeight();
            child.setLayoutParams(layoutParams);
        }

        return true;
    }

}
