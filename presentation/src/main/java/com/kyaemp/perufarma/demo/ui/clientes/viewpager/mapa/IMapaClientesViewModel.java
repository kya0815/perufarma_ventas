package com.kyaemp.perufarma.demo.ui.clientes.viewpager.mapa;

import com.google.android.gms.maps.GoogleMap;
import com.kyaemp.perufarma.demo.ui.base.view.MvvmView;
import com.kyaemp.perufarma.demo.ui.base.viewmodel.MvvmViewModel;

/* Copyright 2016 Patrick Löwenstein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */
public interface IMapaClientesViewModel {
    interface View extends MvvmView {

    }

    interface ViewModel extends MvvmViewModel<View> {
        // Properties
        void onMapReady(GoogleMap googleMap);

    }
}
