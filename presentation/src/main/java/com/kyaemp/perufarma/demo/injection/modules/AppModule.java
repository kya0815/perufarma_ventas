package com.kyaemp.perufarma.demo.injection.modules;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.ClientesEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PedidoEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PointsEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PositionsEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.RelevamientoEntityRepository;
import com.kyaemp.perufarma.data.executor.JobExecutor;
import com.kyaemp.perufarma.data.repository.CatalogoDataRepository;
import com.kyaemp.perufarma.data.repository.ClientesDataRepository;
import com.kyaemp.perufarma.data.repository.LoginDataRepository;
import com.kyaemp.perufarma.data.repository.PedidosDataRepository;
import com.kyaemp.perufarma.data.repository.PointsDataRepository;
import com.kyaemp.perufarma.data.repository.PositionsDataRepository;
import com.kyaemp.perufarma.data.repository.RelevamientoDataRepository;
import com.kyaemp.perufarma.data.repository.SignUpDataRepository;
import com.kyaemp.perufarma.demo.UIThread;
import com.kyaemp.perufarma.demo.injection.qualifier.AppContext;
import com.kyaemp.perufarma.demo.model.mapper.CatalogoModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.ClienteModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.PointModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.PositionModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.RelevamientoModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.RespuestaServerModelDataMapper;
import com.kyaemp.perufarma.demo.model.mapper.UserSignUpModelDataMapper;
import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.interactor.DeletePositionDBUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarCatalogoToOnlineUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPedidosClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionBDUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPositionServerUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPuntosServerUseCase;
import com.kyaemp.perufarma.domain.interactor.GuardarPuntosUseCase;
import com.kyaemp.perufarma.domain.interactor.LoginUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerCatalogoByIDUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerCatalogoLocalUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerClientesUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerDetalleClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerLastPositionServerUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerPedidosClienteUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerPuntosUseCase;
import com.kyaemp.perufarma.domain.interactor.ObtenerRelevamientosUseCase;
import com.kyaemp.perufarma.domain.interactor.SignUpUseCase;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;
import com.kyaemp.perufarma.domain.repository.ClientesRepository;
import com.kyaemp.perufarma.domain.repository.LoginRepository;
import com.kyaemp.perufarma.domain.repository.PedidosRepository;
import com.kyaemp.perufarma.domain.repository.PointsRepository;
import com.kyaemp.perufarma.domain.repository.PositionsRepository;
import com.kyaemp.perufarma.domain.repository.RelevamientoRepository;
import com.kyaemp.perufarma.domain.repository.SignUpRepository;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

//import com.squareup.leakcanary.LeakCanary;
//import com.squareup.leakcanary.RefWatcher;

@Module
public class AppModule {

    private final Application mApp;

    public AppModule(Application app) {
        mApp = app;
    }

    @Provides
    @Singleton
    @AppContext
    Context provideAppContext() {
        return mApp;
    }

    @Provides
    Context provideContext() {
        return mApp;
    }

    @Provides
    @Singleton
    Resources provideResources() {
        return mApp.getResources();
    }


    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }

//    @Provides
//    @Singleton
//    RefWatcher provideRefWatcher() {
//        return LeakCanary.install(mApp);
//    }
    @Provides
    @Named("loginUseCase")
    LoginUseCase provideLoginUseCase(LoginRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new LoginUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }
    @Provides
    LoginRepository provideLoginRepository(LoginDataRepository loginDataRepository) {
        return loginDataRepository;
    }
    @Provides
    @Named("obtenerPuntosUseCase")
    ObtenerPuntosUseCase provideObtenerPuntosUseCase(PointsRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerPuntosUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @Named("guardarPuntosUseCase")
    GuardarPuntosUseCase provideGuardarPuntosUseCase(PointsRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GuardarPuntosUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @Named("guardarPuntosServerUseCase")
    GuardarPuntosServerUseCase provideGuardarPuntosServerUseCase(PointsRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GuardarPuntosServerUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    @Named("obtenerRelevamientosUseCase")
    ObtenerRelevamientosUseCase provideObtenerRelevamientosUseCase(RelevamientoRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerRelevamientosUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }

    @Provides
    RelevamientoRepository provideRelevamientoRepository(RelevamientoDataRepository pointsDataRepository) {
        return pointsDataRepository;
    }

    @Provides
    RelevamientoModelDataMapper provideRelevamientoModelDataMapper() {
        return new RelevamientoModelDataMapper();
    }

    @Provides
    PointsRepository providePointsRepository(PointsDataRepository pointsDataRepository) {
        return pointsDataRepository;
    }

    @Provides
    PointModelDataMapper providePoinModelDataMapper() {
        return new PointModelDataMapper();
    }

    @Provides
    RespuestaServerModelDataMapper provideRespuestaServerModelDataMapper() {
        return new RespuestaServerModelDataMapper();
    }
    @Provides
    PointsEntityRepository providePointsEntityRepository() {
        return new PointsEntityRepository(mApp);
    }
    @Provides
    RelevamientoEntityRepository provideRelevamientoEntityRepository() {
        return new RelevamientoEntityRepository(mApp);
    }

//    @Provides
//    SocketServices provideSocketServices() {
//        return new SocketServices(mApp);
//    }

    @Provides
    CatalogoRepository provideCatalogoRepository(CatalogoDataRepository dataRepository) {
        return dataRepository;
    }
    @Provides
    @Named("guardarCatalogoToOnlineUseCase")
    GuardarCatalogoToOnlineUseCase provideGuardarCatalogoToOnlineUseCase(CatalogoRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GuardarCatalogoToOnlineUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }
    @Provides
    @Named("obtenerCatalogoLocalUseCase")
    ObtenerCatalogoLocalUseCase provideObtenerCatalogoLocalUseCase(CatalogoRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerCatalogoLocalUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }
    @Provides
    CatalogoEntityRepository provideCatalogoEntityRepository() {
        return new CatalogoEntityRepository(mApp);
    }

    @Provides
    CatalogoModelDataMapper provideCatalogoModelDataMapper() {
        return new CatalogoModelDataMapper();
    }

    @Provides
    SignUpRepository provideSignUpRepository(SignUpDataRepository dataRepository) {
        return dataRepository;
    }

    @Provides
    @Named("signUpUseCase")
    SignUpUseCase provideSignUpUseCase(SignUpRepository indicatorRepository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new SignUpUseCase(indicatorRepository, threadExecutor, postExecutionThread);
    }
    @Provides
    UserSignUpModelDataMapper provideUserSignUpModelDataMapper() {
        return new UserSignUpModelDataMapper();
    }

    @Provides
    @Singleton
    PositionsEntityRepository providePositionsEntityRepository() {
        return new PositionsEntityRepository(mApp);
    }

    @Provides
    @Singleton
    PositionsRepository providePositionsRepository(PositionsDataRepository dataRepository) {
        return dataRepository;
    }



    @Provides
    @Singleton
    PositionModelDataMapper providePositionModelDataMapper() {
        return new PositionModelDataMapper();
    }


    @Provides
    @Singleton
    @Named("deletePositionDBUseCase")
    DeletePositionDBUseCase provideDeletePositionDBUseCase(PositionsRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new DeletePositionDBUseCase(repository, threadExecutor, postExecutionThread);
    }
    @Provides
    @Singleton
    @Named("guardarPositionBDUseCase")
    GuardarPositionBDUseCase provideGuardarPositionBDUseCase(PositionsRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GuardarPositionBDUseCase(repository, threadExecutor, postExecutionThread);
    }
    @Provides
    @Singleton
    @Named("guardarPositionServerUseCase")
    GuardarPositionServerUseCase provideGuardarPositionServerUseCase(PositionsRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GuardarPositionServerUseCase(repository, threadExecutor, postExecutionThread);
    }
    @Provides
    @Singleton
    @Named("obtenerLastPositionServerUseCase")
    ObtenerLastPositionServerUseCase provideObtenerLastPositionServerUseCase(PositionsRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerLastPositionServerUseCase(repository, threadExecutor, postExecutionThread);
    }


    //Clientes


    @Provides
    @Singleton
    ClientesEntityRepository provideClientesEntityRepository() {
        return new ClientesEntityRepository(mApp);
    }
    @Provides
    ClientesRepository provideClientesRepository(ClientesDataRepository dataRepository) {
        return dataRepository;
    }
    @Provides
    @Singleton
    ClienteModelDataMapper provideClienteModelDataMapper() {
        return new ClienteModelDataMapper();
    }
    @Provides
    @Singleton
    @Named("obtenerClientesUseCase")
    ObtenerClientesUseCase provideObtenerClientesUseCase(ClientesRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerClientesUseCase(repository, threadExecutor, postExecutionThread);
    }

    @Provides
    @Singleton
    @Named("obtenerDetalleClienteUseCase")
    ObtenerDetalleClienteUseCase provideObtenerDetalleClienteUseCase(ClientesRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerDetalleClienteUseCase(repository, threadExecutor, postExecutionThread);
    }



    @Provides
    @Singleton
    PedidosRepository providePedidosRepository(PedidosDataRepository dataRepository) {
        return dataRepository;
    }

    @Provides
    @Singleton
    PedidoEntityRepository providePedidoEntityRepository() {
        return new PedidoEntityRepository(mApp);
    }

    @Provides
    @Singleton
    @Named("guardarPedidosClienteUseCase")
    GuardarPedidosClienteUseCase provideGuardarPedidosClienteUseCase(PedidosRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new GuardarPedidosClienteUseCase(repository, threadExecutor, postExecutionThread);
    }

    @Provides
    @Singleton
    @Named("obtenerPedidosClienteUseCase")
    ObtenerPedidosClienteUseCase provideObtenerPedidosClienteUseCase(PedidosRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerPedidosClienteUseCase(repository, threadExecutor, postExecutionThread);
    }
    @Provides
    @Singleton
    @Named("obtenerCatalogoByIDUseCase")
    ObtenerCatalogoByIDUseCase provideObtenerCatalogoByIDUseCase(CatalogoRepository repository, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        return new ObtenerCatalogoByIDUseCase(repository, threadExecutor, postExecutionThread);
    }
}
