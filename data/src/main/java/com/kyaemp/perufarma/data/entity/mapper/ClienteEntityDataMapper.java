package com.kyaemp.perufarma.data.entity.mapper;

import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PedidoEntity;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link RespuestaServerEntity} (in the data layer) to {@link RespuestaServer} in the
 * domain layer.
 */
@Singleton
public class ClienteEntityDataMapper {

  @Inject
  ClienteEntityDataMapper() {}

  public Cliente transform(ClientesEntity itemIn) {
    Cliente itemOut = new Cliente();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setCliente_nombre(itemIn.getCliente_nombre());
      itemOut.setCliente_ruc(itemIn.getCliente_ruc());
      itemOut.setCliente_codigo(itemIn.getCliente_codigo());
      itemOut.setUltimo_pedido(itemIn.getUltimo_pedido());
      itemOut.setMonto_vencido(itemIn.getMonto_vencido());
      itemOut.setTelefono(itemIn.getTelefono());
      itemOut.setDireccion(itemIn.getDireccion());
      itemOut.setDepartamen(itemIn.getDepartamen());
      itemOut.setProvincia(itemIn.getProvincia());
      itemOut.setDistrito(itemIn.getDistrito());
      itemOut.setCorreo(itemIn.getCorreo());
      itemOut.setLatitud(itemIn.getLatitud());
      itemOut.setLongitud(itemIn.getLongitud());
    }
    return itemOut;
  }

  public ClientesEntity transform(Cliente itemIn) {
    ClientesEntity itemOut = new ClientesEntity();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setCliente_nombre(itemIn.getCliente_nombre());
      itemOut.setCliente_ruc(itemIn.getCliente_ruc());
      itemOut.setCliente_codigo(itemIn.getCliente_codigo());
      itemOut.setUltimo_pedido(itemIn.getUltimo_pedido());
      itemOut.setMonto_vencido(itemIn.getMonto_vencido());
      itemOut.setTelefono(itemIn.getTelefono());
      itemOut.setDireccion(itemIn.getDireccion());
      itemOut.setDepartamen(itemIn.getDepartamen());
      itemOut.setProvincia(itemIn.getProvincia());
      itemOut.setDistrito(itemIn.getDistrito());
      itemOut.setCorreo(itemIn.getCorreo());
      itemOut.setLatitud(itemIn.getLatitud());
      itemOut.setLongitud(itemIn.getLongitud());
    }
    return itemOut;
  }

  public <R,T> List<R> transform(List<T> c) {
    List<R> points = new ArrayList<>();
    if (c != null && !c.isEmpty()) {
      for (T t : c) {
        points.add(transform(t));
      }
    }
    return points;
  }

  private <R,T> R transform(T t) {
    if(t instanceof ClientesEntity){
      return (R) transform((ClientesEntity) t);
    }else if(t instanceof Cliente){
      return (R) transform((Cliente) t);
    }
    return null;
  }

  public List<Pedido> transformPedidoToPresenter(List<PedidoEntity> listIn){
    List<Pedido> listOut= new ArrayList<>();
    for(PedidoEntity pedido : listIn){
      listOut.add(transformPedido(pedido));
    }
    return listOut;
  }

  public Pedido transformPedido(PedidoEntity itemIn) {
    Pedido itemOut = new Pedido();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setCodigo_item(itemIn.getCodigo_item());
      itemOut.setCantidad(itemIn.getCantidad());
      itemOut.setCliente_codigo(itemIn.getCliente_codigo());
    }
    return itemOut;
  }
  public PedidoEntity transformPedidoToData(Pedido itemIn) {
    PedidoEntity itemOut = new PedidoEntity();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setCodigo_item(itemIn.getCodigo_item());
      itemOut.setCantidad(itemIn.getCantidad());
      itemOut.setCliente_codigo(itemIn.getCliente_codigo());
    }
    return itemOut;
  }
}
