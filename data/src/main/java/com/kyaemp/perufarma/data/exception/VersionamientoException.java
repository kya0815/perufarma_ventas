package com.kyaemp.perufarma.data.exception;


public class VersionamientoException extends Exception {

    public VersionamientoException() {
        super();
    }

    public VersionamientoException(final String message) {
        super(message);
    }

    public VersionamientoException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public VersionamientoException(final Throwable cause) {
        super(cause);
    }
}