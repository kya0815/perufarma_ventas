package com.kyaemp.perufarma.data.net.netmodel;

import java.io.Serializable;


public class PointNetModel implements Serializable {


    private int id;
    private Double lon;
    private Double lat;
    private String name;

    public PointNetModel(int id, Double lon, Double lat, String name) {
        this.id = id;
        this.lon = lon;
        this.lat = lat;
        this.name = name;
    }
}
