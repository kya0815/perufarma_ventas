package com.kyaemp.perufarma.data.entity.mapper;

import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;
import com.kyaemp.perufarma.domain.model.Position;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link RespuestaServerEntity} (in the data layer) to {@link RespuestaServer} in the
 * domain layer.
 */
@Singleton
public class PositionEntityDataMapper {

  @Inject
  PositionEntityDataMapper() {}

  public Position transform(PositionEntity itemIn) {
    Position itemOut = new Position();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setDeviceId(itemIn.getDeviceId());
      itemOut.setTime(itemIn.getTime());
      itemOut.setLatitude(itemIn.getLatitude());
      itemOut.setLongitude(itemIn.getLongitude());
      itemOut.setAltitude(itemIn.getAltitude());
      itemOut.setSpeed(itemIn.getSpeed());
      itemOut.setCourse(itemIn.getCourse());
      itemOut.setBattery(itemIn.getBattery());
      itemOut.setNombre(itemIn.getNombre());
      itemOut.setGuardar(itemIn.getGuardar());
    }
    return itemOut;
  }

  public PositionEntity transform(Position itemIn) {
    PositionEntity itemOut = new PositionEntity();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setDeviceId(itemIn.getDeviceId());
      itemOut.setTime(itemIn.getTime());
      itemOut.setLatitude(itemIn.getLatitude());
      itemOut.setLongitude(itemIn.getLongitude());
      itemOut.setAltitude(itemIn.getAltitude());
      itemOut.setSpeed(itemIn.getSpeed());
      itemOut.setCourse(itemIn.getCourse());
      itemOut.setBattery(itemIn.getBattery());
      itemOut.setNombre(itemIn.getNombre());
      itemOut.setGuardar(itemIn.getGuardar());
    }
    return itemOut;
  }

  public <R,T> List<R> transform(List<T> c) {
    List<R> points = new ArrayList<>();
    if (c != null && !c.isEmpty()) {
      for (T t : c) {
        points.add(transform(t));
      }
    }
    return points;
  }

  private <R,T> R transform(T t) {
    if(t instanceof PositionEntity){
      return (R) transform((PositionEntity) t);
    }else if(t instanceof Position){
      return (R) transform((Position) t);
    }
    return null;
  }
}
