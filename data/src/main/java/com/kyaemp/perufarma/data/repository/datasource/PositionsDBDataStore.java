package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PositionsEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.utiles.Constantes;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
@Singleton
public class PositionsDBDataStore implements PositionsDataStore {

    private PositionsEntityRepository repository;

    @Inject
    public PositionsDBDataStore(PositionsEntityRepository repository) {
        this.repository=repository;
    }



    @Override
    public Observable<Boolean> save(PositionEntity positionEntity) {
        return Observable.create(subscriber -> {
            try {
                if(repository.save(positionEntity)==1){
                    subscriber.onNext(true);
                }else{
                    subscriber.onNext(false);
                }
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_GUARDAR_POSICION));
            }
        });
    }

    @Override
    public Observable<Boolean> delete(int id) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(repository.deleteById(id)==1);
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_BORRAR_POSICION));
            }
        });
    }

    @Override
    public Observable<PositionEntity> getLastPosition() {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(repository.lastItem());
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_BORRAR_POSICION));
            }
        });
    }
}