package com.kyaemp.perufarma.data.exception;

import java.io.IOException;

public class UnauthorizedException extends IOException {

    public UnauthorizedException() {
    }

    public UnauthorizedException(final String message) {
        super(message);
    }

    public UnauthorizedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedException(final Throwable cause) {
        super(cause);
    }
}
