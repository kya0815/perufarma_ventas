package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.CatalogoEntityDataMapper;
import com.kyaemp.perufarma.data.entity.mapper.ClienteEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.CatalogoDataStore;
import com.kyaemp.perufarma.data.repository.datasource.CatalogoDataStoreFactory;
import com.kyaemp.perufarma.data.repository.datasource.ClientesDataStore;
import com.kyaemp.perufarma.data.repository.datasource.ClientesDataStoreFactory;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;
import com.kyaemp.perufarma.domain.repository.ClientesRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ClientesDataRepository implements ClientesRepository {

  private final ClientesDataStoreFactory dataStoreFactory;
  private final ClienteEntityDataMapper entityDataMapper;
  /**
   * Constructs a {@link ClientesDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  ClientesDataRepository(ClientesDataStoreFactory dataStoreFactory, ClienteEntityDataMapper mapper) {
    this.dataStoreFactory = dataStoreFactory;
    this.entityDataMapper = mapper;
  }


  @Override
  public Observable<List<Cliente>> getClientes() {
    final ClientesDataStore dataStore = this.dataStoreFactory.createCloudDataStore();
    return dataStore.getClientes().map(this.entityDataMapper::transform);
  }

  @Override
  public Observable<Cliente> getDetalleCliente(int idCliente) {
    final ClientesDataStore dataStore = this.dataStoreFactory.createDB();
    return dataStore.getDetalleCliente(idCliente).map(this.entityDataMapper::transform);
  }
}
