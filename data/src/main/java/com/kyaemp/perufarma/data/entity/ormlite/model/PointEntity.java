package com.kyaemp.perufarma.data.entity.ormlite.model;



import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;


public class PointEntity extends BaseModel implements Serializable {

    public static final String LON = "lon";
    public static final String LAT = "lat";
    public static final String NAME = "name";
    public static final String ENVIADO = "enviado";

    @DatabaseField(columnName = LON)
    private Double lon;
    @DatabaseField(columnName = LAT)
    private Double lat;
    @DatabaseField(columnName = NAME)
    private String name;
    @DatabaseField(columnName = ENVIADO)
    private boolean enviado;

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getName() {
        return name;
    }

    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        return "name: "+name+", lat:"+lat+", lon:"+lon;
    }
}
