package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.RelevamientoEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.RelevamientoDataStore;
import com.kyaemp.perufarma.data.repository.datasource.RelevamientoDataStoreFactory;
import com.kyaemp.perufarma.domain.model.Relevamiento;
import com.kyaemp.perufarma.domain.repository.RelevamientoRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class RelevamientoDataRepository implements RelevamientoRepository {

  private final RelevamientoDataStoreFactory dataStoreFactory;
  private final RelevamientoEntityDataMapper entityDataMapper;
  /**
   * Constructs a {@link RelevamientoDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  RelevamientoDataRepository(RelevamientoDataStoreFactory dataStoreFactory, RelevamientoEntityDataMapper mapper) {
    this.dataStoreFactory = dataStoreFactory;
    this.entityDataMapper = mapper;
  }

  @Override
  public Observable<List<Relevamiento>> obtenerRelevamientos(int id) {
    final RelevamientoDataStore relevamientoDataStore = this.dataStoreFactory.create();
    return relevamientoDataStore.obtenerRelevamientos(id).map(this.entityDataMapper::transform);
  }
}
