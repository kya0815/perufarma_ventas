package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;
import com.kyaemp.perufarma.data.net.RestApi;
import com.kyaemp.perufarma.data.net.RestApiImpl;

import java.util.List;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
public class PointsCloudDataStore implements PointsDataStore {

    private final RestApiImpl restApi;

    /**
     * Construct a {@link LoginDataStore} based on connections to the api (Cloud).
     *
     * @param loginRestApi The {@link RestApi} implementation to use.
     */
    public PointsCloudDataStore(RestApiImpl loginRestApi) {
        this.restApi = loginRestApi;
    }


    @Override
    public Observable<List<PointEntity>> obtenerPoints() {
        return null;
    }

    @Override
    public Observable<Boolean> guardarPoints(List<PointEntity> points) {
        return null;
    }

    @Override
    public Observable<Boolean> savePoint(int id) {
        return this.restApi.savePoint(id);
    }
}