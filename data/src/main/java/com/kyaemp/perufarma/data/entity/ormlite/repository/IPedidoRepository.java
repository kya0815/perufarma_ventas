package com.kyaemp.perufarma.data.entity.ormlite.repository;

import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PedidoEntity;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public interface IPedidoRepository extends IRepository<PedidoEntity,Integer> {

    public boolean isEmpty() throws SQLException;
    public List<PedidoEntity> getByIdCliente(int idCliente) throws SQLException;
}
