package com.kyaemp.perufarma.data.repository.datasource;


import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.net.RestApiImpl;
import com.kyaemp.perufarma.domain.model.UserSignUp;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
public class SignUpCloudDataStore implements SignUpDataStore {

    private final RestApiImpl restApi;

    /**
     * Construct a {@link LoginDataStore} based on connections to the api (Cloud).
     *
     */
    public SignUpCloudDataStore(RestApiImpl restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<RespuestaServerEntity> singup(UserSignUp userSignUp) {
        return this.restApi.signup(userSignUp);
    }
}