/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.RespuestaServerEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.LoginDataStore;
import com.kyaemp.perufarma.data.repository.datasource.LoginDataStoreFactory;
import com.kyaemp.perufarma.data.repository.datasource.SignUpDataStore;
import com.kyaemp.perufarma.data.repository.datasource.SignUpDataStoreFactory;
import com.kyaemp.perufarma.domain.model.RespuestaServer;
import com.kyaemp.perufarma.domain.model.UserLogin;
import com.kyaemp.perufarma.domain.model.UserSignUp;
import com.kyaemp.perufarma.domain.repository.LoginRepository;
import com.kyaemp.perufarma.domain.repository.SignUpRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;


/**
 * {@link LoginRepository} for retrieving user data.
 */
@Singleton
public class SignUpDataRepository implements SignUpRepository {

  private final SignUpDataStoreFactory dataStoreFactory;
  private final RespuestaServerEntityDataMapper mapper;
  /**
   * Constructs a {@link SignUpDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  SignUpDataRepository(SignUpDataStoreFactory dataStoreFactory, RespuestaServerEntityDataMapper mapper) {
    this.dataStoreFactory = dataStoreFactory;
    this.mapper = mapper;
  }

  @Override
  public Observable<RespuestaServer> singup(UserSignUp userSignUp) {
    final SignUpDataStore loginDataStore = this.dataStoreFactory.createCloudDataStore();
    return loginDataStore.singup(userSignUp).map(mapper::transform);
  }
}
