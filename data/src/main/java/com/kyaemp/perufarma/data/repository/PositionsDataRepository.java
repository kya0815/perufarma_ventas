package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.PointEntityDataMapper;
import com.kyaemp.perufarma.data.entity.mapper.PositionEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.PointsDataStore;
import com.kyaemp.perufarma.data.repository.datasource.PointsDataStoreFactory;
import com.kyaemp.perufarma.data.repository.datasource.PositionsDataStore;
import com.kyaemp.perufarma.data.repository.datasource.PositionsDataStoreFactory;
import com.kyaemp.perufarma.domain.model.Point;
import com.kyaemp.perufarma.domain.model.Position;
import com.kyaemp.perufarma.domain.repository.PointsRepository;
import com.kyaemp.perufarma.domain.repository.PositionsRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class PositionsDataRepository implements PositionsRepository {

  private final PositionsDataStoreFactory dataStoreFactory;
  private final PositionEntityDataMapper mapper;
  /**
   * Constructs a {@link PositionsDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  PositionsDataRepository(PositionsDataStoreFactory dataStoreFactory, PositionEntityDataMapper mapper) {
    this.dataStoreFactory = dataStoreFactory;
    this.mapper = mapper;
  }

  @Override
  public Observable<Boolean> saveDB(Position position) {
    final PositionsDataStore dataStore = this.dataStoreFactory.createDB();
    return dataStore.save(mapper.transform(position));
  }

  @Override
  public Observable<Boolean> saveServer(Position position) {
    final PositionsDataStore dataStore = this.dataStoreFactory.createCloudDataStore();
    return dataStore.save(mapper.transform(position));
  }

  @Override
  public Observable<Boolean> deleteDB(int id) {
    final PositionsDataStore dataStore = this.dataStoreFactory.createDB();
    return dataStore.delete(id);
  }

  @Override
  public Observable<Position> getLastPositionDB() {
    final PositionsDataStore dataStore = this.dataStoreFactory.createDB();
    return dataStore.getLastPosition().map(this.mapper::transform);
  }
}
