package com.kyaemp.perufarma.data.exception;

import java.io.IOException;


public class ErrorCodeReturnedException extends IOException {

    public static final int NO_ERROR_CODE = -1;

    private int httpStatusCode;
    private String errorCode;
    private String serverMessage;

    public ErrorCodeReturnedException(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public ErrorCodeReturnedException(int httpStatusCode, String errorCode, String serverMessage) {
        this(httpStatusCode);
        this.errorCode = errorCode;
        this.serverMessage = serverMessage;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getServerMessage() {
        return serverMessage;
    }

}
