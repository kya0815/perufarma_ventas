package com.kyaemp.perufarma.data.exception;

import android.util.Log;

import org.json.JSONObject;

public class ErrorCodeReturnedExceptionExtractor {

    public ErrorCodeReturnedException parse(int statusCode, String json) {
        try {
            Log.e("json response", "eror: " + json);
            JSONObject obj = new JSONObject(json);

            if (obj.has("error")) {
                String error = obj.getString("error");
                String message = obj.getString("error_description");
                return new ErrorCodeReturnedException(statusCode, error, message);
            } else if (obj.has("resultado")) {
                JSONObject jsonResultado = obj.getJSONObject("resultado");
                String message;
                String error;
                if (jsonResultado.has("message")) {
                    error = "";
                    message = jsonResultado.getString("message");
                } else {
                    error = jsonResultado.getString("error");
                    message = jsonResultado.getString("error_description");
                }
                return new ErrorCodeReturnedException(statusCode, error, message);
            } else {
                String error = obj.getString("error");
                String message = obj.getString("error_description");
                return new ErrorCodeReturnedException(statusCode, error, message);
            }

        } catch (Exception e) {
            return new ErrorCodeReturnedException(statusCode, "", "El app no puede procesar su solicitud.");
        }
    }
}
