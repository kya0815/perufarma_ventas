package com.kyaemp.perufarma.data.entity.ormlite.repository;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.BaseDBRepository;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PedidoEntity;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

@Singleton
public class PedidoEntityRepository extends BaseDBRepository<PedidoEntity,Integer> implements IPedidoRepository {

    @Inject
    public PedidoEntityRepository(Context context) {
        super(context);
    }

    @Override
    public boolean isEmpty() throws SQLException {
        return getDao().countOf()==0;
    }

    @Override
    public List<PedidoEntity> getByIdCliente(int idCliente) throws SQLException {
        return getDao().queryBuilder().where().eq(PedidoEntity.CLIENTE_CODIGO,idCliente).query();
    }

}
