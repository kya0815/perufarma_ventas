package com.kyaemp.perufarma.data.repository.datasource;


import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.domain.model.UserLogin;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface LoginDataStore {

  Observable<RespuestaServerEntity> login(UserLogin userLogin);
}
