package com.kyaemp.perufarma.data.entity.ormlite.repository;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.BaseDBRepository;
import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public interface IPointsRepository extends IRepository<PointEntity,Integer> {


}
