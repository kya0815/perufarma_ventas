package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface PointsDataStore {

  Observable<List<PointEntity>> obtenerPoints();
  Observable<Boolean> guardarPoints(List<PointEntity> points);
  Observable<Boolean> savePoint(int id);
}
