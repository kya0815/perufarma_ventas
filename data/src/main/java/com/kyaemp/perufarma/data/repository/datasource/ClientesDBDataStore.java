package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.ClientesEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.utiles.Constantes;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
@Singleton
public class ClientesDBDataStore implements ClientesDataStore {

    private ClientesEntityRepository entityRepository;

    @Inject
    public ClientesDBDataStore(ClientesEntityRepository entityRepository) {
        this.entityRepository=entityRepository;
    }

    @Override
    public Observable<List<ClientesEntity>> getClientes() {
        return Observable.create(subscriber -> {
            try {
                if(!entityRepository.isEmpty()){
                    subscriber.onNext(entityRepository.queryAll());
                }else{
                    subscriber.onNext(Collections.emptyList());
                }
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_CATALOGO_LOCAL));
            }
        });
    }

    @Override
    public Observable<ClientesEntity> getDetalleCliente(int idCliente) {
        return Observable.create(subscriber -> {
            try {
                if(!entityRepository.isEmpty()){
                    subscriber.onNext(entityRepository.getById(idCliente));
                }else{
                    subscriber.onNext(null);
                }
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_CATALOGO_LOCAL));
            }
        });
    }
}