package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.ClientesEntityRepository;
import com.kyaemp.perufarma.data.net.RestApiImplCatalogo;
import com.kyaemp.perufarma.data.net.RestApiImplClientes;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ClientesDataStoreFactory {

  private final Context context;
  @Inject
  public ClientesEntityRepository entityRepository;

  @Inject
  public ClientesDataStoreFactory(Context context) {
    this.context = context;
  }

  public ClientesDataStore createCloudDataStore() {
    RestApiImplClientes restApi = new RestApiImplClientes(context,entityRepository);

    return new ClientesCloudDataStore(restApi);
  }

  public ClientesDataStore createDB() {

    return new ClientesDBDataStore(entityRepository);
  }

}
