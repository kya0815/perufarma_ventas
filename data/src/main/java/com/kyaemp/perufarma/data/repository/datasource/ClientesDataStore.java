package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface ClientesDataStore {

  Observable<List<ClientesEntity>> getClientes();

  Observable<ClientesEntity> getDetalleCliente(int idCliente);
}
