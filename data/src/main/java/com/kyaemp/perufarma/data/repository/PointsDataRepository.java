package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.PointEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.PointsDataStore;
import com.kyaemp.perufarma.data.repository.datasource.PointsDataStoreFactory;
import com.kyaemp.perufarma.domain.model.Point;
import com.kyaemp.perufarma.domain.repository.PointsRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class PointsDataRepository implements PointsRepository {

  private final PointsDataStoreFactory pointsDataStoreFactory;
  private final PointEntityDataMapper pointEntityDataMapper;
  /**
   * Constructs a {@link PointsDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  PointsDataRepository(PointsDataStoreFactory dataStoreFactory, PointEntityDataMapper mapper) {
    this.pointsDataStoreFactory = dataStoreFactory;
    this.pointEntityDataMapper = mapper;
  }

  @Override
  public Observable<List<Point>> obtenerPoints() {
    final PointsDataStore pointsDataStore = this.pointsDataStoreFactory.create();
    return pointsDataStore.obtenerPoints().map(pointEntities -> this.pointEntityDataMapper.transform(pointEntities));
  }

  @Override
  public Observable<Boolean> guardarPoints(List<Point> points) {
    final PointsDataStore pointsDataStore = this.pointsDataStoreFactory.create();
    return pointsDataStore.guardarPoints(this.pointEntityDataMapper.transform(points));
  }

  @Override
  public Observable<Boolean> savePoint(int id) {
    final PointsDataStore pointsDataStore = this.pointsDataStoreFactory.createCloudDataStore();
    return pointsDataStore.savePoint(id);
  }

}
