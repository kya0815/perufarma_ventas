package com.kyaemp.perufarma.data.services;



public interface OnSocketConnectionListener {
    void onSocketEventFailed();

    void onSocketConnectionStateChange(int socketState);
    void onSocketSendCorreoPrimerChat();
    void onInternetConnectionStateChange(int socketState);
}
