package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PedidoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.ClientesEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PedidoEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.utiles.Constantes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
@Singleton
public class PedidosDBDataStore implements PedidosDataStore {

    private PedidoEntityRepository entityRepository;

    @Inject
    public PedidosDBDataStore(PedidoEntityRepository entityRepository) {
        this.entityRepository=entityRepository;
    }

    @Override
    public Observable<List<PedidoEntity>> getPedidosIdCliente(int idCliente) {
        return Observable.create(subscriber -> {
            try {
                List<PedidoEntity> result= new ArrayList<>();
                if(!entityRepository.isEmpty()){
                    result=entityRepository.getByIdCliente(idCliente);
                }
                subscriber.onNext(result);
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_CATALOGO_LOCAL));
            }
        });
    }

    @Override
    public Observable<Boolean> savePedidoIdCliente(PedidoEntity pedido) {
        return Observable.create(subscriber -> {
            try {
                entityRepository.save(pedido);
                subscriber.onNext(true);
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_CATALOGO_LOCAL));
            }
        });
    }
}