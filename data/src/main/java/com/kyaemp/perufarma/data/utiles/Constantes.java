package com.kyaemp.perufarma.data.utiles;

import android.content.Context;
import android.provider.Settings;

import com.kyaemp.perufarma.data.BuildConfig;

/**
 * Created by pedro.zevallos on 14/08/2017.
 */

public class Constantes {
    public static final String ERROR_CONEXION_INTERNET = "No se cuenta con acceso a internet";
    public static final String ERROR_RESPUESTA_SERVER = "Error en el Servidor";
    public static final String ERROR_LOGEO_INCORRECTO = "Usuario y/o Contaseña Erroneo";
    public static final String ERROR_SIGNUP_INCORRECTO = "Registro Fallido";
    public static final String ERROR_LOGEO_PROCESAMIENTO = "Error respuesta Server";
    public static final String ERROR_PROCESAMIENTO_APP = "Error al procesar los datos";
    public static final String ERROR_HILO_ESPERA = "Error en el Hilo de Espera";
    public static final String ERROR_ACTUALIZANDO_DATOS = "Error al Actualizar datos internamente";
    public static final String ERROR_AL_OBTENER_DATOS = "Error al obtener datos de Relevamientos";
    public static final String ERROR_NO_DETERMINADO = "Se ha producido un error de aplicación";
    public static final String ERROR_OBTENER_DATA_LOCAL = "Error al obtener puntos de Interes";
    public static final String ERROR_OBTENER_RELEVAMIENTOS_LOCAL = "Error al obtener Relevamientos localmente";
    public static final String ERROR_OBTENER_CATALOGO_LOCAL = "Error al obtener Catalogo localmente";
    public static final String ERROR_GUARDAR_DATA_LOCAL = "Error al guardar puntos de Interes";
    public static final String ERROR_GUARDAR_POSICION = "Error al guardar posicion";
    public static final String ERROR_BORRAR_POSICION = "Error al borrar posicion";
    public static final String URL_HOST="http://www.json-generator.com/";
    public static final String URL_NODEJS="http://52.22.169.65:90";
    public static final String PACKAGE_NAME = BuildConfig.APPLICATION_ID;
    public static final String PREFERENCES = PACKAGE_NAME + ".pref";
    public static final String PREF_LOGGIN = "loggin";
    public static final String PREF_USER= "user";
    public static final String PREF_NAME= "name";
    public static final String PREF_KEY_GUARDAR_GPS = "tiempoGuardar";
    public static final String SOCKET_LOGIN="loggin";
    public static final String SOCKET_SIGNUP="registro";
    public static final String SOCKET_GET_TIEMPO_GUARDADO = "getTiempoGuardado";
    public static final String SOCKET_SEND_GPS = "send GPS";
    public static final String SOCKET_NEW_DEVICE = "new device";
    public static final String SOCKET_ESTADO = "estado";
    public static final String ERROR_PROCESAR_DATOS_SOCKET = "error de respuesta del server";
    public static final String ERROR_NO_GUARDO_POSICION = "no guardo posicion GPS";
    public static final String ERROR_CONVERTIR_BYTES = "Error al convertir a bytes";
    public static final String ERROR_CONVERTIR_JSON = "Error al convertir a json";

    public static String idDevice(Context con){
        return Settings.Secure.getString(con.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
