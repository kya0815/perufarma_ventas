package com.kyaemp.perufarma.data.entity.ormlite.repository;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by gilson.maciel on 27/04/2015.
 */
public interface IRepository<T, Id> {
    public int save(T entity) throws SQLException;
    public int saveBatch(List<T> entities) throws Exception;
    public List<T> queryAll() throws SQLException;
    public T findById(Id id) throws SQLException;
    public void delete(T entity) throws SQLException;
    public int deleteById(Id id) throws SQLException;
    public boolean update(T entity) throws SQLException;
    public void clearSafeMode() throws SQLException;

    public void safedelete(T entity) throws SQLException;
}
