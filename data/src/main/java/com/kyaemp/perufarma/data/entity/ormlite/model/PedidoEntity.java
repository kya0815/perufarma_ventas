package com.kyaemp.perufarma.data.entity.ormlite.model;



import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class PedidoEntity implements Serializable {

    public static final String ID = "id";
    public static final String CODIGO_ITEM = "codigo_item";
    public static final String CANTIDAD = "cantidad";
    public static final String CLIENTE_CODIGO = "cliente_codigo";

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true,canBeNull = false)
    private int id;
    @DatabaseField(columnName = CODIGO_ITEM)
    private long codigo_item;
    @DatabaseField(columnName = CANTIDAD)
    private int cantidad;
    @DatabaseField(columnName = CLIENTE_CODIGO)
    private int cliente_codigo;


    public PedidoEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCodigo_item() {
        return codigo_item;
    }

    public void setCodigo_item(long codigo_item) {
        this.codigo_item = codigo_item;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCliente_codigo() {
        return cliente_codigo;
    }

    public void setCliente_codigo(int cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
    }
}
