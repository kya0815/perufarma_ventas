package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.RelevamientoEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.utiles.Constantes;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
@Singleton
public class RelevamientoDBDataStore implements RelevamientoDataStore {

    private RelevamientoEntityRepository relevamientoEntityRepository;

    @Inject
    public RelevamientoDBDataStore(RelevamientoEntityRepository relevamientoEntityRepository) {
        this.relevamientoEntityRepository=relevamientoEntityRepository;
    }

    @Override
    public Observable<List<RelevamientoEntity>> obtenerRelevamientos(int id) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(relevamientoEntityRepository.queryAll());
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_RELEVAMIENTOS_LOCAL));
            }
        });
    }
}