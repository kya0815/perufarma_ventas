package com.kyaemp.perufarma.data.entity.ormlite.model;



import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class ClientesEntity implements Serializable {

    public static final String ID = "id";
    public static final String CLIENTE_NOMBRE = "cliente_nombre";
    public static final String CLIENTE_RUC = "cliente_ruc";
    public static final String CLIENTE_CODIGO = "cliente_codigo";
    public static final String ULTIMO_PEDIDO = "ultimo_pedido";
    public static final String MONTO_VENCIDO = "monto_vencido";
    public static final String TELEFONO = "telefono";
    public static final String DIRECCION = "direccion";
    public static final String LATITUD = "latitud";
    public static final String LONGITUD = "longitud";
    public static final String DEPARTAMEN = "departamen";
    public static final String PROVINCIA = "provincia";
    public static final String DISTRITO = "distrito";
    public static final String CORREO = "correo";

    @DatabaseField(columnName = ID,id = true,canBeNull = false)
    private int id;
    @DatabaseField(columnName = CLIENTE_NOMBRE)
    private String cliente_nombre;
    @DatabaseField(columnName = CLIENTE_RUC)
    private Long cliente_ruc;
    @DatabaseField(columnName = CLIENTE_CODIGO)
    private int cliente_codigo;
    @DatabaseField(columnName = ULTIMO_PEDIDO)
    private String ultimo_pedido;
    @DatabaseField(columnName = MONTO_VENCIDO)
    private int monto_vencido;
    @DatabaseField(columnName = TELEFONO)
    private String telefono;
    @DatabaseField(columnName = DIRECCION)
    private String direccion;
    @DatabaseField(columnName = DEPARTAMEN)
    private String departamen;
    @DatabaseField(columnName = PROVINCIA)
    private String provincia;
    @DatabaseField(columnName = DISTRITO)
    private String distrito;
    @DatabaseField(columnName = CORREO)
    private String correo;
    @DatabaseField(columnName = LATITUD)
    private double latitud;
    @DatabaseField(columnName = LONGITUD)
    private double longitud;


    public ClientesEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCliente_nombre() {
        return cliente_nombre;
    }

    public void setCliente_nombre(String cliente_nombre) {
        this.cliente_nombre = cliente_nombre;
    }

    public Long getCliente_ruc() {
        return cliente_ruc;
    }

    public void setCliente_ruc(Long cliente_ruc) {
        this.cliente_ruc = cliente_ruc;
    }

    public int getCliente_codigo() {
        return cliente_codigo;
    }

    public void setCliente_codigo(int cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
    }

    public String getUltimo_pedido() {
        return ultimo_pedido;
    }

    public void setUltimo_pedido(String ultimo_pedido) {
        this.ultimo_pedido = ultimo_pedido;
    }

    public int getMonto_vencido() {
        return monto_vencido;
    }

    public void setMonto_vencido(int monto_vencido) {
        this.monto_vencido = monto_vencido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getDepartamen() {
        return departamen;
    }

    public void setDepartamen(String departamen) {
        this.departamen = departamen;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
