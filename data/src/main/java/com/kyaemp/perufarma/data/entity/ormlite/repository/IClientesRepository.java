package com.kyaemp.perufarma.data.entity.ormlite.repository;

import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;

import java.sql.SQLException;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public interface IClientesRepository extends IRepository<ClientesEntity,Integer> {

    public boolean isEmpty() throws SQLException;
    public ClientesEntity getById(int id) throws SQLException;
}
