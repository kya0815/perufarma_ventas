package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.net.RestApi;
import com.kyaemp.perufarma.data.net.RestApiImplCatalogo;
import com.kyaemp.perufarma.data.net.RestApiImplClientes;

import java.util.List;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
public class ClientesCloudDataStore implements ClientesDataStore {

    private final RestApiImplClientes restApi;

    /**
     * Construct a {@link LoginDataStore} based on connections to the api (Cloud).
     *
     * @param restApi The {@link RestApi} implementation to use.
     */
    public ClientesCloudDataStore(RestApiImplClientes restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<List<ClientesEntity>> getClientes() {
        return restApi.getClientes();
    }

    @Override
    public Observable<ClientesEntity> getDetalleCliente(int idCliente) {
        return null;
    }
}