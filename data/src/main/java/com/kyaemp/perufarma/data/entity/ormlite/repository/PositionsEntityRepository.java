package com.kyaemp.perufarma.data.entity.ormlite.repository;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.BaseDBRepository;
import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;

import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

@Singleton
public class PositionsEntityRepository extends BaseDBRepository<PositionEntity, Integer> implements IPositionsRepository {

    @Inject
    public PositionsEntityRepository(Context context) {
        super(context);
    }

    @Override
    public PositionEntity lastItem() throws SQLException {
        return getDao().queryBuilder().orderBy(PositionEntity.ID,false).queryForFirst();
    }
}
