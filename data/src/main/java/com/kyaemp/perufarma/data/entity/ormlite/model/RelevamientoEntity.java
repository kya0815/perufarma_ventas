package com.kyaemp.perufarma.data.entity.ormlite.model;



import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class RelevamientoEntity implements Serializable {

    public static final String _ID = "id";
    public static final String EMPRESA = "empresa";
    public static final String TELEFONO = "telefono";
    public static final String DIRECCION = "direccion";
    public static final String LATITUD = "latitud";
    public static final String LONGITUD = "longitud";
    public static final String TERMINADO = "terminado";

    @DatabaseField(columnName = _ID,id = true,canBeNull = false)
    private long id;
    @DatabaseField(columnName = EMPRESA)
    private String empresa;
    @DatabaseField(columnName = TELEFONO)
    private String telefono;
    @DatabaseField(columnName = DIRECCION)
    private String direccion;
    @DatabaseField(columnName = LATITUD)
    private double latitud;
    @DatabaseField(columnName = LONGITUD)
    private double longitud;
    @DatabaseField(columnName = TERMINADO)
    private boolean terminado;

    public RelevamientoEntity() {
    }

    public boolean isTerminado() {
        return terminado;
    }

    public void setTerminado(boolean terminado) {
        this.terminado = terminado;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }
}
