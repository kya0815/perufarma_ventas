package com.kyaemp.perufarma.data.entity.mapper;

import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;
import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.domain.model.Point;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link RespuestaServerEntity} (in the data layer) to {@link RespuestaServer} in the
 * domain layer.
 */
@Singleton
public class PointEntityDataMapper {

  @Inject
  PointEntityDataMapper() {}

  public Point transform(PointEntity pointEntity) {
    Point point = new Point();
    if (pointEntity != null) {
      point.setId(pointEntity.getId());
      point.setLat(pointEntity.getLat());
      point.setLon(pointEntity.getLon());
      point.setName(pointEntity.getName());
      point.setEnviado(pointEntity.isEnviado());
    }
    return point;
  }

  public PointEntity transform(Point point) {
    PointEntity pointEntity = new PointEntity();
    if (point != null) {
      pointEntity.setId(point.getId());
      pointEntity.setLat(point.getLat());
      pointEntity.setLon(point.getLon());
      pointEntity.setName(point.getName());
      pointEntity.setEnviado(point.isEnviado());
    }
    return pointEntity;
  }

  public <R,T> List<R> transform(List<T> c) {
    List<R> points = new ArrayList<>();
    if (c != null && !c.isEmpty()) {
      for (T t : c) {
        points.add(transform(t));
      }
    }
    return points;
  }

  private <R,T> R transform(T t) {
    if(t instanceof PointEntity){
      return (R) transform((PointEntity) t);
    }else if(t instanceof Point){
      return (R) transform((Point) t);
    }
    return null;
  }
//  public List<Point> transform(List<PointEntity> pointsEntity) {
//    List<Point> points = new ArrayList<>();
//    if (pointsEntity != null && !pointsEntity.isEmpty()) {
//      for (PointEntity point : pointsEntity) {
//        points.add(transform(point));
//      }
//    }
//    return points;
//  }
//
//  public List<PointEntity> transform(List<Point> points) {
//    List<PointEntity> pointsEntity = new ArrayList<>();
//    if (points != null && !points.isEmpty()) {
//      for (Point point : points) {
//        pointsEntity.add(transform(point));
//      }
//    }
//    return pointsEntity;
//  }
}
