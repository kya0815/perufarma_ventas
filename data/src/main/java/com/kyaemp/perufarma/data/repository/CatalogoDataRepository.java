package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.CatalogoEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.CatalogoDataStore;
import com.kyaemp.perufarma.data.repository.datasource.CatalogoDataStoreFactory;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class CatalogoDataRepository implements CatalogoRepository {

  private final CatalogoDataStoreFactory dataStoreFactory;
  private final CatalogoEntityDataMapper entityDataMapper;
  /**
   * Constructs a {@link CatalogoDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  CatalogoDataRepository(CatalogoDataStoreFactory dataStoreFactory, CatalogoEntityDataMapper mapper) {
    this.dataStoreFactory = dataStoreFactory;
    this.entityDataMapper = mapper;
  }

  @Override
  public Observable<Boolean> getCatalogoOnline() {
    final CatalogoDataStore relevamientoDataStore = this.dataStoreFactory.createCloudDataStore();
    return relevamientoDataStore.getCatalogoOnline();
  }

  @Override
  public Observable<List<Catalogo>> getCatalogo() {
    final CatalogoDataStore relevamientoDataStore = this.dataStoreFactory.createDB();
    return relevamientoDataStore.getCatalogo().map(this.entityDataMapper::transform);
  }

  @Override
  public Observable<Catalogo> getCatalogoByID(long id) {
    final CatalogoDataStore relevamientoDataStore = this.dataStoreFactory.createDB();
    return relevamientoDataStore.getCatalogoByID(id).map(entityDataMapper::transform);
  }
}
