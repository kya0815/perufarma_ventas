package com.kyaemp.perufarma.data.entity.ormlite.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by gilson.maciel on 27/04/2015.
 */
public abstract class BaseModel {
    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true, canBeNull = false)
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
