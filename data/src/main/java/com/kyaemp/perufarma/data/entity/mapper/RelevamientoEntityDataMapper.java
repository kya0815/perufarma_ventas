package com.kyaemp.perufarma.data.entity.mapper;

import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;
import com.kyaemp.perufarma.domain.model.Relevamiento;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link RespuestaServerEntity} (in the data layer) to {@link RespuestaServer} in the
 * domain layer.
 */
@Singleton
public class RelevamientoEntityDataMapper {

  @Inject
  RelevamientoEntityDataMapper() {}

  public Relevamiento transform(RelevamientoEntity itemIn) {
    Relevamiento itemOut = new Relevamiento();
    if (itemIn != null) {
      itemOut.set_id(itemIn.getId());
      itemOut.setEmpresa(itemIn.getEmpresa());
      itemOut.setTelefono(itemIn.getTelefono());
      itemOut.setDireccion(itemIn.getDireccion());
      itemOut.setLatitud(itemIn.getLatitud());
      itemOut.setLongitud(itemIn.getLongitud());
      itemOut.setTerminado(itemIn.isTerminado());
    }
    return itemOut;
  }

  public RelevamientoEntity transform(Relevamiento itemIn) {
    RelevamientoEntity itemOut = new RelevamientoEntity();
    if (itemIn != null) {
      itemOut.setId(itemIn.get_id());
      itemOut.setEmpresa(itemIn.getEmpresa());
      itemOut.setTelefono(itemIn.getTelefono());
      itemOut.setDireccion(itemIn.getDireccion());
      itemOut.setLatitud(itemIn.getLatitud());
      itemOut.setLongitud(itemIn.getLongitud());
      itemOut.setTerminado(itemIn.isTerminado());
    }
    return itemOut;
  }

  public <R,T> List<R> transform(List<T> c) {
    List<R> points = new ArrayList<>();
    if (c != null && !c.isEmpty()) {
      for (T t : c) {
        points.add(transform(t));
      }
    }
    return points;
  }

  private <R,T> R transform(T t) {
    if(t instanceof RelevamientoEntity){
      return (R) transform((RelevamientoEntity) t);
    }else if(t instanceof Relevamiento){
      return (R) transform((Relevamiento) t);
    }
    return null;
  }
//  public List<RelevamientoEntity> transform(List<RelevamientoEntity> pointsEntity) {
//    List<RelevamientoEntity> points = new ArrayList<>();
//    if (pointsEntity != null && !pointsEntity.isEmpty()) {
//      for (RelevamientoEntity point : pointsEntity) {
//        points.add(transform(point));
//      }
//    }
//    return points;
//  }
//
//  public List<RelevamientoEntity> transform(List<RelevamientoEntity> points) {
//    List<RelevamientoEntity> pointsEntity = new ArrayList<>();
//    if (points != null && !points.isEmpty()) {
//      for (RelevamientoEntity point : points) {
//        pointsEntity.add(transform(point));
//      }
//    }
//    return pointsEntity;
//  }
}
