/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.RespuestaServerEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.LoginDataStore;
import com.kyaemp.perufarma.data.repository.datasource.LoginDataStoreFactory;
import com.kyaemp.perufarma.domain.model.RespuestaServer;
import com.kyaemp.perufarma.domain.model.UserLogin;
import com.kyaemp.perufarma.domain.repository.LoginRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;


/**
 * {@link LoginRepository} for retrieving user data.
 */
@Singleton
public class LoginDataRepository implements LoginRepository {

  private final LoginDataStoreFactory loginDataStoreFactory;
  private final RespuestaServerEntityDataMapper respuestaServerEntityDataMapper;
  /**
   * Constructs a {@link LoginDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  LoginDataRepository(LoginDataStoreFactory dataStoreFactory, RespuestaServerEntityDataMapper mapper) {
    this.loginDataStoreFactory = dataStoreFactory;
    this.respuestaServerEntityDataMapper = mapper;
  }

  @Override
  public Observable<RespuestaServer> login(UserLogin userLogin) {
    final LoginDataStore loginDataStore = this.loginDataStoreFactory.createCloudDataStore();
    return loginDataStore.login(userLogin).map(respuestaServerEntityDataMapper::transform);
  }
}
