package com.kyaemp.perufarma.data.net.netmodel;



import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;

import java.io.Serializable;
import java.util.List;

public class RespuestaServerNetClientesEntity implements Serializable {


    private int error;
    private String mensaje;
    private List<ClientesEntity> data;

    public RespuestaServerNetClientesEntity() {
    }

    public RespuestaServerNetClientesEntity(int status, String mensaje, List<ClientesEntity> data) {
        this.error = status;
        this.mensaje = mensaje;
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<ClientesEntity> getData() {
        return data;
    }

    public void setData(List<ClientesEntity> data) {
        this.data = data;
    }

    public String toString(){
        return "error: "+ error +", messages:"+ mensaje+", data:"+data;
    }
}
