package com.kyaemp.perufarma.data.repository;

import com.kyaemp.perufarma.data.entity.mapper.ClienteEntityDataMapper;
import com.kyaemp.perufarma.data.repository.datasource.ClientesDataStore;
import com.kyaemp.perufarma.data.repository.datasource.ClientesDataStoreFactory;
import com.kyaemp.perufarma.data.repository.datasource.PedidosDataStore;
import com.kyaemp.perufarma.data.repository.datasource.PedidosDataStoreFactory;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;
import com.kyaemp.perufarma.domain.repository.ClientesRepository;
import com.kyaemp.perufarma.domain.repository.PedidosRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class PedidosDataRepository implements PedidosRepository {

  private final PedidosDataStoreFactory dataStoreFactory;
  private final ClienteEntityDataMapper entityDataMapper;
  /**
   * Constructs a {@link PedidosDataRepository}.
   *
   * @param dataStoreFactory A factory to construct different data source implementations.
   */
  @Inject
  PedidosDataRepository(PedidosDataStoreFactory dataStoreFactory, ClienteEntityDataMapper mapper) {
    this.dataStoreFactory = dataStoreFactory;
    this.entityDataMapper = mapper;
  }

  @Override
  public Observable<List<Pedido>> getPedidosIdCliente(int idCliente) {
    final PedidosDataStore dataStore = this.dataStoreFactory.createDB();
    return dataStore.getPedidosIdCliente(idCliente).map(this.entityDataMapper::transformPedidoToPresenter);
  }

  @Override
  public Observable<Boolean> savePedidoIdCliente(Pedido pedido) {
    final PedidosDataStore dataStore = this.dataStoreFactory.createDB();
    return dataStore.savePedidoIdCliente(entityDataMapper.transformPedidoToData(pedido));
  }
}
