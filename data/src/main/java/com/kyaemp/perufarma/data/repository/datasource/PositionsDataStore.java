package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface PositionsDataStore {

  Observable<Boolean> save(PositionEntity positionEntity);
  Observable<Boolean> delete(int id);
  Observable<PositionEntity> getLastPosition();
}
