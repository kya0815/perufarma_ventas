package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;
import com.kyaemp.perufarma.data.net.RestApiImplPositions;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
public class PositionsCloudDataStore implements PositionsDataStore {

    private final RestApiImplPositions restApi;

    public PositionsCloudDataStore(RestApiImplPositions restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<Boolean> save(PositionEntity positionEntity) {
        return restApi.save(positionEntity);
    }

    @Override
    public Observable<Boolean> delete(int id) {
        return null;
    }

    @Override
    public Observable<PositionEntity> getLastPosition() {
        return null;
    }
}