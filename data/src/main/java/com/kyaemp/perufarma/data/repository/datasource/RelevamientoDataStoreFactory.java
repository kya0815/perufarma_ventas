package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.repository.RelevamientoEntityRepository;
import com.kyaemp.perufarma.data.net.RestApiImplRelevamiento;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link LoginDataStore}.
 */
@Singleton
public class RelevamientoDataStoreFactory {

  private final Context context;
  @Inject
  public RelevamientoEntityRepository relevamientoEntityRepository;
  @Inject
  public RelevamientoDataStoreFactory(Context context) {
    this.context = context;
  }

  public RelevamientoDataStore create() {
    return createCloudDataStore();
  }

  /**
   * Create {@link LoginDataStore} to retrieve data from the Cloud.
   */
  public RelevamientoDataStore createCloudDataStore() {
    RestApiImplRelevamiento restApi = new RestApiImplRelevamiento(context,relevamientoEntityRepository);

    return new RelevamientoCloudDataStore(restApi);
  }

}
