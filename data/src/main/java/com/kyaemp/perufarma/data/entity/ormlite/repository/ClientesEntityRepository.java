package com.kyaemp.perufarma.data.entity.ormlite.repository;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.BaseDBRepository;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;

import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

@Singleton
public class ClientesEntityRepository extends BaseDBRepository<ClientesEntity,Integer> implements IClientesRepository {

    @Inject
    public ClientesEntityRepository(Context context) {
        super(context);
    }

    @Override
    public boolean isEmpty() throws SQLException {
        return getDao().countOf()==0;
    }

    @Override
    public ClientesEntity getById(int id) throws SQLException {
        return getDao().queryForId(id);
    }


}
