package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;


import com.kyaemp.perufarma.data.net.RestApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link LoginDataStore}.
 */
@Singleton
public class LoginDataStoreFactory {

  private final Context context;

  @Inject
  public LoginDataStoreFactory(Context context) {
    this.context = context;
  }

  public LoginDataStore create() {
    return createCloudDataStore();
  }

  /**
   * Create {@link LoginDataStore} to retrieve data from the Cloud.
   */
  public LoginDataStore createCloudDataStore() {
    RestApiImpl loginRestApi = new RestApiImpl(context);

    return new LoginCloudDataStore(loginRestApi);
  }

}
