package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface CatalogoDataStore {

  Observable<Boolean> getCatalogoOnline();
  Observable<List<CatalogoEntity>> getCatalogo();
  Observable<CatalogoEntity> getCatalogoByID(long id);
}
