package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.repository.PointsEntityRepository;
import com.kyaemp.perufarma.data.net.RestApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link LoginDataStore}.
 */
@Singleton
public class PointsDataStoreFactory {

  private final Context context;
  @Inject
  public PointsEntityRepository pointsEntityRepository;

  @Inject
  public PointsDataStoreFactory(Context context) {
    this.context = context;
  }

  public PointsDataStore create() {
    PointsDataStore pointsDataStore=createDB();
    return pointsDataStore;
  }

  public PointsDataStore createCloudDataStore() {
    RestApiImpl restApi = new RestApiImpl(context);

    return new PointsCloudDataStore(restApi);
  }

  private PointsDataStore createDB() {

    return new PointsDBDataStore(pointsEntityRepository);
  }

}
