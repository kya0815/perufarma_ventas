package com.kyaemp.perufarma.data.net;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PositionsEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.exception.NetworkConnectionException;
import com.kyaemp.perufarma.data.services.SocketManager;
import com.kyaemp.perufarma.data.utiles.Constantes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.socket.client.Ack;

import static com.kyaemp.perufarma.data.utiles.Constantes.SOCKET_SEND_GPS;
import static com.kyaemp.perufarma.data.utiles.Constantes.URL_NODEJS;

@Singleton
public class RestApiImplPositions {

    private final Context context;

    PositionsEntityRepository repository;
    /**
     * Constructor of the class
     *
     * @param context {@link Context}.
     */
    @Inject
    public RestApiImplPositions(Context context, PositionsEntityRepository repository) {
        this.context = context.getApplicationContext();
        this.repository=repository;
    }

    public Observable<Boolean> save(PositionEntity positionEntity) {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    JSONObject request=null;
                    Gson gson = new Gson();
                    String jsonString = gson.toJson(positionEntity);
                    try {
                        request = new JSONObject(jsonString);
                    } catch (JSONException e) {
                        subscriber.onError(new ErrorException(Constantes.ERROR_CONVERTIR_JSON));
                    }
                    if(request!=null) {
                        String data = request.toString();
                        byte[] bytes = null;
                        try {
                            if (data != null) {
                                bytes = data.getBytes("UTF-8");
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            subscriber.onError(new ErrorException(Constantes.ERROR_CONVERTIR_BYTES));
                        }
                        SocketManager.getInstance().connectSocket(URL_NODEJS);
                        SocketManager.getInstance().getSocket().emit(SOCKET_SEND_GPS, bytes, (Ack) args -> {
                            try {
                                boolean result = (Boolean) args[0];
                                if (result) {
                                    repository.delete(positionEntity);
                                    subscriber.onNext(true);
                                    subscriber.onComplete();
                                }else{
                                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_GUARDO_POSICION));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                subscriber.onError(new ErrorException(Constantes.ERROR_PROCESAR_DATOS_SOCKET));
                            }
                        });
                    }
                } catch (Exception e) {
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            } else {
                subscriber.onError(new NetworkConnectionException());
            }
        });
    }


    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }


}