package com.kyaemp.perufarma.data.net;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.ClientesEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.exception.NetworkConnectionException;
import com.kyaemp.perufarma.data.net.netmodel.RespuestaServerNetClientesEntity;
import com.kyaemp.perufarma.data.net.netmodel.RespuestaServerNetEntity;
import com.kyaemp.perufarma.data.utiles.Constantes;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyaemp.perufarma.data.utiles.Constantes.URL_NODEJS;

@Singleton
public class RestApiImplClientes {

    private final Context context;

    private ClientesEntityRepository entityRepository;
    /**
     * Constructor of the class
     *
     * @param context {@link Context}.
     */
    @Inject
    public RestApiImplClientes(Context context, ClientesEntityRepository entityRepository) {
        this.context = context.getApplicationContext();
        this.entityRepository=entityRepository;
    }

    public Observable<List<ClientesEntity>> getClientes() {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    RestApi restApi = new BaseNet().create(URL_NODEJS,RestApi.class);
                    Call<RespuestaServerNetClientesEntity> call = restApi.getClientes();
                    call.enqueue(new Callback<RespuestaServerNetClientesEntity>() {
                        @Override
                        public void onResponse(Call<RespuestaServerNetClientesEntity> call, Response<RespuestaServerNetClientesEntity> response) {
                            if(response.isSuccessful()){
                                try{
                                    if(response.body().getError()==0){
                                        entityRepository.clearSafeMode();
                                        entityRepository.saveBatch(response.body().getData());
                                        subscriber.onNext(response.body().getData());
                                    }else{
                                        subscriber.onError(new ErrorException(Constantes.ERROR_RESPUESTA_SERVER));
                                    }

                                }catch (Exception e){
                                    subscriber.onError(new ErrorException(Constantes.ERROR_PROCESAMIENTO_APP));
                                }
                            }else{
                                subscriber.onError(new ErrorException(Constantes.ERROR_RESPUESTA_SERVER));
                            }
                            subscriber.onComplete();
                        }

                        @Override
                        public void onFailure(Call<RespuestaServerNetClientesEntity> call, Throwable t) {
                            subscriber.onError(t);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            } else {
                try {
                    if(!entityRepository.isEmpty()){
                        subscriber.onNext(entityRepository.queryAll());
                    }else{
                        subscriber.onNext(Collections.emptyList());
                    }
                    subscriber.onComplete();
                }catch (Exception e){
                    e.printStackTrace();
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            }
        });
    }


    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }


}