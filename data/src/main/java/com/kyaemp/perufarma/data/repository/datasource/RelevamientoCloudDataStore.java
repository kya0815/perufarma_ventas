package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;
import com.kyaemp.perufarma.data.net.RestApi;
import com.kyaemp.perufarma.data.net.RestApiImplRelevamiento;

import java.util.List;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
public class RelevamientoCloudDataStore implements RelevamientoDataStore {

    private final RestApiImplRelevamiento restApi;

    /**
     * Construct a {@link LoginDataStore} based on connections to the api (Cloud).
     *
     * @param loginRestApi The {@link RestApi} implementation to use.
     */
    public RelevamientoCloudDataStore(RestApiImplRelevamiento loginRestApi) {
        this.restApi = loginRestApi;
    }

    @Override
    public Observable<List<RelevamientoEntity>> obtenerRelevamientos(int id) {
        return restApi.obtenerRelevamientos(id);
    }
}