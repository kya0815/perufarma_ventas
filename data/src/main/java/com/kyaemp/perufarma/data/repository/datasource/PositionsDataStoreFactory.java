package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.repository.PointsEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PositionsEntityRepository;
import com.kyaemp.perufarma.data.net.RestApiImpl;
import com.kyaemp.perufarma.data.net.RestApiImplPositions;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link LoginDataStore}.
 */
@Singleton
public class PositionsDataStoreFactory {

  private final Context context;
  @Inject
  public PositionsEntityRepository repository;

  @Inject
  public PositionsDataStoreFactory(Context context) {
    this.context = context;
  }

  public PositionsDataStore createCloudDataStore() {
    RestApiImplPositions restApi = new RestApiImplPositions(context,repository);
    return new PositionsCloudDataStore(restApi);
  }

  public PositionsDataStore createDB() {
    return new PositionsDBDataStore(repository);
  }

}
