package com.kyaemp.perufarma.data.net;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.RelevamientoEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.utiles.Constantes;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class RestApiImplRelevamiento {

    private final Context context;

    public RelevamientoEntityRepository relevamientoEntityRepository;
    /**
     * Constructor of the class
     *
     * @param context {@link Context}.
     */
    @Inject
    public RestApiImplRelevamiento(Context context,RelevamientoEntityRepository relevamientoEntityRepository) {
        this.context = context.getApplicationContext();
        this.relevamientoEntityRepository=relevamientoEntityRepository;
    }

    public Observable<List<RelevamientoEntity>> obtenerRelevamientos(int id) {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    RestApi restApi = new BaseNet().create(Constantes.URL_HOST,RestApi.class);

                    Call<List<RelevamientoEntity>> call = restApi.getRelevamiento(id);
                    call.enqueue(new Callback<List<RelevamientoEntity>>() {
                        @Override
                        public void onResponse(Call<List<RelevamientoEntity>> call, Response<List<RelevamientoEntity>> response) {
                            if(response.isSuccessful()) {
                                List<RelevamientoEntity> respuestaServer = response.body();
                                Log.e("respuestaServer", response.toString());
                                try {
                                    relevamientoEntityRepository.clearSafeMode();
                                    relevamientoEntityRepository.saveBatch(respuestaServer);
                                    subscriber.onNext(respuestaServer);
                                }catch (Exception e){
                                    e.printStackTrace();
                                    subscriber.onError(new ErrorException(Constantes.ERROR_AL_OBTENER_DATOS));
                                }
                            }else {
                                subscriber.onError(new ErrorException(Constantes.ERROR_RESPUESTA_SERVER));
                            }
                            subscriber.onComplete();
                        }

                        @Override
                        public void onFailure(Call<List<RelevamientoEntity>> call, Throwable t) {
                            subscriber.onError(t);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            } else {
                try {
                    subscriber.onNext(relevamientoEntityRepository.queryAll());
                    subscriber.onComplete();
                }catch (Exception e){
                    subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_RELEVAMIENTOS_LOCAL));
                }
            }
        });
    }



    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }


}