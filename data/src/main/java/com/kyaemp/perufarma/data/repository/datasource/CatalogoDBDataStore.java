package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.utiles.Constantes;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
@Singleton
public class CatalogoDBDataStore implements CatalogoDataStore {

    private CatalogoEntityRepository catalogoEntityRepository;

    @Inject
    public CatalogoDBDataStore(CatalogoEntityRepository catalogoEntityRepository) {
        this.catalogoEntityRepository=catalogoEntityRepository;
    }

    @Override
    public Observable<Boolean> getCatalogoOnline() {
        return null;
    }

    @Override
    public Observable<List<CatalogoEntity>> getCatalogo() {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(catalogoEntityRepository.queryAll());
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_CATALOGO_LOCAL));
            }
        });
    }

    @Override
    public Observable<CatalogoEntity> getCatalogoByID(long id) {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(catalogoEntityRepository.getById(id));
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_CATALOGO_LOCAL));
            }
        });
    }
}