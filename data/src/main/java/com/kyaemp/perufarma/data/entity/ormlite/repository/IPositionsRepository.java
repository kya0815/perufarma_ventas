package com.kyaemp.perufarma.data.entity.ormlite.repository;

import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;

import java.sql.SQLException;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public interface IPositionsRepository extends IRepository<PositionEntity,Integer> {

    public PositionEntity lastItem() throws  SQLException;
}
