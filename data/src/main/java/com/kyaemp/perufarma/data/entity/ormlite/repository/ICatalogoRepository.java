package com.kyaemp.perufarma.data.entity.ormlite.repository;

import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;

import java.sql.SQLException;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public interface ICatalogoRepository extends IRepository<CatalogoEntity,Integer> {

    public CatalogoEntity getById(long id) throws SQLException;
}
