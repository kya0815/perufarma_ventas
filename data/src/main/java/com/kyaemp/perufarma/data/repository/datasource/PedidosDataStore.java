package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PedidoEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface that represents a data store from where data is retrieved.
 */
public interface PedidosDataStore {

  Observable<List<PedidoEntity>> getPedidosIdCliente(int idCliente);
  Observable<Boolean> savePedidoIdCliente(PedidoEntity pedido);
}
