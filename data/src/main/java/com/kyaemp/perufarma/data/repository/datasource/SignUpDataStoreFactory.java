package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;

import com.kyaemp.perufarma.data.net.RestApiImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Factory that creates different implementations of {@link SignUpDataStore}.
 */
@Singleton
public class SignUpDataStoreFactory {

  private final Context context;

  @Inject
  public SignUpDataStoreFactory(Context context) {
    this.context = context;
  }

  public SignUpDataStore create() {
    return createCloudDataStore();
  }

  /**
   * Create {@link SignUpDataStore} to retrieve data from the Cloud.
   */
  public SignUpDataStore createCloudDataStore() {
    RestApiImpl restApi = new RestApiImpl(context);

    return new SignUpCloudDataStore(restApi);
  }

}
