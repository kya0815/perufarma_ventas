package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PointsEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.utiles.Constantes;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
@Singleton
public class PointsDBDataStore implements PointsDataStore {

    private PointsEntityRepository pointsEntityRepository;

    @Inject
    public PointsDBDataStore(PointsEntityRepository pointsEntityRepository) {
        this.pointsEntityRepository=pointsEntityRepository;
    }

    @Override
    public Observable<List<PointEntity>> obtenerPoints() {
        return Observable.create(subscriber -> {
            try {
                subscriber.onNext(pointsEntityRepository.queryAll());
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_OBTENER_DATA_LOCAL));
            }
        });
    }

    @Override
    public Observable<Boolean> guardarPoints(List<PointEntity> points) {
        return Observable.create(subscriber -> {
            try {
                if(pointsEntityRepository.saveBatch(points)==points.size()){
                    subscriber.onNext(true);
                }
                subscriber.onComplete();
            } catch (Exception e) {
                e.printStackTrace();
                subscriber.onError(new ErrorException(Constantes.ERROR_GUARDAR_DATA_LOCAL));
            }
        });
    }

    @Override
    public Observable<Boolean> savePoint(int id) {
        return null;
    }

}