package com.kyaemp.perufarma.data.entity;



import java.io.Serializable;

public class RespuestaServerEntity implements Serializable {


    private int status;
    private String mensaje;

    public RespuestaServerEntity() {
    }
    public RespuestaServerEntity(int status, String mensaje) {
        this.status = status;
        this.mensaje = mensaje;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String toString(){
        return "status: "+status+", messages:"+ mensaje;
    }
}
