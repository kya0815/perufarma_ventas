package com.kyaemp.perufarma.data.entity;



import java.io.Serializable;

public class RespuestaServerSocket implements Serializable {


    private int error;
    private String mensaje;
    private String data;

    public RespuestaServerSocket() {
    }

    public RespuestaServerSocket(int error, String mensaje, String data) {
        this.error = error;
        this.mensaje = mensaje;
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
