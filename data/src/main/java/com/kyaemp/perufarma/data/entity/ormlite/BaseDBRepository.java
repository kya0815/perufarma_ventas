package com.kyaemp.perufarma.data.entity.ormlite;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.DBHelper;
import com.kyaemp.perufarma.data.entity.ormlite.repository.BaseOrmLiteRepository;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public abstract class BaseDBRepository<T, Id> extends BaseOrmLiteRepository<T, Id> {
    public BaseDBRepository(Context context) {
        super(context, DBHelper.class);
    }
}