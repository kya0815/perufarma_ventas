package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.repository.PedidoEntityRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class PedidosDataStoreFactory {

  private final Context context;
  @Inject
  public PedidoEntityRepository entityRepository;

  @Inject
  public PedidosDataStoreFactory(Context context) {
    this.context = context;
  }

  public PedidosDataStore createDB() {

    return new PedidosDBDataStore(entityRepository);
  }

}
