package com.kyaemp.perufarma.data.net;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.exception.NetworkConnectionException;
import com.kyaemp.perufarma.data.net.netmodel.RespuestaServerNetEntity;
import com.kyaemp.perufarma.data.utiles.Constantes;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyaemp.perufarma.data.utiles.Constantes.URL_NODEJS;

@Singleton
public class RestApiImplCatalogo {

    private final Context context;

    CatalogoEntityRepository catalogoEntityRepository;
    /**
     * Constructor of the class
     *
     * @param context {@link Context}.
     */
    @Inject
    public RestApiImplCatalogo(Context context,CatalogoEntityRepository catalogoEntityRepository) {
        this.context = context.getApplicationContext();
        this.catalogoEntityRepository=catalogoEntityRepository;
    }

    public Observable<Boolean> getCatalogo() {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    RestApi restApi = new BaseNet().create(URL_NODEJS,RestApi.class);
                    Call<RespuestaServerNetEntity> call = restApi.getCatalogo();
                    call.enqueue(new Callback<RespuestaServerNetEntity>() {
                        @Override
                        public void onResponse(Call<RespuestaServerNetEntity> call, Response<RespuestaServerNetEntity> response) {
                            if(response.isSuccessful()){
                                try{
                                    if(response.body().getError()==0){
                                        catalogoEntityRepository.clearSafeMode();
                                        catalogoEntityRepository.saveBatch(response.body().getData());
                                        subscriber.onNext(true);
                                    }else{
                                        subscriber.onError(new ErrorException(Constantes.ERROR_RESPUESTA_SERVER));
                                    }

                                }catch (Exception e){
                                    subscriber.onError(new ErrorException(Constantes.ERROR_PROCESAMIENTO_APP));
                                }
                            }else{
                                subscriber.onError(new ErrorException(Constantes.ERROR_RESPUESTA_SERVER));
                            }
                            subscriber.onComplete();
                        }

                        @Override
                        public void onFailure(Call<RespuestaServerNetEntity> call, Throwable t) {
                            subscriber.onError(t);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            } else {
                subscriber.onError(new NetworkConnectionException());
            }
        });
    }


    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }


}