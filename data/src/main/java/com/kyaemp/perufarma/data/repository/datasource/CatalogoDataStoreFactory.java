package com.kyaemp.perufarma.data.repository.datasource;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.repository.CatalogoEntityRepository;
import com.kyaemp.perufarma.data.net.RestApiImplCatalogo;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class CatalogoDataStoreFactory {

  private final Context context;
  @Inject
  public CatalogoEntityRepository catalogoEntityRepository;
  
  @Inject
  public CatalogoDataStoreFactory(Context context) {
    this.context = context;
  }

  public CatalogoDataStore createCloudDataStore() {
    RestApiImplCatalogo restApi = new RestApiImplCatalogo(context,catalogoEntityRepository);

    return new CatalogoCloudDataStore(restApi);
  }

  public CatalogoDataStore createDB() {

    return new CatalogoDBDataStore(catalogoEntityRepository);
  }

}
