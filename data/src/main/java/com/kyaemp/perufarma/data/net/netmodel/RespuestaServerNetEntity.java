package com.kyaemp.perufarma.data.net.netmodel;



import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;

import java.io.Serializable;
import java.util.List;

public class RespuestaServerNetEntity implements Serializable {


    private int error;
    private String mensaje;
    private List<CatalogoEntity> data;

    public RespuestaServerNetEntity() {
    }

    public RespuestaServerNetEntity(int status, String mensaje, List<CatalogoEntity> data) {
        this.error = status;
        this.mensaje = mensaje;
        this.data = data;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public List<CatalogoEntity> getData() {
        return data;
    }

    public void setData(List<CatalogoEntity> data) {
        this.data = data;
    }

    public String toString(){
        return "error: "+ error +", messages:"+ mensaje+", data:"+data;
    }
}
