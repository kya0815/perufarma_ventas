package com.kyaemp.perufarma.data.entity.ormlite.repository;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;
import com.kyaemp.perufarma.data.entity.ormlite.BaseDBRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

@Singleton
public class PointsEntityRepository extends BaseDBRepository<PointEntity,Integer> implements IPointsRepository {

    @Inject
    public PointsEntityRepository(Context context) {
        super(context);
    }
}
