package com.kyaemp.perufarma.data.entity.ormlite.model;



import android.location.Location;

import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.Date;

public class PositionEntity implements Serializable {

    public static final String ID = "id";
    public static final String DEVICEID = "deviceId";
    public static final String TIME = "time";
    public static final String LATITUDE= "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ALTITUDE = "altitude";
    public static final String SPEED = "speed";
    public static final String COURSE = "course";
    public static final String BATTERY = "battery";
    public static final String NOMBRE = "nombre";
    public static final String GUARDAR = "guardar";

    @DatabaseField(columnName = ID,generatedId = true , allowGeneratedIdInsert = true)
    private int id;
    @DatabaseField(columnName = DEVICEID)
    private String deviceId;
    @DatabaseField(columnName = TIME)
    private Date time;
    @DatabaseField(columnName = LATITUDE)
    private double latitude;
    @DatabaseField(columnName = LONGITUDE)
    private double longitude;
    @DatabaseField(columnName = ALTITUDE)
    private double altitude;
    @DatabaseField(columnName = SPEED)
    private double speed;
    @DatabaseField(columnName = COURSE)
    private double course;
    @DatabaseField(columnName = BATTERY)
    private double battery;
    @DatabaseField(columnName = NOMBRE)
    private String nombre;
    @DatabaseField(columnName = GUARDAR)
    private int guardar;

    public PositionEntity() {
    }

    public PositionEntity(String deviceId, Location location, double battery, String nombre, int guardar) {
        this.deviceId = deviceId;
        time = new Date(location.getTime());
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        altitude = location.getAltitude();
        speed = location.getSpeed(); //
        course = location.getBearing();
        this.battery = battery;
        this.nombre=nombre;
        this.guardar=guardar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getCourse() {
        return course;
    }

    public void setCourse(double course) {
        this.course = course;
    }

    public double getBattery() {
        return battery;
    }

    public void setBattery(double battery) {
        this.battery = battery;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getGuardar() {
        return guardar;
    }

    public void setGuardar(int guardar) {
        this.guardar = guardar;
    }
}
