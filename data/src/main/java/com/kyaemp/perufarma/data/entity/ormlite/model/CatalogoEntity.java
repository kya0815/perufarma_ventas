package com.kyaemp.perufarma.data.entity.ormlite.model;



import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class CatalogoEntity implements Serializable {

    public static final String ID = "id";
    public static final String CATEGORIA = "categoria";
    public static final String MARCA = "marca";
    public static final String PRODUCTO = "producto";
    public static final String STOCK_ALMACEN = "stock_almacen";
    public static final String CANTIDAD_VENDIDA = "cantidad_vendida";
    public static final String PRECIO_UNITARIO = "precio_unitario";
    public static final String SUBTOTAL = "subtotal";

    @DatabaseField(columnName = ID,id = true,canBeNull = false)
    public long id;
    @DatabaseField(columnName = CATEGORIA)
    private String categoria;
    @DatabaseField(columnName = MARCA)
    private String marca;
    @DatabaseField(columnName = PRODUCTO)
    private String producto;
    @SerializedName("stock_almacen")
    @DatabaseField(columnName = STOCK_ALMACEN)
    private int stockAlmacen;
    @SerializedName("cantidad_vendida")
    @DatabaseField(columnName = CANTIDAD_VENDIDA)
    private int cantidadVendida;
    @SerializedName("precio_unitario")
    @DatabaseField(columnName = PRECIO_UNITARIO)
    private double precioUnitario;
    @DatabaseField(columnName = SUBTOTAL)
    private double subtotal;

    public CatalogoEntity() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getStockAlmacen() {
        return stockAlmacen;
    }

    public void setStockAlmacen(int stockAlmacen) {
        this.stockAlmacen = stockAlmacen;
    }

    public int getCantidadVendida() {
        return cantidadVendida;
    }

    public void setCantidadVendida(int cantidadVendida) {
        this.cantidadVendida = cantidadVendida;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }
}
