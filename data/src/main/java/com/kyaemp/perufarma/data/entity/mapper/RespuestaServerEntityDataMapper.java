package com.kyaemp.perufarma.data.entity.mapper;

import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link RespuestaServerEntity} (in the data layer) to {@link RespuestaServer} in the
 * domain layer.
 */
@Singleton
public class RespuestaServerEntityDataMapper {

  @Inject
  RespuestaServerEntityDataMapper() {}

  /**
   * Transform a {@link RespuestaServerEntity} into an {@link RespuestaServer}.
   *
   * @param userSignupEntity Object to be transformed.
   * @return {@link RespuestaServer} if valid {@link RespuestaServerEntity} otherwise null.
   */
  public RespuestaServer transform(RespuestaServerEntity userSignupEntity) {
    RespuestaServer respuestaServer = new RespuestaServer();
    if (userSignupEntity != null) {
      respuestaServer.setStatus(userSignupEntity.getStatus());
      respuestaServer.setMensaje(userSignupEntity.getMensaje());
    }
    return respuestaServer;
  }

  public RespuestaServerEntity transform(RespuestaServer respuestaServer) {
    RespuestaServerEntity respuestaServerEntity = new RespuestaServerEntity();
    if (respuestaServer != null) {
      respuestaServerEntity.setStatus(respuestaServer.getStatus());
      respuestaServerEntity.setMensaje(respuestaServer.getMensaje());
    }
    return respuestaServerEntity;
  }
}
