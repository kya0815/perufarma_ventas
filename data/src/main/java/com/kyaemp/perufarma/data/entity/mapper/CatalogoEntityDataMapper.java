package com.kyaemp.perufarma.data.entity.mapper;

import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.model.RespuestaServer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Mapper class used to transform {@link RespuestaServerEntity} (in the data layer) to {@link RespuestaServer} in the
 * domain layer.
 */
@Singleton
public class CatalogoEntityDataMapper {

  @Inject
  CatalogoEntityDataMapper() {}

  public Catalogo transform(CatalogoEntity itemIn) {
    Catalogo itemOut = new Catalogo();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setCategoria(itemIn.getCategoria());
      itemOut.setMarca(itemIn.getMarca());
      itemOut.setProducto(itemIn.getProducto());
      itemOut.setStockAlmacen(itemIn.getStockAlmacen());
      itemOut.setCantidadVendida(itemIn.getCantidadVendida());
      itemOut.setPrecioUnitario(itemIn.getPrecioUnitario());
      itemOut.setSubtotal(itemIn.getSubtotal());
    }
    return itemOut;
  }

  public CatalogoEntity transform(Catalogo itemIn) {
    CatalogoEntity itemOut = new CatalogoEntity();
    if (itemIn != null) {
      itemOut.setId(itemIn.getId());
      itemOut.setCategoria(itemIn.getCategoria());
      itemOut.setMarca(itemIn.getMarca());
      itemOut.setProducto(itemIn.getProducto());
      itemOut.setStockAlmacen(itemIn.getStockAlmacen());
      itemOut.setCantidadVendida(itemIn.getCantidadVendida());
      itemOut.setPrecioUnitario(itemIn.getPrecioUnitario());
      itemOut.setSubtotal(itemIn.getSubtotal());
    }
    return itemOut;
  }

  public <R,T> List<R> transform(List<T> c) {
    List<R> points = new ArrayList<>();
    if (c != null && !c.isEmpty()) {
      for (T t : c) {
        points.add(transform(t));
      }
    }
    return points;
  }

  private <R,T> R transform(T t) {
    if(t instanceof CatalogoEntity){
      return (R) transform((CatalogoEntity) t);
    }else if(t instanceof Catalogo){
      return (R) transform((Catalogo) t);
    }
    return null;
  }
//  public List<CatalogoEntity> transform(List<CatalogoEntity> pointsEntity) {
//    List<CatalogoEntity> points = new ArrayList<>();
//    if (pointsEntity != null && !pointsEntity.isEmpty()) {
//      for (CatalogoEntity point : pointsEntity) {
//        points.add(transform(point));
//      }
//    }
//    return points;
//  }
//
//  public List<CatalogoEntity> transform(List<CatalogoEntity> points) {
//    List<CatalogoEntity> pointsEntity = new ArrayList<>();
//    if (points != null && !points.isEmpty()) {
//      for (CatalogoEntity point : points) {
//        pointsEntity.add(transform(point));
//      }
//    }
//    return pointsEntity;
//  }
}
