package com.kyaemp.perufarma.data.net;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.kyaemp.perufarma.data.BuildConfig;
import com.kyaemp.perufarma.data.entity.RespuestaServerSocket;
import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;
import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.entity.ormlite.repository.PointsEntityRepository;
import com.kyaemp.perufarma.data.entity.ormlite.repository.RelevamientoEntityRepository;
import com.kyaemp.perufarma.data.exception.ErrorException;
import com.kyaemp.perufarma.data.exception.NetworkConnectionException;
import com.kyaemp.perufarma.data.net.netmodel.PointNetModel;
import com.kyaemp.perufarma.data.services.SocketManager;
import com.kyaemp.perufarma.data.utiles.Constantes;
import com.kyaemp.perufarma.domain.model.UserLogin;
import com.kyaemp.perufarma.domain.model.UserSignUp;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.socket.client.Ack;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.kyaemp.perufarma.data.utiles.Constantes.PREF_LOGGIN;
import static com.kyaemp.perufarma.data.utiles.Constantes.PREF_NAME;
import static com.kyaemp.perufarma.data.utiles.Constantes.PREF_USER;
import static com.kyaemp.perufarma.data.utiles.Constantes.SOCKET_LOGIN;
import static com.kyaemp.perufarma.data.utiles.Constantes.SOCKET_SIGNUP;
import static com.kyaemp.perufarma.data.utiles.Constantes.URL_NODEJS;

@Singleton
public class RestApiImpl {

    private final Context context;
    @Inject
    public PointsEntityRepository pointsEntityRepository;
    @Inject
    public RelevamientoEntityRepository relevamientoEntityRepository;
    /**
     * Constructor of the class
     *
     * @param context {@link Context}.
     */
    @Inject
    public RestApiImpl(Context context) {
        this.context = context.getApplicationContext();
    }

    public Observable<Boolean> savePoint(int id) {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    PointEntity pointEntity =pointsEntityRepository.findById(id);
                    PointNetModel pointNetModel = new PointNetModel(pointEntity.getId(),pointEntity.getLon(),pointEntity.getLat(),pointEntity.getName());
                    Gson gson = new Gson();
                    String jsonPoint = gson.toJson(pointNetModel);
                    Log.e("jsonPoint: ",jsonPoint);
                    RestApi restApi = new BaseNet().create(BuildConfig.HOST,RestApi.class);

                    Call<RespuestaServerEntity> call = restApi.savePoint(jsonPoint);
                    call.enqueue(new Callback<RespuestaServerEntity>() {
                        @Override
                        public void onResponse(Call<RespuestaServerEntity> call, Response<RespuestaServerEntity> response) {
                            if(response.isSuccessful()) {
                                RespuestaServerEntity respuestaServer = response.body();
                                Log.e("respuesta", respuestaServer.toString());
                                pointEntity.setEnviado(true);
                                try {
                                    subscriber.onNext(pointsEntityRepository.update(pointEntity));
                                }catch (Exception e){
                                    e.printStackTrace();
                                    subscriber.onError(new ErrorException(Constantes.ERROR_ACTUALIZANDO_DATOS));
                                }
                            }else {
                                subscriber.onError(new ErrorException(Constantes.ERROR_RESPUESTA_SERVER));
                            }
                            subscriber.onComplete();
                        }
                        @Override
                        public void onFailure(Call<RespuestaServerEntity> call, Throwable t) {
                            subscriber.onError(t);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            } else {
                subscriber.onError(new NetworkConnectionException());
            }
        });
    }


    public Observable<RespuestaServerEntity> login(UserLogin userLogin) {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    userLogin.setDeviceId(Constantes.idDevice(context));
                    Gson gson = new Gson();
                    SocketManager.getInstance().connectSocket(URL_NODEJS);
                    SocketManager.getInstance().getSocket().emit(SOCKET_LOGIN, gson.toJson(userLogin), (Ack) args -> {
                        try {
                            RespuestaServerSocket result = gson.fromJson(String.valueOf(args[0]),RespuestaServerSocket.class);
                            if (result.getError()==0) {
                                context.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit()
                                        .putBoolean(PREF_LOGGIN,true).apply();
                                context.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit()
                                        .putString(PREF_USER,userLogin.getUser()).apply();
                                context.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit()
                                        .putString(PREF_NAME,result.getData()).apply();
                                subscriber.onNext(new RespuestaServerEntity(1,userLogin.getUser()+" Se Logeo Correctamente"));
                                subscriber.onComplete();
                            } else {
                                subscriber.onError(new ErrorException(Constantes.ERROR_LOGEO_INCORRECTO));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(new ErrorException(Constantes.ERROR_LOGEO_PROCESAMIENTO));
                        }
                    });
                } catch (Exception e) {
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            } else {
                subscriber.onError(new NetworkConnectionException());
            }
        });
    }

    public Observable<RespuestaServerEntity> signup(UserSignUp userSignUp) {
        return Observable.create(subscriber -> {
            if (isThereInternetConnection()) {
                try {
                    userSignUp.setDeviceId(Constantes.idDevice(context));
                    Gson gson = new Gson();
                    SocketManager.getInstance().connectSocket(URL_NODEJS);
                    SocketManager.getInstance().getSocket().emit(SOCKET_SIGNUP, gson.toJson(userSignUp), (Ack) args -> {
                        try {
                            RespuestaServerSocket result = gson.fromJson(String.valueOf(args[0]),RespuestaServerSocket.class);
                            if (result.getError()==0) {
                                context.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit()
                                        .putBoolean(PREF_LOGGIN,true).apply();
                                context.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit()
                                        .putString(PREF_USER,userSignUp.getUser()).apply();
                                context.getSharedPreferences(Constantes.PREFERENCES, Context.MODE_PRIVATE).edit()
                                        .putString(PREF_NAME,userSignUp.getName()).apply();
                                subscriber.onNext(new RespuestaServerEntity(1,"Bienvenido "+userSignUp.getName()));
                                subscriber.onComplete();
                            } else {
                                subscriber.onError(new ErrorException(result.getMensaje()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            subscriber.onError(new ErrorException(Constantes.ERROR_LOGEO_PROCESAMIENTO));
                        }
                    });
                } catch (Exception e) {
                    subscriber.onError(new ErrorException(Constantes.ERROR_NO_DETERMINADO));
                }
            } else {
                subscriber.onError(new NetworkConnectionException());
            }
        });
    }

    /**
     * Checks if the device has any active internet connection.
     *
     * @return true device with internet connection, otherwise false.
     */
    private boolean isThereInternetConnection() {
        boolean isConnected;

        ConnectivityManager connectivityManager =
                (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }


}