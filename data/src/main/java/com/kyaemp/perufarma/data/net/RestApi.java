package com.kyaemp.perufarma.data.net;


import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;
import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.net.netmodel.RespuestaServerNetClientesEntity;
import com.kyaemp.perufarma.data.net.netmodel.RespuestaServerNetEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestApi {

    @POST("/")
    Call<RespuestaServerEntity> savePoint(@Body String JsonPoint);

    @GET("/api/json/get/cgDAGMfjdu")
    Call<List<RelevamientoEntity>> getRelevamiento(@Query("indent") int id);

    @GET("/api/getCatalogo")
    Call<RespuestaServerNetEntity> getCatalogo();
    @GET("/api/getClientes")
    Call<RespuestaServerNetClientesEntity> getClientes();

}