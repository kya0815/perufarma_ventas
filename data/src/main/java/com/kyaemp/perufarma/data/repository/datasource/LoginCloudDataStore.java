package com.kyaemp.perufarma.data.repository.datasource;


import com.kyaemp.perufarma.data.entity.RespuestaServerEntity;
import com.kyaemp.perufarma.data.net.RestApiImpl;
import com.kyaemp.perufarma.domain.model.UserLogin;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
public class LoginCloudDataStore implements LoginDataStore {

    private final RestApiImpl loginRestApi;

    /**
     * Construct a {@link LoginDataStore} based on connections to the api (Cloud).
     *
     * @param loginRestApi The {@link RestApiImpl} implementation to use.
     */
    public LoginCloudDataStore(RestApiImpl loginRestApi) {
        this.loginRestApi = loginRestApi;
    }

    @Override
    public Observable<RespuestaServerEntity> login(UserLogin userLogin) {
        return this.loginRestApi.login(userLogin);
    }
}