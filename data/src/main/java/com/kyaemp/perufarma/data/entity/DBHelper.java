package com.kyaemp.perufarma.data.entity;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.AbstractDBHelper;
import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.ClientesEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PedidoEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PointEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.PositionEntity;
import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;


/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public class DBHelper extends AbstractDBHelper{

    private static final String TABLE_NAME = "perufarma.db";

    public DBHelper(Context context) {
        super(context, TABLE_NAME);
    }

    @Override
    protected Class<?>[] getTableClassList() {
        return new Class<?>[]{PointEntity.class,RelevamientoEntity.class,
                CatalogoEntity.class, PositionEntity.class, ClientesEntity.class,
                PedidoEntity.class};
    }
}
