package com.kyaemp.perufarma.data.entity.ormlite.repository;

import com.kyaemp.perufarma.data.entity.ormlite.model.RelevamientoEntity;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

public interface IRelevamientoRepository extends IRepository<RelevamientoEntity,Integer> {


}
