package com.kyaemp.perufarma.data.repository.datasource;

import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;
import com.kyaemp.perufarma.data.net.RestApi;
import com.kyaemp.perufarma.data.net.RestApiImplCatalogo;

import java.util.List;

import io.reactivex.Observable;

/**
 * {@link LoginDataStore} implementation based on connections to the api (Cloud).
 */
public class CatalogoCloudDataStore implements CatalogoDataStore {

    private final RestApiImplCatalogo restApi;

    /**
     * Construct a {@link LoginDataStore} based on connections to the api (Cloud).
     *
     * @param restApi The {@link RestApi} implementation to use.
     */
    public CatalogoCloudDataStore(RestApiImplCatalogo restApi) {
        this.restApi = restApi;
    }

    @Override
    public Observable<Boolean> getCatalogoOnline() {
        return restApi.getCatalogo();
    }

    @Override
    public Observable<List<CatalogoEntity>> getCatalogo() {
        return null;
    }

    @Override
    public Observable<CatalogoEntity> getCatalogoByID(long id) {
        return null;
    }
}