package com.kyaemp.perufarma.data.entity.ormlite.repository;

import android.content.Context;

import com.kyaemp.perufarma.data.entity.ormlite.BaseDBRepository;
import com.kyaemp.perufarma.data.entity.ormlite.model.CatalogoEntity;

import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by pedro.zevallos on 26/10/2017.
 */

@Singleton
public class CatalogoEntityRepository extends BaseDBRepository<CatalogoEntity,Integer> implements ICatalogoRepository {

    @Inject
    public CatalogoEntityRepository(Context context) {
        super(context);
    }

    @Override
    public CatalogoEntity getById(long id) throws SQLException {
        return getDao().queryForEq(CatalogoEntity.ID,id).get(0);
    }
}
