package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Position;
import com.kyaemp.perufarma.domain.repository.PositionsRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerLastPositionServerUseCase extends UseCase<Position, ObtenerLastPositionServerUseCase.Params> {

  private final PositionsRepository repository;

  @Inject
  public ObtenerLastPositionServerUseCase(PositionsRepository repository, ThreadExecutor threadExecutor,
                                          PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<Position> buildUseCaseObservable(Params params) {
    return this.repository.getLastPositionDB();
  }

  public static final class Params {

  }
}
