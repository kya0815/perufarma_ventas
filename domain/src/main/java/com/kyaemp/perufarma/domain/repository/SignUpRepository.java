package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.RespuestaServer;
import com.kyaemp.perufarma.domain.model.UserLogin;
import com.kyaemp.perufarma.domain.model.UserSignUp;

import io.reactivex.Observable;

public interface SignUpRepository {

  Observable<RespuestaServer> singup(UserSignUp userSignUp);
}
