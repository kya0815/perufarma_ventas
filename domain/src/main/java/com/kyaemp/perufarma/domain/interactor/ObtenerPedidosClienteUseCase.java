package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;
import com.kyaemp.perufarma.domain.repository.ClientesRepository;
import com.kyaemp.perufarma.domain.repository.PedidosRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerPedidosClienteUseCase extends UseCase<List<Pedido>, ObtenerPedidosClienteUseCase.Params> {

  private final PedidosRepository repository;

  @Inject
  public ObtenerPedidosClienteUseCase(PedidosRepository repository, ThreadExecutor threadExecutor,
                                      PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override
  Observable<List<Pedido>> buildUseCaseObservable(Params params) {
    return this.repository.getPedidosIdCliente(params.id);
  }

  public static final class Params {
    private final int id;

    private Params(int id) {
      this.id = id;
    }

    public static Params id(int id) {
      return new Params(id);
    }
  }
}
