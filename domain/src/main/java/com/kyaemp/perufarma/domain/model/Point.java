package com.kyaemp.perufarma.domain.model;

public class Point  {

    private int id;
    private Double lat;
    private Double lon;
    private String name;
    private boolean enviado;

    public Point(){

    }

    public Point(int id, Double lat, Double lon, String name,boolean enviado) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.name = name;
        this.enviado = enviado;
    }

    public int getId() {
        return id;
    }

    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
