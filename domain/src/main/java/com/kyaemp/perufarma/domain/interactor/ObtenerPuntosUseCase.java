package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Point;
import com.kyaemp.perufarma.domain.repository.PointsRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerPuntosUseCase extends UseCase<List<Point>, ObtenerPuntosUseCase.Params> {

  private final PointsRepository pointsRepository;

  @Inject
  public ObtenerPuntosUseCase(PointsRepository pointsRepository, ThreadExecutor threadExecutor,
                              PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.pointsRepository = pointsRepository;
  }

  @Override Observable<List<Point>> buildUseCaseObservable(Params params) {
    return this.pointsRepository.obtenerPoints();
  }

  public static final class Params {
  }
}
