package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.Relevamiento;

import java.util.List;

import io.reactivex.Observable;

public interface RelevamientoRepository {

  Observable<List<Relevamiento>> obtenerRelevamientos(int id);
}
