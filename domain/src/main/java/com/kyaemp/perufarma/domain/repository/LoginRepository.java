package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.RespuestaServer;
import com.kyaemp.perufarma.domain.model.UserLogin;

import io.reactivex.Observable;

public interface LoginRepository {

  Observable<RespuestaServer> login(UserLogin userLogin);
}
