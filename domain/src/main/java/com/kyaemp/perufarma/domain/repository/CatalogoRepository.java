package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.model.Relevamiento;

import java.util.List;

import io.reactivex.Observable;

public interface CatalogoRepository {

  Observable<Boolean> getCatalogoOnline();
  Observable<List<Catalogo>> getCatalogo();
  Observable<Catalogo> getCatalogoByID(long id);
}
