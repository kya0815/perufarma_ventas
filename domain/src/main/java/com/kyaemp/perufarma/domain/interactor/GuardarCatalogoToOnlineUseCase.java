package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Relevamiento;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;
import com.kyaemp.perufarma.domain.repository.RelevamientoRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class GuardarCatalogoToOnlineUseCase extends UseCase<Boolean, GuardarCatalogoToOnlineUseCase.Params> {

  private final CatalogoRepository repository;

  @Inject
  public GuardarCatalogoToOnlineUseCase(CatalogoRepository repository, ThreadExecutor threadExecutor,
                                        PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<Boolean> buildUseCaseObservable(Params params) {
    return this.repository.getCatalogoOnline();
  }

  public static final class Params {
  }
}
