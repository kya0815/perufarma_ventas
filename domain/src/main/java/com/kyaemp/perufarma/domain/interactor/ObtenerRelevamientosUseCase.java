package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Relevamiento;
import com.kyaemp.perufarma.domain.repository.RelevamientoRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerRelevamientosUseCase extends UseCase<List<Relevamiento>, ObtenerRelevamientosUseCase.Params> {

  private final RelevamientoRepository repository;

  @Inject
  public ObtenerRelevamientosUseCase(RelevamientoRepository repository, ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<List<Relevamiento>> buildUseCaseObservable(Params params) {
    return this.repository.obtenerRelevamientos(params.id);
  }

  public static final class Params {
    private final int id;

    private Params(int id) {
      this.id = id;
    }

    public static Params id(int id) {
      return new Params(id);
    }
  }
}
