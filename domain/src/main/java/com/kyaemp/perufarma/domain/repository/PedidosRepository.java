package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.model.Pedido;

import java.util.List;

import io.reactivex.Observable;

public interface PedidosRepository {

  Observable<List<Pedido>> getPedidosIdCliente(int idCliente);
  Observable<Boolean> savePedidoIdCliente(Pedido pedido);
}
