package com.kyaemp.perufarma.domain.model;

public class Cliente {

    private int id;
    private String cliente_nombre;
    private Long cliente_ruc;
    private int cliente_codigo;
    private String ultimo_pedido;
    private int monto_vencido;
    private String telefono;
    private String direccion;
    private String departamen;
    private String provincia;
    private String distrito;
    private String correo;
    private double latitud;
    private double longitud;

    public Cliente(){

    }

    public Cliente(int id, String cliente_nombre, Long cliente_ruc, int cliente_codigo, String ultimo_pedido, int monto_vencido, String telefono, String direccion,
                   String departamen,String provincia,String distrito,String correo,double latitud, double longitud) {
        this.id = id;
        this.cliente_nombre = cliente_nombre;
        this.cliente_ruc = cliente_ruc;
        this.cliente_codigo = cliente_codigo;
        this.ultimo_pedido = ultimo_pedido;
        this.monto_vencido = monto_vencido;
        this.telefono = telefono;
        this.direccion = direccion;
        this.departamen = departamen;
        this.provincia = provincia;
        this.distrito = distrito;
        this.correo = correo;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCliente_nombre() {
        return cliente_nombre;
    }

    public void setCliente_nombre(String cliente_nombre) {
        this.cliente_nombre = cliente_nombre;
    }

    public Long getCliente_ruc() {
        return cliente_ruc;
    }

    public void setCliente_ruc(Long cliente_ruc) {
        this.cliente_ruc = cliente_ruc;
    }

    public int getCliente_codigo() {
        return cliente_codigo;
    }

    public void setCliente_codigo(int cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
    }

    public String getUltimo_pedido() {
        return ultimo_pedido;
    }

    public void setUltimo_pedido(String ultimo_pedido) {
        this.ultimo_pedido = ultimo_pedido;
    }

    public int getMonto_vencido() {
        return monto_vencido;
    }

    public void setMonto_vencido(int monto_vencido) {
        this.monto_vencido = monto_vencido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getDepartamen() {
        return departamen;
    }

    public void setDepartamen(String departamen) {
        this.departamen = departamen;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
