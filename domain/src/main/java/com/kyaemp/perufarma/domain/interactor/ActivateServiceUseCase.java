package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Relevamiento;
import com.kyaemp.perufarma.domain.repository.RelevamientoRepository;
import com.kyaemp.perufarma.domain.repository.ServiceRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ActivateServiceUseCase extends UseCase<Boolean, ActivateServiceUseCase.Params> {

  private final ServiceRepository repository;

  @Inject
  public ActivateServiceUseCase(ServiceRepository repository, ThreadExecutor threadExecutor,
                                PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<Boolean> buildUseCaseObservable(Params params) {
    return this.repository.activateServiceSocket();
  }

  public static final class Params {
  }
}
