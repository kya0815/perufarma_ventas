package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerCatalogoLocalUseCase extends UseCase<List<Catalogo>, ObtenerCatalogoLocalUseCase.Params> {

  private final CatalogoRepository repository;

  @Inject
  public ObtenerCatalogoLocalUseCase(CatalogoRepository repository, ThreadExecutor threadExecutor,
                                     PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<List<Catalogo>> buildUseCaseObservable(Params params) {
    return this.repository.getCatalogo();
  }

  public static final class Params {
  }
}
