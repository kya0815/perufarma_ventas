/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.domain.interactor;


import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.RespuestaServer;
import com.kyaemp.perufarma.domain.model.UserLogin;
import com.kyaemp.perufarma.domain.model.UserSignUp;
import com.kyaemp.perufarma.domain.repository.LoginRepository;
import com.kyaemp.perufarma.domain.repository.SignUpRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;


/**
 * This class is an implementation of {@link UseCase} that represents a use case for
 * retrieving data related to an specific {@link UserLogin}.
 */
@Singleton
public class SignUpUseCase extends UseCase<RespuestaServer, SignUpUseCase.Params> {

  private final SignUpRepository repository;

  @Inject
  public SignUpUseCase(SignUpRepository repository, ThreadExecutor threadExecutor,
                       PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override
  Observable<RespuestaServer> buildUseCaseObservable(Params params) {
    return this.repository.singup(params.userSignUp);
  }

  public static final class Params {

    private final UserSignUp userSignUp;

    private Params(UserSignUp userSignUp) {
      this.userSignUp = userSignUp;
    }

    public static Params userSignUp(UserSignUp userSignUp) {
      return new Params(userSignUp);
    }
  }
}
