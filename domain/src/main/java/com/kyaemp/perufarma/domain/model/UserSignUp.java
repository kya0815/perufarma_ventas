package com.kyaemp.perufarma.domain.model;

/**
 * Created by pedro.zevallos on 11/07/2017.
 */

public class UserSignUp {
    private String user;
    private String pass;
    private String code;
    private String name;
    private String deviceId;

    public UserSignUp(String user, String pass, String code, String name) {
        this.user = user;
        this.pass = pass;
        this.code = code;
        this.name = name;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
