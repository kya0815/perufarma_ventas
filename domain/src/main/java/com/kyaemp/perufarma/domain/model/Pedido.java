package com.kyaemp.perufarma.domain.model;

public class Pedido {

    private int id;
    private long codigo_item;
    private int cantidad;
    private int cliente_codigo;

    public Pedido(){

    }

    public Pedido(int id, long codigo_item, int cantidad, int cliente_codigo) {
        this.id = id;
        this.codigo_item = codigo_item;
        this.cantidad = cantidad;
        this.cliente_codigo = cliente_codigo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCodigo_item() {
        return codigo_item;
    }

    public void setCodigo_item(long codigo_item) {
        this.codigo_item = codigo_item;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCliente_codigo() {
        return cliente_codigo;
    }

    public void setCliente_codigo(int cliente_codigo) {
        this.cliente_codigo = cliente_codigo;
    }
}
