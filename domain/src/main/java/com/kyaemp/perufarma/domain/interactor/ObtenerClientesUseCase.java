package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;
import com.kyaemp.perufarma.domain.repository.ClientesRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerClientesUseCase extends UseCase<List<Cliente>, ObtenerClientesUseCase.Params> {

  private final ClientesRepository repository;

  @Inject
  public ObtenerClientesUseCase(ClientesRepository repository, ThreadExecutor threadExecutor,
                                PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<List<Cliente>> buildUseCaseObservable(Params params) {
    return this.repository.getClientes();
  }

  public static final class Params {
  }
}
