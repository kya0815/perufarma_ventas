package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Cliente;
import com.kyaemp.perufarma.domain.repository.ClientesRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerDetalleClienteUseCase extends UseCase<Cliente, ObtenerDetalleClienteUseCase.Params> {

  private final ClientesRepository repository;

  @Inject
  public ObtenerDetalleClienteUseCase(ClientesRepository repository, ThreadExecutor threadExecutor,
                                      PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override
  Observable<Cliente> buildUseCaseObservable(Params params) {
    return this.repository.getDetalleCliente(params.id);
  }

  public static final class Params {
    private final int id;

    private Params(int id) {
      this.id = id;
    }

    public static Params id(int id) {
      return new Params(id);
    }
  }
}
