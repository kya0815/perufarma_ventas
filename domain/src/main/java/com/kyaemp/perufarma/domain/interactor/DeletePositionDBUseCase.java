package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Position;
import com.kyaemp.perufarma.domain.repository.PositionsRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class DeletePositionDBUseCase extends UseCase<Boolean, DeletePositionDBUseCase.Params> {

  private final PositionsRepository repository;

  @Inject
  public DeletePositionDBUseCase(PositionsRepository repository, ThreadExecutor threadExecutor,
                                 PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<Boolean> buildUseCaseObservable(Params params) {
    return this.repository.deleteDB(params.id);
  }

  public static final class Params {
    private final int id;

    private Params(int id) {
      this.id = id;
    }

    public static Params id(int id) {
      return new Params(id);
    }
  }
}
