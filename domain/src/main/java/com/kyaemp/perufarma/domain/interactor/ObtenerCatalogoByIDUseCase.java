package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Catalogo;
import com.kyaemp.perufarma.domain.repository.CatalogoRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class ObtenerCatalogoByIDUseCase extends UseCase<Catalogo, ObtenerCatalogoByIDUseCase.Params> {

  private final CatalogoRepository repository;

  @Inject
  public ObtenerCatalogoByIDUseCase(CatalogoRepository repository, ThreadExecutor threadExecutor,
                                    PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<Catalogo> buildUseCaseObservable(Params params) {
    return this.repository.getCatalogoByID(params.id);
  }

  public static final class Params {
    private final long id;

    private Params(long id) {
      this.id = id;
    }

    public static Params id(long id) {
      return new Params(id);
    }
  }
}
