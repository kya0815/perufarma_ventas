package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Point;
import com.kyaemp.perufarma.domain.repository.PointsRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class GuardarPuntosUseCase extends UseCase<Boolean, GuardarPuntosUseCase.Params> {

  private final PointsRepository pointsRepository;

  @Inject
  public GuardarPuntosUseCase(PointsRepository pointsRepository, ThreadExecutor threadExecutor,
                              PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.pointsRepository = pointsRepository;
  }

  @Override Observable<Boolean> buildUseCaseObservable(Params params) {
    return this.pointsRepository.guardarPoints(params.points);
  }

  public static final class Params {

    private final List<Point> points;

    private Params(List<Point> points) {
      this.points = points;
    }

    public static Params listaPuntos(List<Point> points) {
      return new Params(points);
    }
  }
}
