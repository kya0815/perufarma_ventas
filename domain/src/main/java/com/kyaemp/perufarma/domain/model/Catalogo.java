package com.kyaemp.perufarma.domain.model;

public class Catalogo {

    private long id;
    private String categoria;
    private String marca;
    private String producto;
    private int stockAlmacen;
    private int cantidadVendida;
    private double precioUnitario;
    private double subtotal;

    public Catalogo(){

    }

    public Catalogo(long id, String categoria, String marca, String producto, int stockAlmacen, int cantidadVendida, double precioUnitario, double subtotal) {
        this.id = id;
        this.categoria = categoria;
        this.marca = marca;
        this.producto = producto;
        this.stockAlmacen = stockAlmacen;
        this.cantidadVendida = cantidadVendida;
        this.precioUnitario = precioUnitario;
        this.subtotal = subtotal;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getStockAlmacen() {
        return stockAlmacen;
    }

    public void setStockAlmacen(int stockAlmacen) {
        this.stockAlmacen = stockAlmacen;
    }

    public int getCantidadVendida() {
        return cantidadVendida;
    }

    public void setCantidadVendida(int cantidadVendida) {
        this.cantidadVendida = cantidadVendida;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }
}
