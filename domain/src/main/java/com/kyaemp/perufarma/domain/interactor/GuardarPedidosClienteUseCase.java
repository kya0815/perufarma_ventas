package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Pedido;
import com.kyaemp.perufarma.domain.repository.PedidosRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class GuardarPedidosClienteUseCase extends UseCase<Boolean, GuardarPedidosClienteUseCase.Params> {

  private final PedidosRepository repository;

  @Inject
  public GuardarPedidosClienteUseCase(PedidosRepository repository, ThreadExecutor threadExecutor,
                                      PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override
  Observable<Boolean> buildUseCaseObservable(Params params) {
    return this.repository.savePedidoIdCliente(params.pedido);
  }

  public static final class Params {
    private final Pedido pedido;

    private Params(Pedido pedido) {
      this.pedido = pedido;
    }

    public static Params pedido(Pedido pedido) {
      return new Params(pedido);
    }
  }
}
