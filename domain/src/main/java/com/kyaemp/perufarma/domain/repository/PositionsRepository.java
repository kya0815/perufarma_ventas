package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.Point;
import com.kyaemp.perufarma.domain.model.Position;

import java.util.List;

import io.reactivex.Observable;

public interface PositionsRepository {

  Observable<Boolean> saveDB(Position position);
  Observable<Boolean> saveServer(Position position);
  Observable<Boolean> deleteDB(int id);
  Observable<Position> getLastPositionDB();
}
