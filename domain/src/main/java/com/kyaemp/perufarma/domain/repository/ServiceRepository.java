package com.kyaemp.perufarma.domain.repository;



import io.reactivex.Observable;

public interface ServiceRepository {

  Observable<Boolean> activateServiceSocket();
}
