package com.kyaemp.perufarma.domain.model;

/**
 * Created by pedro.zevallos on 11/07/2017.
 */

public class UserLogin {
    private String user;
    private String pass;
    private String deviceId;

    public UserLogin(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
