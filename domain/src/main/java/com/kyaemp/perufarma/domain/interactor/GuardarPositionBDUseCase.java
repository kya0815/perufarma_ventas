package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.model.Position;
import com.kyaemp.perufarma.domain.repository.PositionsRepository;
import com.kyaemp.perufarma.domain.repository.RelevamientoRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class GuardarPositionBDUseCase extends UseCase<Boolean, GuardarPositionBDUseCase.Params> {

  private final PositionsRepository repository;

  @Inject
  public GuardarPositionBDUseCase(PositionsRepository repository, ThreadExecutor threadExecutor,
                                  PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.repository = repository;
  }

  @Override Observable<Boolean> buildUseCaseObservable(Params params) {
    return this.repository.saveDB(params.position);
  }

  public static final class Params {
    private final Position position;

    private Params(Position position) {
      this.position = position;
    }

    public static Params position(Position position) {
      return new Params(position);
    }
  }
}
