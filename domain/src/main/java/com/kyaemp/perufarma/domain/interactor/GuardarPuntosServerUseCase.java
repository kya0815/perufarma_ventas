/**
 * Copyright (C) 2015 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.kyaemp.perufarma.domain.interactor;

import com.kyaemp.perufarma.domain.executor.PostExecutionThread;
import com.kyaemp.perufarma.domain.executor.ThreadExecutor;
import com.kyaemp.perufarma.domain.repository.PointsRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class GuardarPuntosServerUseCase extends UseCase<Boolean, GuardarPuntosServerUseCase.Params> {

  private final PointsRepository pointsRepository;

  @Inject
  public GuardarPuntosServerUseCase(PointsRepository pointsRepository, ThreadExecutor threadExecutor,
                                    PostExecutionThread postExecutionThread) {
    super(threadExecutor, postExecutionThread);
    this.pointsRepository = pointsRepository;
  }

  @Override Observable<Boolean> buildUseCaseObservable(Params params) {
    return this.pointsRepository.savePoint(params.id);
  }

  public static final class Params {

    private final int id;

    private Params(int id) {
      this.id = id;
    }

    public static Params id(int id) {
      return new Params(id);
    }
  }
}
