package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.Point;

import java.util.List;

import io.reactivex.Observable;

public interface PointsRepository {

  Observable<List<Point>> obtenerPoints();
  Observable<Boolean> guardarPoints(List<Point> points);
  Observable<Boolean> savePoint(int id);
}
