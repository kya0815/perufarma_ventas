package com.kyaemp.perufarma.domain.repository;


import com.kyaemp.perufarma.domain.model.Cliente;

import java.util.List;

import io.reactivex.Observable;

public interface ClientesRepository {

  Observable<List<Cliente>> getClientes();
  Observable<Cliente> getDetalleCliente(int idCliente);
}
